--------------------------INSTRUKCJA--------------------------

-----INSTALACJA-----
1. Sciągnij eclipse PHP Luna
2. Po uruchomieniu wejdź w Window > Show View > Other > Git repositories
3. Wcisnij Clone Git i wpisz potrzebne dane
4. Zaznacz opcję by od razu zaimportowało projekt do workspace
5. Zainstaluj xampp (default C:\xampp) wraz z apache i mysql

-----KONFIGURACJA-----
1. uruchom xampp i odpal mysql oraz apache

2. W eclipse lewym górnym rogu jest ikonka play ze skrzynką - wciśnij strzałkę obok i wybierz external tools configuration
3. Wciśnij new launch configuration
4. Zakładka Main, Location: ${workspace_loc:/grapm/install.bat} , Workig Directory: ${workspace_loc:/grapm} i zapisz.
	- możesz dodac w Arguments /D co spowoduje, że zanim zostaną skopiowane pliki najpierw usunie cały folder 
	- bądź zrobic 2 konfiguracje 
5. Teraz wystarczy, że uruchomisz tą konfigurację a wszystkie pliki skopiują się do folderu xamppa. 
	-możesz zmieni ściezkę docelową edytując install.bat

6. W lewym górnym rogu wcisnij strzałkę obok ikonki play a następnie run configurations.
7. Zaznacz PHP Web Application i wcisnij new launch configuration.
8. W zakładce Server Wcisnij przycisk New obok PHP Server, nazwij, wybierz debuger, Base URL: http://localhost (jesli będzie pokazywał błąd to dodaj port czyli http://localhost:80), local Web Root: C:\xampp\htdocs, File: /grapm/index.html, zaznacz Auto Generate.
9. Window > Preferences > Run/Debug > Launching > Save reuqired.. zaznacz Always
10. Teraz wystarczy, że po wprowadzeniu zmian wciśniesz play ze skrzynką, następnie samo play, a pokaże Ci się strona główna w eclipsie (index.html).

-----BAZA DANYCH-----
1. W panelu administracyjnym bazy danych (domyślnie <adres>/graPM/adminpanel.html ustówrz baze danych o nazwie grapm). Bazie ustaw metodę porównywania napisów na utf8_unicode_ci.
2. W tej bazie utwórz tabelę o nazwie scheme o kolumnach: id, name, expanded, language, json. 
	Wszystkie oprócz id typu TEXT, a id typu INT PRIMARY I KONIECZNIE ZAZNACZ KOLUMNĘ A_I (AUTOINCREMENT)
3. W tej bazie utwórz też tabelę o nazwie game_logs o kolumnach: id, log_version, timestamp, gameplay_id, 
	logged_fieldnames, logged_values, extra_risk_log_fieldnames, extra_risk_log_values. Wszystkie oprócz id oraz log_version są typu TEXT, 
	a id typu INT PRIMARY I KONIECZNIE ZAZNACZ KOLUMNĘ A_I (AUTOINCREMENT)
4. Zrób również tabelę game_rating z kolumnami: id, log_version, timestamp, gameplay_id, rating. Wszystkie oprócz id oraz log_version są typu TEXT, 
	a id typu INT PRIMARY I KONIECZNIE ZAZNACZ KOLUMNĘ A_I (AUTOINCREMENT)
5. W razie wątpliwości uploadxlsx.php i scheme.php powinny pozwolić wywioskować dane o bazie

----ROZWÓJ APLIKACJI-----
W pliku index.html kolejność importowania js skryptów jest ważna. Podczas rozwoju aplikacji należy zamienić
import skryptu grapm.min.js na:
<script src="js/language.js"></script>
<script src="js/classes/InterfaceFunctions.js"></script>
<script src="js/classes/MainContainer.js"></script>
<script src="js/classes/PlayFactory.js"></script>
<script src="js/classes/ProductsFactory.js"></script>
<script src="js/classes/RisksFactory.js"></script>
<script src="js/classes/SolutionsFactory.js"></script>
<script src="js/classes/TasksFactory.js"></script>
<script src="js/classes/ResourcesFactory.js"></script>
<script src="js/classes/LogMaker.js"></script>
<script src="js/classes/entity/Product.js"></script>
<script src="js/classes/entity/Resource.js"></script>
<script src="js/classes/entity/Risk.js"></script>
<script src="js/classes/entity/Solution.js"></script>
<script src="js/classes/Config.js"></script>

W pliku adminpanel.html powinno to wyglądać następująco:
<script src="js/readxlsx/shim.js"></script>
<script src="js/readxlsx/jszip.js"></script>
<script src="js/readxlsx/xlsx.js"></script>
<script src="js/readxlsx/loadxlsx.js"></script>
<script src="js/readxlsx/adminpanel.js"></script>

----YOUTUBE-----
Dane do konta youtube na które zostały wrzucone filimki instruktażowe (logowanie danymi z podczepionego gmaila):
login: grapm.eti
hasło: grapm2016