$(document).ready(function() {
	var request= new XMLHttpRequest();
	request.open("GET", "php/scheme.php?id=info", true);
	request.send(null);
	var self = this;
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			json = request.responseText;
			json = JSON.parse(json);
			
			var combo = document.getElementById("schemeCombo");
			for( var lang in json) {
				var schemes = json[lang].schemes;
				for(var i=0;i<schemes.length;i++) {
					var option = document.createElement("option");
					option.text = schemes[i].name + " - " + lang;
					option.value = schemes[i].id;
					try {
						combo.add((option), null); //Standard 
					}catch(error) {
						combo.add(option); // IE only
					}
				}
			}
			
			$('#delete').show();
		}
	}
});
$('#deleteButton').click(function() {
	var combo = $('#schemeCombo');
	var request= new XMLHttpRequest();
	request.open("GET", "php/delete.php?id="+combo.val(), true);
	request.send(null);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			var response = request.responseText;
			$('#response').text(response);
		}
	}
});