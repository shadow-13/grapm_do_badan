function Language() {
	this.properties = {
		"pl" : {
			textWelcomeGameName : { text : "GraPM", when : 0},
			textWelcomeTitle : { text : "Naucz się zarządzać projektami", when : 0 },
			textHowToPlay : { text : "Jak grać?", when : 0 },
			textWelcomeContents : { text : "Wciel się w rolę kierownika projektu i osiągnij sukces. Wytwórz zamówiony produkt, zarządzaj zakresem, jakością, czasem, zadaniami i ryzykiem projektu.\\nPrzenoś produkty i działania wobec ryzyka na listę zadań, naciśnij pasek postępu by Twoje zasoby przydzieliły się kolejno do zadań i rozpoczęły pracę.\\nObserwuj wskaźniki projektu, szczególnie zadowolenie klienta i zespołu. Zatrzymuj zadania, zmieniaj plany, reaguj na pojawiające się ryzyka.\\nPrzeprowadź jak najlepiej projekt w ograniczonym czasie lub jak najsprawniej wytwórz kompletny produkt. Powodzenia!", when : 0 },
			textWelcomeTypeTime : { text : "Ograniczony czas", when : 0 },
			textWelcomeTypeFull : { text : "Kompletny produkt", when : 0 },
			textWelcomeStart : { text : "Rozpocznij Grę", when : 0 },
			textWelcomeFooter : { text : "Projekt dyplomowy inżynierski 2014\\nPolitechnika Gdańska, Wydział Elektroniki, Telekomunikacji i Informatyki, Katedra Inżynierii Oprogramowania\\nAutorzy: Damian Płatek, Rafał Piechowski, Anna Wasik, Sylwia Grabowska\\nOpiekun projektu i autor koncepcji gry: dr inż. Jakub Miler\\n wersja 1.1, Anna Wasik 2016", when : 0 },
			textWelcomeTutorial : { text : "https://www.youtube.com/embed/sJ2U9OL1EfA", when : 0 },
			
			textPopupEndTitleSucceed : { text : "SUKCES!", when : 1 },
			textPopupEndContentsSucceed : { text : "Projekt ukończony", when : 1 },
			textPopupEndTitleTimeOut : { text : "KONIEC CZASU", when : 1 },
			textPopupEndContentsTimeOut : { text : "Projekt częściowo ukończony", when : 1 },
			textPopupEndTitleLost : { text : "PORAŻKA!", when : 1 },
			textPopupEndContentsLostClient : { text : "Niezadowolony klient zrezygnował z projektu", when : 1 },
			textPopupEndContentsLostTeam : { text : "Niezadowolony zespół odszedł z projektu", when : 1 },
			textPopupEndContentsLostRisk : { text : "Ryzyko porażki projektu osiągnęło 100%", when : 1 },	
			textPopupQuitContent : { text : "Przerwać projekt?", when : 1 },
			textPopupQuitYesButton : { text : "Tak", when : 1 },
			textPopupQuitNoButton : { text : "Nie", when : 1 },
			textPopupEndContentsStars : { text : "Oceń grę:", when : 1 },
			
			textInterfaceTitleProject : { text : "Projekt", when : 0 },
			textInterfaceTitleProjectTooltip : { text : "Projekt, którym zarządzasz. Przenoś produkty i działania wobec ryzyka na listę zadań, naciśnij pasek postępu by Twoje zasoby rozpoczęły pracę.", when : 1 },
			textInterfaceTitleProducts : { text : "Produkt", when : 0 },
			textInterfaceTitleProductsTooltip : { text : "Elementy i cechy produktu, który masz dostarczyć klientowi. Widzisz kolejno nazwę elementu, wartość biznesową elementu zwiększającą zakres wykonanego produktu (pudełka), wpływ elementu na jakość produktu (gwiazdki) oraz pracochłonność wytworzenia elementu (młotki).", when : 1 },
			textInterfaceTitleResources : { text : "Zasoby", when : 0 },
			textInterfaceTitleResourcesTooltip : { text : "Twój zespół wykonujący zadania. Dbaj o jego zadowolenie. Zasoby mają nazwę oraz różną wydajność pracy (młotki - ile pracy liczonej w młotkach lub kołach zębatych zasób wykonuje w ciągu jednego dnia). Sortuj zasoby tak, aby najwydajniejsze realizowały największe zadania.", when : 1 },
			textInterfaceTitleTasks : { text : "Zadania", when : 0 },
			textInterfaceTitleTasksTooltip : { text : "Bieżące zadania do wykonania w projekcie. Przenoś tutaj elementy produktu oraz działania wobec ryzyka. Ustawiaj kolejność, w jakiej mają je wykonywać Twoje zasoby. Uruchamiaj i zatrzymuj pracę paskiem postępu pracy pod listą. Zarządzaj zadaniami w trakcie pauzy. Pamiętaj, że każda pauza kosztuje 100 punktów.", when : 1 },
			textInterfaceTitleRisks : { text : "Ryzyka", when : 0 },
			textInterfaceTitleRisksTooltip : { text : "Niepewne zdarzenia mogące wpłynąć negatywnie lub pozytywnie na Twój projekt. Czerwone zagrożenia wpływają negatywnie, a zielone szanse pozytywnie. Widzisz kolejno nazwę ryzyka, możliwość jego wystąpienia w % oraz siłę jego wpływu od 1 do 4 wykrzykników. Dobierz odpowiednie działania wobec ryzyka aby obniżyć zagrożenia i zwiększyć szanse.", when : 1 },
			textInterfaceTitleActions : { text : "Działania wobec ryzyka", when : 0 },
			textInterfaceTitleActionsTooltip : { text : "Różne działania, które możesz podjąć by wpłynąć ma możliwość wystąpienia ryzyk. Działania mają nazwę oraz pracochłonność (koła zębate). Nie wiadomo, które działanie okaże się najlepsze na Twoje aktualne ryzyka. Przeglądaj ryzyka, wyszukuj pasujące działania, podejmuj je i obserwuj ich wpływ na ryzyko.", when : 1 },
			textInterfaceProjectScope : { text : "Zakres", when : 0 },
			textInterfaceProjectScopeTooltip : { text : "Wartość biznesowa produktu, który wytwarzasz. Widzisz przyrastającą wartość biznesową wraz z wytwarzaniem poszczególnych elementów produktu. Wykonaj jak najsprawniej kompletny produkt by ukończyć projekt z sukcesem.", when : 1 },
			textInterfaceProjectQuality : { text : "Jakość", when : 0 },
			textInterfaceProjectQualityTooltip : { text : "Jakość produktu, który wytwarzasz. Jakość jest określona w skali -5 (najgorsza) do +5 (najlepsza). Postaraj się utrzymywać wysoką jakość produktu wytwarzając poszczególne elementy o różnym, nawet negatywnym wpływie na jakość.", when : 1 },
			textInterfaceProjectTime : { text : "Czas", when : 0 },
			textInterfaceProjectTimeTooltip : { text : "Czas na realizację projektu. Jeżeli grasz na ograniczony czas, to po wyczerpaniu czasu projekt zostanie zakończony. W grze w kompletny produkt możesz przekroczyć czas maksymalnie o 100%. Postaraj się optymalnie wykorzystywać dostępny czas.", when : 1 },
			textInterfaceProjectClient : { text : "Klient", when : 0 },
			textInterfaceProjectClientTooltip : { text : "Zadowolenie Twojego klienta. Klient oczekuje od Ciebie wydajnej pracy, wysokiej jakości produktu i niskiego ryzyka projektu. Gdy jest zbyt długo niezadowolony, może zrezygnować z projektu, co oznacza Twoją porażkę!", when : 1 },
			textInterfaceProjectTeam : { text : "Zespół", when : 0 },
			textInterfaceProjectTeamTooltip : { text : "Zadowolenie Twojego zespołu. Zespół oczekuje dobrych postępów projektu, dużej wydajności pracy oraz niskiego ryzyka projektu. Gdy jest zbyt długo niezadowolony, może odejść z projektu, co oznacza Twoją porażkę!", when : 1 },
			textInterfaceProjectRisk : { text : "Ryzyko", when : 0 },
			textInterfaceProjectRiskTooltip : { text : "Ogólny poziom ryzyka projektu. Oznacza możliwość porażki Twojego projektu. Klient i zespół nie lubią zbyt wysokiego ryzyka. Pozbywaj się zagrożeń i wykorzystuj szanse, by zmniejszyć ogólne ryzyko projektu.", when : 1 },
			textInterfaceProjectScore : { text : "Punkty", when : 0 },
			textInterfaceProjectScoreTooltip : { text : "Twój wynik w grze. Punkty otrzymujesz za wytwarzanie produktu, obniżanie zagrożeń i wykorzystywanie szans, utrzymywanie wysokiej jakości produktu, wysokiego zadowolenia klienta i zespołu oraz niskiego poziomu ryzyka. Zdobądź najwięcej punktów i zostań najlepszym kierownikiem projektu!", when : 1 },
			
			textProductValueImgTooltip : { text : "Wartość dla klienta - wpływa na zakres", when : 1 },
			textProductQualityImgTooltip : { text : "Wpływ na jakość projektu", when : 1 },
			textProductSizeImgTooltip : { text : "Pracochłonność", when : 1 },
			textResourceProductivityImgTooltip : { text : "Wydajność zasobu", when : 1 },
			textRiskInfluenceImgTooltip : { text : "Wpływ ryzyka", when : 1 },
			textAutopause : { text : "Autopauza", when : 0 },
			
			textInterfaceTimeUnitDay : { text : "dzień", when : 0 },
			textInterfaceTimeUnitWeek : { text : "tydzień", when : 0 },
			textInterfaceTimeUnitMonth : { text : "miesiąc", when : 0 },
			textInterfaceTimeUnitYear : { text : "rok", when : 0 }
		},
		
		"en" : {
			textWelcomeGameName : { text : "GraPM", when : 0 },
			textWelcomeTitle : { text : "Learn to manage projects", when : 0 },
			textHowToPlay : { text : "How to play?", when : 0 },
			textWelcomeContents : { text : "Take on the role of project manager and achieve success. Deliver ordered product, manage the scope, quality, time, tasks, and risk of the project. \\nMove products and risk actions to the task list, press the progress bar to allocate resources to the tasks sequentially and start work. \\nMonitor the project indicators, customer and team satisfaction. Pause tasks, change your plans, react to appearing risks. \\nAccomplish best results in limited time or efficiently complete the product. Good luck!", when : 0 },
			textWelcomeTypeTime : { text : "Limited time", when : 0 },
			textWelcomeTypeFull : { text : "Complete product", when : 0 },
			textWelcomeStart : { text : "PLAY", when : 0 },
			textWelcomeFooter : { text : "Engineering Diploma Project 2014\\nGdansk University of Technology, Poland, Faculty of Electronics, Telecommunications and Informatics, Department of Software Engineering\\nAuthors: Damian Płatek, Rafał Piechowski, Anna Wasik, Sylwia Grabowska\\nProject supervisor and author of the game concept: Jakub Miler, PhD\\n version 1.1, Anna Wasik 2016", when : 0 },
			textWelcomeTutorial : { text : "https://www.youtube.com/embed/uqpD-GIQQhQ", when : 0 },
			
			textPopupEndTitleSucceed : { text : "YOU WIN!", when : 1 },
			textPopupEndContentsSucceed : { text : "Project completed", when : 1 },
			textPopupEndTitleTimeOut : { text : "TIME IS OUT", when : 1 },
			textPopupEndContentsTimeOut : { text : "Project partially completed", when : 1 },
			textPopupEndTitleLost : { text : "YOU LOSE!", when : 1 },
			textPopupEndContentsLostClient : { text : "Dissatisfied customer cancelled the project", when : 1 },
			textPopupEndContentsLostTeam : { text : "Dissatisfied team left the project", when : 1 },
			textPopupEndContentsLostRisk : { text : "The risk of project failure reached 100%", when : 1 },
			textPopupQuitContent : { text : "Abandon the project?", when : 1 },
			textPopupQuitYesButton : { text : "Yes", when : 1 },
			textPopupQuitNoButton : { text : "No", when : 1 },
			textPopupEndContentsStars : { text : "Rate game:", when : 1 },

			textInterfaceTitleProject : { text : "Project", when : 0 },
			textInterfaceTitleProjectTooltip : { text : "The project you manage. Move products and risk actions to the task list, press the progress bar to have your resources work.", when : 1 },
			textInterfaceTitleProducts : { text : "Product", when : 0 },
			textInterfaceTitleProductsTooltip : { text : "Elements and features of the product to deliver to the customer. Product elements have a name, business value increasing the product scope (boxes), impact on product quality (stars), and development cost (hammers).", when : 1 },
			textInterfaceTitleResources : { text : "Resources", when : 0 },
			textInterfaceTitleResourcesTooltip : { text : "Your team carrying out the tasks. Care for its satisfaction. Resources have a name and varying productivity (hammers - how much work counted in hammers or gears a resource does in one day). Sort the resources to have the most productive ones do the largest tasks.", when : 1 },
			textInterfaceTitleTasks : { text : "Tasks", when : 0 },
			textInterfaceTitleTasksTooltip : { text : "Current tasks to do in the project. Move the products elements and risk actions here. Sort the tasks to adjust the development sequence for your resources. Start and pause work with the progress bar under the task list. Manage tasks while paused. Mind each pause costs 100 points.", when : 1 },
			textInterfaceTitleRisks : { text : "Risks", when : 0 },
			textInterfaceTitleRisksTooltip : { text : "Uncertain events that may affect your project negatively or positively. Red threats have negative impact while green opportunities have positive impact. Risks have a name, possibility to occur in %, and the impact in a range of 1 to 4 exclamation marks. Select appropriate risk actions to reduce threats and increase opportunities.", when : 1 },
			textInterfaceTitleActions : { text : "Risk Actions", when : 0 },
			textInterfaceTitleActionsTooltip : { text : "Various actions you might take to change the possibility of risk occurrence. Risk actions have a name and execution cost (gears). You may not know which actions will affect your current risks. Examine the risks, search for matching actions, start them and observe the impact on risks.", when : 1 },
			textInterfaceProjectScope : { text : "Scope", when : 0 },
			textInterfaceProjectScopeTooltip : { text : "Business value of the product you develop. Business value increases gradually with the development of each product element. Develop complete product most effectively to finish the project successfully and WIN.", when : 1 },
			textInterfaceProjectQuality : { text : "Quality", when : 0 },
			textInterfaceProjectQualityTooltip : { text : "The quality of the product you develop. Quality scale ranges from -5 (worst) do +5 (best). Try to keep high product quality while developing the product elements with various impact on quality, even negative.", when : 1 },
			textInterfaceProjectTime : { text : "Time", when : 0 },
			textInterfaceProjectTimeTooltip : { text : "Time to deliver the product. If you play limited time, your project will end when this time runs out. When you play complete product, you can exceed the project time by 100% at most. Try to use the project time optimally.", when : 1 },
			textInterfaceProjectClient : { text : "Customer", when : 0 },
			textInterfaceProjectClientTooltip : { text : "Satisfaction of your customer. The customer expects effective work, high product quality, and low project risk. If the customer is dissatisfied too long, he can cancel the project and YOU LOSE!", when : 1 },
			textInterfaceProjectTeam : { text : "Team", when : 0 },
			textInterfaceProjectTeamTooltip : { text : "Satisfaction of your team. Your team expects effective progress, high productivity, and low project risk. If the team is dissatisfied too long, they can leave the project and YOU LOSE!", when : 1 },
			textInterfaceProjectRisk : { text : "Risk", when : 0 },
			textInterfaceProjectRiskTooltip : { text : "Overall level of project risk. It indicates the possibility of project failure. The customer and the team do not like high risk. Eliminate threats and exploit opportunities to reduce the overall project risk.", when : 1 },
			textInterfaceProjectScore : { text : "Score", when : 0 },
			textInterfaceProjectScoreTooltip : { text : "Your game score. You gain points for product development, eliminating threats, exploiting opportunities, keeping high product quality, high customer and team satisfaction, and low overall risk. Get the most points and become the best project manager!", when : 1 },
			
			textProductValueImgTooltip : { text : "Value for customer - influences project scope", when : 1 },
			textProductQualityImgTooltip : { text : "Influences project quality", when : 1 },
			textProductSizeImgTooltip : { text : "Effort", when : 1 },
			textResourceProductivityImgTooltip : { text : "Resource productivity", when : 1 },
			textRiskInfluenceImgTooltip : { text : "Risk influence", when : 1 },
			textAutopause : { text : "Autopause", when : 0 },
			
			textInterfaceTimeUnitDay : { text : "day", when : 0 },
			textInterfaceTimeUnitWeek : { text : "week", when : 0 },
			textInterfaceTimeUnitMonth : { text : "month", when : 0 },
			textInterfaceTimeUnitYear : { text : "year", when : 0 }
		}
	}
	this.choosenLanguage = "pl";
}
/* @arg language = pl, en 
 * @return pozostałe, które są ładowane w trakcie gry
 * W taki sposób, aby zaoszczędzić trochę na pamięci
 */
Language.prototype.switchLanguage = function(language) {
	if(!this.properties.hasOwnProperty(language)) language = "pl";
	this.choosenLanguage = language;
	var loadedLater = {};
	this.loadedLaterTooltips = {};
	for(var languageProperty in this.properties) {
		if(languageProperty.toString() == language.toString()) {
			var lang = this.properties[languageProperty];
			for(var fieldId in lang) {
				if(!lang.hasOwnProperty(fieldId)) continue;
				if(lang[fieldId].when == 0) {
					$("#"+fieldId).html(lang[fieldId].text.replace(/\\n/g,'<br/>'));
				}
			}
			break;
		}
	} 	
}
	
Language.prototype.getTooltipsText = function(language) {
	var loadedLaterTooltips = {};
	for(var fieldId in this.properties[this.choosenLanguage]) {
		if(fieldId.indexOf("Tooltip") > -1) {
			loadedLaterTooltips[fieldId] = this.properties[this.choosenLanguage][fieldId];
		}
	}
	return loadedLaterTooltips;
}

Language.prototype.getDialogsText = function(language) {
	var loadedLaterDialogs = {};
	for(var fieldId in this.properties[this.choosenLanguage]) {
		if(fieldId.indexOf("textPopup") > -1) {
			loadedLaterDialogs[fieldId] = this.properties[this.choosenLanguage][fieldId];
		}
	}
	return loadedLaterDialogs;
}
/* zmiennej globalne */
globalLanguage = new Language();
mainContainer = null;
defaultLanguage = "pl";
log = log4javascript.getLogger();
ajaxAppender = new log4javascript.AjaxAppender("php/writetolog.php");
log.addAppender(ajaxAppender);

function getCssTaskProgress(progress) {
	var css = 'linear-gradient(to left, #636363 :progress%, blue 0%)';
	return css.replace(':progress',progress);
}

function getProductLi(product) {
	var result = '<li id='+product.id+' class="draggable interactable product">' + product.name + 
	'<span class="assignedResource"></span><span class="close ui-icon ui-icon-close"></span>'
	+ '<span class="right">'+ product.value + ' <img src="img/cubeValue.png" class="smallImg textProductValueImg" />   ' 
	+ product.quality + ' <img src="img/starQuality.png" class="smallImg textProductQualityImg" /><span class="taskSize"> ' 
	+ Math.round(product.size-product.size*product.progress/100) 
	+ ' </span><img src="img/hammer-redSize.png" class="smallImg textProductSizeImg" /></span></li>'; 			
	return result;
}

function getSolutionLi(solution) {
	var result = '<li id='+solution.id+' class="draggable interactable solution">' + solution.name + 
	'<span class="assignedResource"></span><span class="close ui-icon ui-icon-close"></span>' 
	+ '<span class="right textProductSizeImg"><span class="taskSize">'
	+ Math.round(solution.size-solution.size*solution.progress/100) 
	+' </span><img src="img/1.png" class="smallImg" /></li>';
	return result;
}
function getRiskLi(risk) {
	var impacts = '';
	for(var i =0; i<risk.impact;i++) {
		impacts += '!';
	}
	var result = '<li id='+risk.id+' class="ui-state-default risk"' +
	'style="background:'+risk.getColor()+' !important;"><span>' +
	risk.name + '</span><span class="right textRiskInfluenceImg"><span class="riskChance">'+ 
	Math.floor(risk.chance) +'%</span> <span class="riskImpact textBorder">'+
	impacts+'</span></span>' +'</li>';
	return result;
}

function getResourceLi(recource) {
	var result = '<li id='+recource.id+' class="resource">' +  recource.name + '<span class="right textResourceProductivityImg"><span id="resourceProductivity">' 
	+ Math.round(recource.productivity) + '</span> <img src="img/hammer-blueProductivity.png" class="smallImg" /></span></li>'; 			
	return result;
}

function getResourceProducitvityEffect(change) {
	var result = '<span class="' + (change > 0 ? 'green' : 'red') + '">';
	result += ' (' + (change > 0 ? '↑' : '↓') + Math.abs(change) + ')';
	result += '</span>';
	return result;
}

function changeRiskLiChanceAndColor(risk) {
	$("#risks #"+risk.id).css('background-color',risk.getColor());
	$("#risks #"+risk.id+' .right .riskChance').text(Math.floor(risk.chance)+"%");
}

function setTooltips(language) {
	for(var id in language) {
		if(!language.hasOwnProperty(id) && !id.contains('Tooltip')) continue;
		var idx = id.replace('Tooltip','');
		$('#'+idx).prop('title',language[id].text);
		$('#'+idx).addClass('withTooltip');
	}
	$(".withTooltip").tooltip({show: {delay: 300}});
}

function setUnitSpan(unitTextId) {
	var span = '<span id="'+unitTextId+'"></span>: <span id="valueInterfaceTimeUnit">0</span>';
	$('#progress5Text').append(span);
	$('#'+unitTextId).text(globalLanguage.properties[globalLanguage.choosenLanguage][unitTextId].text);
}
function changeLanguage(language) {
	$($('.selectedLanguage')[0]).removeClass('selectedLanguage');
	globalLanguage.switchLanguage(language);
	$("#"+language).addClass('selectedLanguage');
	var combo = document.getElementById("welcomeLowerComboBox");
	var length = combo.options.length;
	for (i = 0; i < length; i++) {
		combo.options[0] = null;
	}
	var videoSource = globalLanguage.properties[language].textWelcomeTutorial.text;
    $('#tutorialVideoPlayer').attr("src",videoSource);
	
	var schemes = globalLanguage.properties[language].schemes;
	if(schemes!=undefined) {
		for(var i=0;i<schemes.length;i++) {
			var option = document.createElement("option");
			option.text = schemes[i].expanded;
			option.value = schemes[i].id;
			try {
				combo.add((option), null); //Standard 
			}catch(error) {
				combo.add(option); // IE only
			}
		}
	}
	setTooltips(globalLanguage.getTooltipsText());
}
function showStartPopup(text) {
	$('html').addClass('overlay');
	$('#textStartPopupUpper').html(text);
	$('#startPopup').fadeIn();
}
function showTutorialPopup(tutorialHtml) {
	$('html').addClass('overlay');
	$('#tutorialPopup').fadeIn();
}
function setImgTooltips() {
	language = globalLanguage.getTooltipsText();
	for(var id in this.language) {
		if(!this.language.hasOwnProperty(id) && !id.contains('Tooltip')) continue;
		if(id.indexOf('Img') != -1) {
		var classx = id.replace('Tooltip','');
		$('.'+classx).prop('title',language[id].text);
		$('.'+classx).addClass('withTooltip');
		}
	}
	$(".withTooltip").tooltip({show: {delay: 300}});
}

/* Ładowane na początku */
$(function() {
	$( "ul, li" ).disableSelection();
	$( "#progressbar" ).progressbar().css({"visibility": "hidden"});
	$( "#icons li" ).hover(
			function() {
				$( this ).addClass( "ui-state-hover" );
			},
			function() {
				$( this ).removeClass( "ui-state-hover" );
			}
	);
	$( ".product, .task, .solution" ).hover(
			function() {
				$( this ).addClass("highlighted");
			},
			function() {
				$( this ).removeClass("highlighted");
			}
	);
	$( "#dialog" ).dialog({
		autoOpen: false,
		width: 400,
		close: function() { $('html').removeClass('overlay'); },
		buttons: [
			{
				text: "Ok",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	
	/* Inicjalizacja gry (START) */
	var request = new XMLHttpRequest();
	request.open("GET", "php/scheme.php?id=info", true);
	request.send(null);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			json = request.responseText;
			json = JSON.parse(json);
			for(var lang in json) {
				for(var field in json[lang]) {
					globalLanguage.properties[lang][field] = json[lang][field];
				}
			}
			changeLanguage(defaultLanguage);
			$('#preloader').fadeOut();
			$('#allPanelWelcome').fadeIn();
		}
	}
	/* Inicjalizacja gry (END) */
	
	$('#startPopupLowerButton').click(function() {
		$('#startPopup').fadeOut();
		$('html').removeClass('overlay');
	});
	
	$('#panelWelcomeLowerStartGameButton').click(function() {
		
		var schemeId = document.getElementById("welcomeLowerComboBox").value;
		var tryb = $("input[name=panelWelcomeLowerRadioButtons]:checked").val();
		if(schemeId == undefined || schemeId == "") return;
		mainContainer = new MainContainer();
		$('#allPanelWelcome').fadeOut("slow",function() {
			$('#preloader').fadeIn();
			mainContainer.init(schemeId,tryb,function() {
				$('#preloader').fadeOut();
				$('#allPanelGame').fadeIn("slow");
			});
			
		});
	});
	$('#tutorialPopupLowerButton').click(function() {
		$('#tutorialPopup').fadeOut();
		var videoSource=$('#tutorialVideoPlayer').attr("src");
	    $('#tutorialVideoPlayer').attr("src",videoSource); //zatrzyma wideo, rozwiazanie niezalezne od zmian API
		$('html').removeClass('overlay');
	});
	$('#howToPlayButton').click(function() {
		$('html').addClass('overlay');
		$('#tutorialPopup').fadeIn("slow");
	});
	var languages = $('#languagesAvailable').children();
	for(var i=0; i<languages.length;i++){
		$('#'+languages[i].id).click(function() {
			changeLanguage(this.id);
		});
	}
});

function MainContainer() {
	this.productsFactory = new ProductsFactory(this);
	this.risksFactory = new RisksFactory(this);
	this.playFactory = new PlayFactory(this);
	this.tasksFactory = new TasksFactory(this);
	this.solutionsFactory = new SolutionsFactory(this);
	this.resourcesFactory = new ResourcesFactory(this);
	this.tryb = 0;
}

MainContainer.prototype.init = function(schemeId, tryb, whenReady) {
	Config = new Config();
	uniqueId = getUniqueUserId();
	
	this.tryb = tryb;
	/*Wczytywanie z bazy */
	var json = "";
	var request= new XMLHttpRequest();
	request.open("GET", "php/scheme.php?id="+schemeId, true);
	request.send(null);
	var self = this;
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			json = request.responseText;
			json = JSON.parse(json);

			showStartPopup(json.Project[0].Project.replace('\n','<br>'));
			
			Config.loadConfig(json.Config);
			self.playFactory.ticket();
			
			self.productsFactory.loadProducts(json.Features);
			self.productsFactory.showProducts();
			
			self.risksFactory.loadRisks(json.Risks);
			
			self.solutionsFactory.loadSolutions(json.Actions);
			self.solutionsFactory.showSolutions();
			
			self.resourcesFactory.loadResources(json.Resources);
			self.resourcesFactory.showResources();
			
			setImgTooltips();
			
			self.tasksFactory.initTasks();
			self.productsFactory.initProducts();
			self.solutionsFactory.initSolutions();
			self.playFactory.initParametersFromProject(json.Project[0]);
			self.playFactory.logMaker.getStartData();

			$("#controls").on("click", "#progressbar", (function() {self.playFactory.changeState();}));

			whenReady();
		}
	};
	
	
	
}

function getUniqueUserId() {
	/*funkcja z http://guid.us/GUID/JavaScript do wygenerowania mocno unikalnego id dla danego użytkownika*/
	function S4() {
	    return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
	}	
	return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
}

/* zwraca potrzebne informacje do wyświetlenia początkowego menu */
function getAllProjectsInfo() {
	
}
function PlayFactory(parent) {
	//pobiera wartość z configa przy inicjalizacji
	this.pauseClicked = false;
	this.currentDate; //data wyrazona w ilosciach godzin, które mineły od początku gry
	this.completion = 0;
	this.score = 0;
	this.customerSatisfaction = 5;
	this.teamHappiness = 5;
	this.quality = 6; // Wartości od 1 do 11
	this.parent = parent;
	this.previousCompletion = 0;
	this.riskFigtingPercentage = 10;
	this.productFactory = parent.productsFactory;
	this.maxScenarioTime = 0; // wyrazone w jednostkach
	this.startingProductivity = 0;
	this.unhappyTeamDays = 0;
	this.unhappyCustomerDays = 0;
	this.minTeamHappiness = 3; 
	this.minCustomerSatisfaction = 3;
	this.dialogsDict = globalLanguage.getDialogsText();
	this.pauseScorePenalty = 100;
	this.logMaker = new LogMaker(this);
	this.allRisksMaterialisedEverCount = 0;
	this.allThreatsMaterialisedEverCount = 0;
	this.lastPauseStop;
	this.lastPauseStart;
	this.gameRating;
	this.isAutopause = false;
	
}

PlayFactory.prototype.getTimeProcentage = function() {
	var result = ( this.currentDate / this.maxScenarioTime ) * 100;
	if (result < 1) result = 1;
	return result;
}

PlayFactory.prototype.updateStatistics = function() {
	this.score = Config.returnScore(this.score,this.previousCompletion,this.productFactory.getAllProgressInPoints(),this.teamHappiness,this.customerSatisfaction,this.riskFigtingPercentage);
	this.previousCompletion = this.completion;
}

// Aktualizacja statystyk - głównie w lewym górnym panelu
PlayFactory.prototype.updateStatisticsNow = function(value) {
	var self = this;
	// Zakres
    self.completion = value;
    $('#progress1Text').text(Math.floor(self.completion) + "%");
    $('#progressbar').progressbar("option", "value", Math.floor(self.getCurrentProgress())); 
    $('#progress1').progressbar("option", "value", Math.floor(self.completion));
    this.actualizeColorOfProgressbar("#progressbar", "#66b3ff");
    this.actualizeColorOfProgressbar("#progress1", "#0080ff");
    
    // Wynik
    this.updateScore();
    
    // Klient
    $('#progress2').progressbar("option", "value", self.customerSatisfaction*18+10);
    
    // Jakość
    this.actualizeQuality();
    
    // Zespół
    $('#progress4').progressbar("option", "value", self.teamHappiness*18+10);
    
    // Czas
    $('#valueInterfaceTimeUnit').text(Math.floor(self.currentDate/24));
    $('#progress5').progressbar("option", "value", Math.floor(self.currentDate / self.maxScenarioTime * 100 % 100));
    if( self.currentDate / self.maxScenarioTime * 100 <= 100 ) {
    	$( "#progress5" ).css({
        	"background": "white"
        });
    	this.actualizeColorOfProgressbar("#progress5", "#0080ff");
    }
    else if( self.currentDate / self.maxScenarioTime * 100 > 100 ) {
    	$( "#progress5" ).css({
        	"background": "#0080ff"
        });
    	this.actualizeColorOfProgressbar("#progress5", "#FF0000");
    }
    
    // Ryzyko
    $('#progress6Text').text(Math.floor(self.riskFigtingPercentage) + "%");
    $('#progress6').progressbar("option", "value", Math.floor(self.riskFigtingPercentage));
    
    this.actualizeColorOfAllProgressbars(self.customerSatisfaction*20, self.teamHappiness*20, 100-10);
}

PlayFactory.prototype.actualizeQuality = function() {
	var middle = 6;
	var tmp = middle-this.quality;
	for (var int = 1; int <= 11; int++) {
		if(tmp > 0 && this.quality <= int && int < middle) {
			$( "#quality" + int ).progressbar({value: 100});
		} else if(tmp < 0 && middle < int && int <= this.quality) {
			$( "#quality" + int ).progressbar({value: 100});
		} else {
			$( "#quality" + int ).progressbar({value: 0});
		}
	}
	$( "#quality" + middle ).progressbar({value: 100});
}

PlayFactory.prototype.actualizeColorOfAllProgressbars = function(value2,value4,value6) {
    this.actualizeColorOfProgressbarOnValue('#progress2',value2);
    this.actualizeColorOfProgressbarOnValue('#progress4',value4);
    this.actualizeColorOfProgressbarOnValue('#progress6',value6);
}

PlayFactory.prototype.actualizeColorOfProgressbar = function(progressbar,color) {
    progress = $( progressbar );
    progressValue = progress.find( ".ui-progressbar-value" );
    progressValue.css({
    	"background": color
    });
}

PlayFactory.prototype.actualizeColorOfProgressbarOnValue = function(progressbar,valuePercentage) {
    if( valuePercentage <= 20 ) {
    	this.actualizeColorOfProgressbar(progressbar, "#ff8080");
    }
    else if( valuePercentage <= 40 ) {
    	this.actualizeColorOfProgressbar(progressbar, "#ff8040");
    }
    else if( valuePercentage <= 60 ) {
    	this.actualizeColorOfProgressbar(progressbar, "#ffff80");
    }
    else if( valuePercentage <= 80 ) {
    	this.actualizeColorOfProgressbar(progressbar, "#80ff80");
    }
    else {
    	this.actualizeColorOfProgressbar(progressbar, "#25df20");
    }
}

PlayFactory.prototype.updateCustomerSatisfaction = function() {
	this.customerSatisfaction = Config.returnCustomerSatisfaction(this.completion,this.getTimeProcentage(),this.quality-1,10,this.riskFigtingPercentage);
}

PlayFactory.prototype.updateTeamHappiness = function() {
	this.teamHappiness = Config.returnTeamHappiness(this.startingProductivity, this.parent.resourcesFactory.getAllProductivity(), this.completion, this.getTimeProcentage(), this.riskFigtingPercentage);
}

PlayFactory.prototype.setQualityProgressbarColors = function() {
	this.actualizeColorOfProgressbar("#quality1", "#800000");
	this.actualizeColorOfProgressbar("#quality2", "#CF0000");
	this.actualizeColorOfProgressbar("#quality3", "#FF3300");
	this.actualizeColorOfProgressbar("#quality4", "#FF9900");
	this.actualizeColorOfProgressbar("#quality5", "#FFDF00");
	this.actualizeColorOfProgressbar("#quality6", "#FFFF00");
	this.actualizeColorOfProgressbar("#quality7", "#D7FC03");
	this.actualizeColorOfProgressbar("#quality8", "#98FC03");
	this.actualizeColorOfProgressbar("#quality9", "#33FF00");
	this.actualizeColorOfProgressbar("#quality10", "#33CC00");
	this.actualizeColorOfProgressbar("#quality11","#339900");
}

PlayFactory.prototype.ticket = function () {
	var self = this;
	
	self.currentDate = 0;
	self.pauseClicked = true;
	
	// INICJOWANIE PROGRESSBARÓW
	$( "#progressbar" ).css({"visibility": "visible"});
    $('#progressbar').progressbar("option", "value", 0);

    var progressbar1 = $( "#progress1" );
    progressbar1.progressbar({value: false});
    var progressbar2 = $( "#progress2" );
    progressbar2.progressbar({value: false});
    var progressbar4 = $( "#progress4" );
    progressbar4.progressbar({value: false});
    var progressbar5 = $( "#progress5" );
    progressbar5.progressbar({value: false});
    var progressbar6 = $( "#progress6" );
    progressbar6.progressbar({value: false});
    
    // INIT QUALITY PROGRESSBAR
    for (var int = 1; int <= 11; int++) {
    	$( "#quality" + int ).progressbar({value: false});
	}
    this.setQualityProgressbarColors();
    this.createQuitDialog();
	$('#backToWelcome').click(function() { 
		self.openQuitDialog(); 
		});
    
	self.customerSatisfaction = Config.initialCustomerSatisfaction;
	self.teamHappiness = Config.initialTeamHappiness;
	
	var pointPerTickets = Config.pointPerTickets; // x ticketów = wykonanie 1 punktu
	
	var ticketCounter = 0;
	var previousTicketCounter = 0;
	
	var maxUnhappyTeamDays = Config.maxUnhappyTeamDays;
	var maxUnhappyCustomerDays = Config.maxUnhappyCustomerDays;
	
	self.minCustomerSatisfaction = Config.minCustomerSatisfaction;
	self.minTeamHappiness = Config.minTeamHappiness;
	
	self.pauseScorePenalty = Config.pauseScorePenalty;
	
	self.updateStatisticsNow(0);
	
	setInterval(function() {
		if(self.pauseClicked === false){
			// Obliczanie liczby dni, godzin i minut na podstawie realnej liczby sekund
			var days = Math.floor(self.currentDate / 24) % 30;
			var hours = Math.floor(self.currentDate) % 24;
			
		    var oldDate = Math.floor(self.currentDate/24);
			// Zwiększanie czasu 
			self.currentDate = self.currentDate + 24 / Config.ticketsPerTimeUnit;
			
			if(oldDate != Math.floor(self.currentDate/24)) {
				if(self.teamHappiness <= self.minTeamHappiness) {
					self.unhappyTeamDays += 1;
				}
				else {
					self.unhappyTeamDays = 0;
				}
				if(self.customerSatisfaction <= self.minCustomerSatisfaction) {
					self.unhappyCustomerDays += 1;
				}
				else {
					self.unhappyCustomerDays = 0;
				}
			}
			
			//Sprawdzanie warunkow konca gry
			if(self.unhappyTeamDays >= maxUnhappyTeamDays) {
		    	var title = self.dialogsDict["textPopupEndTitleLost"].text;
		    	var text = self.dialogsDict["textPopupEndContentsLostTeam"].text;
		    	self.gameOverDialog(title, text, 0);
			}
			else if(self.unhappyCustomerDays >= maxUnhappyCustomerDays) {
		    	var title = self.dialogsDict["textPopupEndTitleLost"].text;
		    	var text = self.dialogsDict["textPopupEndContentsLostClient"].text;
		    	self.gameOverDialog(title, text, 0);
			}
			else if(self.riskFigtingPercentage == 100) {
		    	var title = self.dialogsDict["textPopupEndTitleLost"].text;
		    	var text = self.dialogsDict["textPopupEndContentsLostRisk"].text;
		    	self.gameOverDialog(title, text, 0);
			}
		    
		    //Przeliczanie produktywności do zadań
		    var tasksList = self.parent.tasksFactory.tasksList;
		    tasksList.map(function(task) {
		    	task.resources.map(function(resource){
		    		task.addProgress(resource.productivity);
		    	});
		    });
		    
		    //aktualizacja postępu
		    var value = self.parent.productsFactory.getAllProgress(); // aktualny postęp prac
		    
		    //ryzyko
		    self.parent.risksFactory.checkAndIncreaceRiskChance();
		    $.each(self.parent.risksFactory.visibleRisksList,function(i,risk){
		    
		    	if(risk == undefined) return;
		    	risk.ticketsLeft --;
		    	if(risk.mutex == false && risk.ticketsLeft <= 0 && risk.visible == true && risk.succeed == false) { //czas na wykonanie ryzyka(szansa)
		    		if(risk.mutex == true) return;
		    		risk.mutex = true;
		    		if(Config.debug) console.log(i+"Czas minął, wykonanie ryzyka(szansa):" +risk.toString());
		    		risk.rollEvent(); //wykonanie ryzyka(szansa)
		    		if(risk.succeed == true) {
		    			
		    			self.allRisksMaterialisedEverCount += 1;
		    			if(!risk.isGood) {
		    				self.allThreatsMaterialisedEverCount += 1;
		    			}
		    			
		    			var jRisk = $("#risks #"+risk.id);
			    		var height = jRisk.height();
			    		var maxHeight = Math.floor(height * 1.30);
			    		if(jRisk.css('font-size') == undefined) 
			    			console.log("error");
			    		var fontSize = jRisk.css('font-size').replace('px','');
			    		var maxFontSize = Math.floor(fontSize * 1.3);
			    		
			    		jRisk.animate({ height: maxHeight, fontSize:maxFontSize }, 500 , function(){
			    			jRisk.animate({ height: height, fontSize:fontSize }, 200,function(){
				    			risk.removeVisible(risk.isGood);
				    		} );
			    		});
		    					    			
		    		} else {
		    			risk.ticketsLeft = risk.tickets/2;  //ponownie wywołanie ryzyka za 1/2 ticketów
		    			risk.mutex = false;
		    		}
		    		
		    	}
		    });		    

	    	self.updateCustomerSatisfaction();
	    	self.updateTeamHappiness();
		    
	    	self.updateStatisticsNow(value);
	        self.actualizeColorOfAllProgressbars(self.customerSatisfaction*20, self.teamHappiness*20, 100-self.riskFigtingPercentage);
		    
			// Działanie TAKTów o których mówił p. Miler
		    ticketCounter = self.parent.productsFactory.getAllProgress();
		    if( ticketCounter - previousTicketCounter >= Config.takt ){
		    	previousTicketCounter = ticketCounter;
		    	
		    	// Aktualizacja statystyk
		    	self.updateStatistics();
		    }
		    
		    if(value == 100) {
		    	var title = self.dialogsDict["textPopupEndTitleSucceed"].text;
		    	var text = self.dialogsDict["textPopupEndContentsSucceed"].text;
		    	self.gameOverDialog(title, text, 1);
		    }
		    else if(self.parent.tryb == 1 && self.currentDate >= self.maxScenarioTime ) {
		    	var title = self.dialogsDict["textPopupEndTitleTimeOut"].text;
		    	var text = self.dialogsDict["textPopupEndContentsTimeOut"].text;
		    	self.gameOverDialog(title, text, 2);
		    }		    
		    else if(self.parent.tryb == 2 && self.currentDate >= self.maxScenarioTime * 2 ) {
		    	var title = self.dialogsDict["textPopupEndTitleTimeOut"].text;
		    	var text = self.dialogsDict["textPopupEndContentsTimeOut"].text;
		    	self.gameOverDialog(title, text, 2);
		    }
		    
		  //autopauza gdy brak roboty lub nowe ryzyko w miedzyczasie
		    if(self.parent.tasksFactory.tasksList.length == 0 || self.isAutopause === true) { 
		    	self.doPause(true);
		    	return;
		    }
		}
	    
	}, Config.milisecondsPerTicket);
}

PlayFactory.prototype.changeState = function(isAutopause) {
	if(this.pauseClicked === true) this.undoPause(isAutopause);
	else this.doPause(isAutopause);
}

/*przycisk Pause */
PlayFactory.prototype.doPause = function(isAutopause) {
	this.pauseClicked = true;
	this.lastPauseStart = new Date();
	$('#progressText').attr('src','img/play.png');
	this.parent.tasksFactory.keepAllocation = false;
	$("#resources").sortable("enable");
	$("#tasks").sortable("enable");
	$("#solutions").sortable("enable");
	$("#products").sortable("enable");
	$("#tasks .close").css({"visibility": "visible"});
	this.parent.tasksFactory.hideAssignedResourcesShorts();
	
	if(isAutopause != true) {
		this.score -= this.pauseScorePenalty;
		this.updateScore();
	}
	else {
    	$('#autopausePopup').fadeIn(100).fadeOut(1600);
	}
}
/* przycisk Play */
PlayFactory.prototype.undoPause = function() {
	if(this.parent.tasksFactory.tasksList.length == 0) return;
	$('#progressText').attr('src','img/pause.png');
	this.parent.resourcesFactory.freeAllResources();
	this.parent.resourcesFactory.reorderResources();
	this.parent.tasksFactory.reorderTasks();
	this.parent.tasksFactory.allocateResources();
	this.pauseClicked = false;
	$("#resources").sortable("disable");
	$("#tasks").sortable("disable");
	$("#solutions").sortable("disable");
	$("#products").sortable("disable");
	$("#tasks .close").css({"visibility": "hidden"});
	this.logMaker.logGameState();
	this.lastPauseStop = new Date();//.getTime();
	this.isAutopause = false;
}

PlayFactory.prototype.initParametersFromProject = function(json) {
	var unit = json.Unit.toLowerCase();
	var textInterfaceTimeUnit = "textInterfaceTimeUnit";
	textInterfaceTimeUnit += unit.charAt(0).toUpperCase() + unit.slice(1);
	setUnitSpan(textInterfaceTimeUnit);
	this.maxScenarioTime = json.Time * 24;
	$('#textInterfaceSchemeTitle').html(json.Name);
}

PlayFactory.prototype.gameOverDialog = function(title, text, won) {
	 this.pauseClicked = true;
	 $("#tasks .close").click();
	 $("#tasks").sortable("enable");
	 this.parent.tasksFactory.keepAllocation = false;
	 this.parent.resourcesFactory.freeAllResources();
	 
	 var scoreText = $("#textInterfaceProjectScore").text() + ": <span id='endScore'>" + $('#valueInterfaceProjectScore').text() + "</span>";
	 
	 var titleClass;	 
	 if (won == 0) {titleClass = "class='lost'"}
	 else if(won == 1) {titleClass = "class='won'"}
	 
	 var starsText = this.dialogsDict["textPopupEndContentsStars"].text;
	 var feedbackStars = "<img id='ocena' src='img/ocena0.gif' usemap='#stars'><map name='stars'>" +
	 	"<area id='stars1' class='stars' shape='rect' coords='0,0,54,60'>" +
	 	"<area id='stars2' class='stars' shape='rect' coords='55,0,104,60'>" +
	 	"<area id='stars3' class='stars' shape='rect' coords='105,0,154,60'>" +
	 	"<area id='stars4' class='stars' shape='rect' coords='155,0,204,60'>" +
	 	"<area id='stars5' class='stars' shape='rect' coords='205,0,260,60'>" +
	 	"</map>";
	 
	 $('#dialog').append("<p class='center'><span id='popupTitle' " + titleClass + ">" + title + "</span><br\>" + text + "<br\>" + scoreText + "</p><br\>");
	 $("#mainMenu").clone().appendTo( "#dialog" );
	 $('#dialog').append("<p class='center'><span id='starsText'>" + starsText + "</span><span id='feedbackStars'>" + feedbackStars + "</span></p>");
	 
	 var self = this;
	 
	 $("#dialog").on("mouseover", ".stars", (function() {self.showStars(this.id);}));
	 $("#dialog").on("click", ".stars", (function() {self.sendStars(this.id);}));
	 $("#dialog").dialog({
		 buttons: [{
			 text: "OK",      
			 click: function() {        
				 $( this ).dialog( "close" );
				 $('#allPanelGame').fadeOut(function() {
					 window.location.href = "";
					 });
				 }    
		 }],
		 dialogClass: "rounded noTitleBar", 
		 height: $(window).height()*0.80,
		 widht: $(window).width()*0.4767,
		 minHeight: $(window).height()*0.34,
		 minWidth: $(window).width()*0.4767
		 });
	 $('#dialog').dialog("open");
	 
}

PlayFactory.prototype.createQuitDialog = function() {
	var self = this;
	var dialogButtons = {};
	var yesButton = this.dialogsDict["textPopupQuitYesButton"].text;
	var noButton = this.dialogsDict["textPopupQuitNoButton"].text;
	
	dialogButtons[yesButton] = function() { 
		$( this ).dialog( "close" ); 
		$('#allPanelGame').fadeOut(function() {
			window.location.href = "";
		 });
		};
	dialogButtons[noButton] = function() {
		$( this ).dialog( "close" ); 
		self.pauseClicked = $('#quitPopup').data("wasPaused");	 
		};
	
	$('#quitPopup').text(this.dialogsDict["textPopupQuitContent"].text);
	$('#quitPopup').dialog({		
		autoOpen: false,
		width: 400,
		close: function() { $('html').removeClass('overlay'); },
		position: { my: "center", at: "center", of: window },
		buttons: dialogButtons,
		dialogClass: "noTitleBar"
	});
}

PlayFactory.prototype.getCurrentProgress = function() {
	var allCurrentProgress = 100* this.parent.tasksFactory.tasksList.length;
	var currentProgress = 0;
	for(var i=0;i<this.parent.tasksFactory.tasksList.length;i++) {
		currentProgress += this.parent.tasksFactory.tasksList[i].progress;
	}
	return currentProgress / allCurrentProgress * 100;
}
PlayFactory.prototype.changeRiskFigtingPercentageBy = function(value) {

	this.riskFigtingPercentage += value;
	if(this.riskFigtingPercentage > 100) {
		this.riskFigtingPercentage = 100;
	}
	if(this.riskFigtingPercentage < 10) {
		this.riskFigtingPercentage = 10;
	}	
}

PlayFactory.prototype.updateScore = function() {
	$('#valueInterfaceProjectScore').text(Math.floor(this.score));
}
PlayFactory.prototype.openQuitDialog = function() {
	var wasPaused = this.pauseClicked;	
	this.pauseClicked = true;
	
	$('html').addClass('overlay');
	$('#quitPopup').data("wasPaused", wasPaused).dialog("open");
}

PlayFactory.prototype.showStars = function(starsId) {
	var starsNumber = starsId.slice(-1);
	document.getElementById('ocena').src='img/ocena' + starsNumber +'.gif';
}

PlayFactory.prototype.sendStars = function(starsId) {
	var starsNumber = starsId.slice(-1);
	$("#dialog").off("click", ".stars");
	$("#dialog").off("mouseover", ".stars");
	this.gameRating = starsNumber;
	this.logMaker.logGameRating();
}

PlayFactory.prototype.makeAutopause = function() {
	this.isAutopause = true;
}
function ProductsFactory(parent) {
	this.productsList = [];
	this.parent = parent;
	this.allSize = 0;
}

ProductsFactory.prototype.initProducts = function() {
	var self = this;
	
	$( "#products" ).sortable({connectWith: "#tasks", 
		over: function (event, ui) {
			if(ui.item.hasClass("solution")){
				ui.item.addClass("youShallNotPass");
			}
			else if(ui.item.hasClass("product")){
				ui.item.removeClass("youShallNotPass");
			}
		},
		receive: function (event, ui) {
			if(ui.item.hasClass("product")){
				self.parent.tasksFactory.freeTaskResources(ui.item, false);
			}
		}
	});
}

ProductsFactory.prototype.loadProducts = function (products) {
	var self = this;
	
	$.each(products, function( i, item ) {     
		var product = new Product(item, self);	
		self.productsList.push(product);
		self.allSize += product.size;
	});
	
}

ProductsFactory.prototype.showProducts = function() {
	var result = "";
	$.each(this.productsList, function( i, item ) {  
		if (item.visible && (item.isOnTaskList == false)) {
			result += getProductLi(item);
		}
	});
	$( "#products" ).html(result);
}

ProductsFactory.prototype.getAllProgress = function () {
	var result = 0;
	var i = this.productsList.length;
	while(i--) {
		result += this.productsList[i].progress;
	}
	if(this.productsList.length>0) {
		result /= this.productsList.length*100;
		result *= 100;
	}
	return result;
}

ProductsFactory.prototype.getAllProgressInPoints = function () {
	var result = 0;
	var i = this.productsList.length;
	while(i--) {
		result += this.productsList[i].progress;
	}
	return result;
}

ProductsFactory.prototype.getAllProductsSize = function () {
	var result = 0;
	var i = this.productsList.length;
	while(i--) {
		result += this.productsList[i].size;
	}
	return result;
}

ProductsFactory.prototype.getNotDoneProductsCount = function() {
	var result = 0;
	$.each(this.productsList, function( i, item ) {  
		if (item.progress < 100 ) {
			result += 1;
		}
	});
	return result;
}

ProductsFactory.prototype.getProductsWithAssignedResourcesCount = function() {
	var result = 0;
	$.each(this.productsList, function( i, item ) {  
		if (item.resources.length > 0 && (item.isOnTaskList == true)) {
			result += 1;
		}
	});
	return result;
}
function RisksFactory(parent) {
	this.risksList = [];
	this.visibleRisksList = []; //lista ryzyk, które są wyświetlone w interfejsie
	this.riskChance = 0;
	this.riskTime = 1;
	this.riskDisappearTime = 1;
	this.parent = parent;
	this.eventsProductivityMutex = 0;
	this.eventsResourcesMutex;
	this.eventsProgressMutex;
	this.allRisksEverCount = 0;
	this.allThreatsEverCount = 0;
	this.allThreatsRemovedCount = 0;
	this.allOpportunitiesMissedCount = 0;
	this.allThreatsRandomlyDisappearedCount = 0;	
}

RisksFactory.prototype.loadRisks = function (risks) {
	var self = this;
	$.each(risks, function( i, item ) {     
		var risk = new Risk(item,self);	
		self.risksList.push(risk);
	});
}

RisksFactory.prototype.checkAndIncreaceRiskChance = function() {
	
	if(this.riskDisappearTime%(Config.riskDisappearTimePerTickets+1) == 0){
		this.riskDisappearTime = 1; //reset czasu
		var roll = Math.floor((Math.random() * 100) + 1); //losowanie
		if(roll <= Config.riskDisappearChance) {
			roll = Math.floor((Math.random() 
					* (this.visibleRisksList.length-1))); //losowanie ryzyka, które ma być usunięte
			
			var risk = this.visibleRisksList[roll];			
			var idx = this.visibleRisksList.indexOf(risk);
			if(idx < 0) return;
			this.addRiskRandomlyDisappearedCounter(risk.isGood);
			risk.removeVisible(true);
			this.refreshAllVisible();
			if(Config.debug) console.log("Usunięto ryzyko z interfejsu:" + risk.toString());
		}
	} else {
		this.riskDisappearTime++;
	}
	if(this.riskTime%(Config.riskTimePerTickets+1) == 0) {
		this.riskTime = 1; //reset czasu
		this.riskChance = Config.riskChance;
		var roll = Math.floor((Math.random() * 100) + 1); //losowanie
		if(roll <= this.riskChance) { //wyświetlenie szansy na zaistnienie ryzka
			roll = Math.floor((Math.random() 
					* (this.risksList.length-1))); //losowanie ryzyka, które ma być dodane
			
			var risk = this.risksList[roll];			
			if(this.visibleRisksList.indexOf(risk) != -1) {
				if(Config.debug) console.log("To ryzyko jest już na liście: " + risk.toString());
				return; //to ryzyko jest już na liście
			}
			if(risk.currentDelay > 0) {
				risk.currentDelay--;
				return;
			}
			risk.currentDelay = risk.baseDelay-1;
			risk.baseDelay *= risk.baseDelay;
			
			risk.ticketsLeft = Math.floor((Math.random() * 
					Config.riskTicketsLeftInterval[1]) + 
					Config.riskTicketsLeftInterval[0]); //losowanie czasu (ticketów) do zainstnienia ryzyka
			risk.tickets = risk.ticketsLeft;
			
			if(Config.debug) console.log("Dodano ryzyko  do interfejsu:" + risk.toString());
			this.visibleRisksList.push(risk);
			risk.interfaceAppearTime = new Date();
			this.changeVisibleRisksBy(1, risk.isGood);
			this.allRisksEverCount += 1;
			if(!risk.isGood) {
				this.allThreatsEverCount += 1;
				}
			this.refreshAllVisible();
			this.parent.playFactory.makeAutopause();
		}
		
	} else {
		this.riskTime++;
	}
}
RisksFactory.prototype.refreshAllVisible = function () {
	$("#risks").html('');
	this.visibleRisksList.sort(function(a,b) { return parseInt(b.chance) - parseInt(a.chance) } );
	$.each(this.visibleRisksList,function(i,item){
		item.setVisible();
	});
}

RisksFactory.prototype.changeVisibleRisksBy = function(quantity, isRiskGood) {
	if(isRiskGood == false) {		
		this.parent.playFactory.changeRiskFigtingPercentageBy(quantity * 10);
	}
}

RisksFactory.prototype.countVisibleThreats = function() {
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {     
		if(risk.isGood == false) {		
			result += 1;
		}
	});
	return result;
}

RisksFactory.prototype.addThreatsRemovedCounter = function(isRiskGood) {
	if(!isRiskGood){
		this.allThreatsRemovedCount += 1;
	}
}

RisksFactory.prototype.addRiskRandomlyDisappearedCounter = function(isRiskGood) {
	if(!isRiskGood){
		this.allThreatsRandomlyDisappearedCount += 1;
	}
	else {
		this.allOpportunitiesMissedCount += 1;
	}
}

RisksFactory.prototype.getVisibleThreatsProbabilitySum = function() {
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.isGood == false) {
			result += risk.chance;
		}
	});
	return result;
}

RisksFactory.prototype.getVisibleOpportunitiesProbabilitySum = function() {
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.isGood == true) {
			result += risk.chance;
		}
	});
	return result;
}

RisksFactory.prototype.getVisibleThreatsImpactSum = function() { //wykrzykniki
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.isGood == false) {
			result += Number(risk.impact);
		}
	});
	return result;
}

RisksFactory.prototype.getVisibleOpportunitiesImpactSum = function() { //wykrzykniki
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.isGood == true) {
			result += Number(risk.impact);
		}
	});
	return result;
}

RisksFactory.prototype.getDesirablyAffectedRisksOfThisIdCount = function(riskId, addOrSub) {
	var fullRiskId = "risk"+ riskId;
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.id == fullRiskId) {
			if (risk.isGood && addOrSub === true) {
				result += 1;
			}
			else if ((risk.isGood === false) && (addOrSub === false)) {
				result += 1;
			}		
		}
	});
	return result;
}
function SolutionsFactory(parent) {
	this.solutionsList = [];
	this.solutionsOnTasksList = [];
	this.parent = parent;
}

SolutionsFactory.prototype.initSolutions = function() {
	var self = this;
	
	$( "#solutions" ).sortable({connectWith: "#tasks",
		forcePlaceholderSize: false,
	    helper: function(e,li) {
	        copyHelper= li.clone().insertAfter(li);
	        return li.clone();
	    },
	    stop: function() {
	        copyHelper && copyHelper.remove();
	    },
	    over: function (event, ui) {
			if(ui.item.hasClass("product")){
				ui.item.addClass("youShallNotPass");
			}
			else if(ui.item.hasClass("solution")){
				ui.item.removeClass("youShallNotPass");
			}
		},
		receive: function (event, ui) {
			if(ui.item.hasClass("solution")){
				var thisObj = $("#"+ ui.item.attr("id"));
				self.parent.tasksFactory.freeTaskResources(thisObj, false);
				self.deleteFromSolutionsOnTasksList(thisObj.attr("id"));
				thisObj.remove();
			}
		}
	});
}

SolutionsFactory.prototype.loadSolutions = function (solutions) {
	var self = this;
	$.each(solutions, function( i, item ) {     
		var solution = new Solution(item, self);	
		self.solutionsList.push(solution);
	});
}

SolutionsFactory.prototype.showSolutions = function() {
	var result = "";
	$.each(this.solutionsList, function( i, item ) {  
			result += getSolutionLi(item);
	});
	$( "#solutions" ).html(result);
}

SolutionsFactory.prototype.deleteFromSolutionsOnTasksList = function(solutionId) {
	var solutionsOnTasksList = this.solutionsOnTasksList;
	var i = solutionsOnTasksList.length;
	while(i--) {
		if(solutionId == solutionsOnTasksList[i]) {
			solutionsOnTasksList.splice(i, 1);
		};
	};
}
function TasksFactory(parent) {
	this.tasksList = [];
	this.parent = parent;
	this.keepAllocation = false;
	this.counter = 0;
	this.allUniqueSolutionsEverIds = [];
	this.allEffectiveSolutionsEverCount = 0;
	this.allSolutionsEverCount = 0;
}
TasksFactory.prototype.initTasks = function () {
	var self = this;
	$( "#tasks" ).sortable({connectWith: "#products, #solutions", 
		stop: function (event, ui){
			if(ui.item.hasClass("youShallNotPass")){
				$(this).sortable('cancel');
				ui.item.removeClass("youShallNotPass");
			}},
			receive: function (event, ui) {
				copyHelper= null;
				if(ui.item.hasClass("solution")){
					ui.item.attr("id", $(ui.item).attr("id"));
				}
				ui.item.find(".close").css({"visibility": "visible"});
				var task = self.setIsOnTaskList($(ui.item),true);
				if(task != null)	{
					self.tasksList.push(task);
				}
			}
	});
	
	$("#tasks").on("click", "span.close", (function(event, isTaskDone) {
		removeTask( $(this).parent(), (isTaskDone ? isTaskDone : false) );
	}));
	
	function removeTask(thisObj, isTaskDone) {
		thisObj.fadeOut(function() {
			thisObj.detach();
			
			if (thisObj.hasClass("product") && isTaskDone==false) {
				thisObj.appendTo("#products");
				thisObj.fadeIn();
			}
			else if (thisObj.hasClass("solution")) {
				self.parent.solutionsFactory.deleteFromSolutionsOnTasksList(thisObj.attr("id"));
				thisObj.remove();
			}
			
			
		});
		self.freeTaskResources(thisObj, isTaskDone);
	}
}
/* uporządkowanie listy zadan */
TasksFactory.prototype.reorderTasks = function () {
	
	var orderedTasksList = $("#tasks").children();
	var newTasksList = [];
	var idsList = [];
	
	$(orderedTasksList).each(function(index, value) {
		idsList.push(value.id);
	});
	
	while(idsList.length > 0) {
		for(var i=0;i<this.tasksList.length;i++) {
			if(idsList[0] == this.tasksList[i].id) {
				/* przy okazji wyczyszczenie danego taska */
				$("#tasks #" + this.tasksList[i].id + " .assignedResource").html("");
				this.tasksList[i].resources = [];
				
				newTasksList.push(this.tasksList[i]);
				idsList.shift();
			}
		}
	}
	this.tasksList = newTasksList;	
}
TasksFactory.prototype.allocateResources = function () {
	//przydzielenie zasobów do zadań
	this.keepAllocation = true;
	var self = this;
	
	var intvl = setInterval(function() {
	    if (!self.keepAllocation) { 
	        clearInterval(intvl);
	    }
	    
	    var freeResourcesList = self.parent.resourcesFactory.getFreeResources();
	    if(freeResourcesList.length==0) return;
	    
	    for(var i=0;i<self.tasksList.length;i++) {
	    	if(self.tasksList[i].resources == 0) {
	    		var resource= freeResourcesList.shift();
	    		resource.task = self.tasksList[i];
	    		self.tasksList[i].resources.push(resource);
	    		resource.currentlyUsed = true;
	    		var shortcut = "(" + resource.name.substr(0,3) + ")";
	    		$("#tasks #" + self.tasksList[i].id + " .assignedResource").html(shortcut);
	    		if(freeResourcesList.length==0) break;
	    	}
	    }  
	    
	}, 100);
}
TasksFactory.prototype.freeTaskResources = function(thisObj, isTaskDone) {
	thisObj.find(".close").css({"visibility": "hidden"});
	thisObj.find(".assignedResource").html("");
	
	var i = this.tasksList.length;
	while(i--) {
		if(this.tasksList[i].id == thisObj.attr("id")) {
			task = this.tasksList.splice(i,1)[0];
			var freedResourcesList;
			if(isTaskDone) {
				freedResourcesList = task.done();
			} else {
				freedResourcesList = task.resources;
				task.resources = [];				
			}
			this.parent.resourcesFactory.freeResources(freedResourcesList);
			break;
		}
	}
}

TasksFactory.prototype.taskDone = function (task) {
	$("#"+ task.id + " .ui-icon-close").trigger("click", [true]);
}

TasksFactory.prototype.setIsOnTaskList = function (task, boolean) {	
	var container;
	if(task.hasClass("product")) {
		container = this.parent.productsFactory.productsList;
	}
	else {
		container = this.parent.solutionsFactory.solutionsList;
	}
	
	var taskObject;	
	var i = container.length;
	var id = task.attr("id");
	
	while(i--) {
		if(container[i].id == id) {
			taskObject = container[i];
			if(task.hasClass("solution")) {
				var newSolution = {};
				jQuery.extend(newSolution,taskObject);
				newSolution.id += "_" + this.counter;
				task.attr("id", id +  "_" + this.counter);
				this.counter += 1;
				this.parent.solutionsFactory.solutionsOnTasksList.push(newSolution);
				return newSolution;
			}
			else {
				taskObject.isOnTaskList = boolean;
				return taskObject;
			}
		}
	}
}

TasksFactory.prototype.allocateResourcesNOW = function () {
	//przydzielenie zasobów do zadań NATYCHMIAST, tylko na potrzeby logowania
	this.keepAllocation = true;
	var self = this;
	

	    var freeResourcesList = self.parent.resourcesFactory.getFreeResources();
	    if(freeResourcesList.length==0) return;
	    
	    for(var i=0;i<self.tasksList.length;i++) {
	    	if(self.tasksList[i].resources == 0) {
	    		var resource= freeResourcesList.shift();
	    		resource.task = self.tasksList[i];
	    		self.tasksList[i].resources.push(resource);
	    		resource.currentlyUsed = true;
	    		var shortcut = "(" + resource.name.substr(0,3) + ")";
	    		$("#tasks #" + self.tasksList[i].id + " .assignedResource").html(shortcut);
	    		if(freeResourcesList.length==0) break;
	    	}
	    }  
}

TasksFactory.prototype.getTasksWithAssignedResourcesCount = function() {
	var result = 0;
	$.each(this.tasksList, function( i, item ) {  
		if (item.resources.length > 0) {
			result += 1;
		}
	});
	return result;
}

TasksFactory.prototype.getAllTaskAverageProgressInPoints = function () {
	var result = 0;
	var len = this.tasksList.length;
	var i = this.tasksList.length;
	while(i--) {
		result += this.tasksList[i].progress;
	}
	if(len != 0) {
		result = result / len;
	}
	return result;
}

TasksFactory.prototype.getPlannedToDoProductCount = function() {
	var result = 0;
	$.each(this.tasksList, function( i, item ) {  
		if (item.id.indexOf("product") > -1 ) { //tzn. ten task to produkt
			result += 1;
		}
	});
	return result;
}

TasksFactory.prototype.getSolutionsOnTasksListSize = function () {
	var result = 0;
	$.each(this.tasksList, function( i, item ) {  
		if (item.id.indexOf("solution") > -1 ) { //tzn. ten task to solution
			result += item.size;
		}
	});
	return result;
}

TasksFactory.prototype.updateAllSolutionsEverData = function () {
	var self = this;
	$.each(this.tasksList, function( i, item ) {
		var itemId = item.id;
		if (itemId.indexOf("solution") > -1 ) { //tzn. ten task to solution
			self.allSolutionsEverCount += 1;
			if($.inArray(itemId, self.allUniqueSolutionsEverIds) === -1) { //nie mielismy dotad takiego
				self.allUniqueSolutionsEverIds.push(itemId);			
			}
			var desirablyAffectedRisksCount = 0;
			$.each(item.solutionEvents, function( i, solutionEvent ) {
				desirablyAffectedRisksCount += self.parent.risksFactory.getDesirablyAffectedRisksOfThisIdCount(solutionEvent.riskId, solutionEvent.addOrSub);
			});	
			if (desirablyAffectedRisksCount > 0) {
				self.allEffectiveSolutionsEverCount += 1;
			}
		}
	});
}

TasksFactory.prototype.getSolutionsOnTasksListIds = function () {
	var result = [];
	$.each(this.tasksList, function( i, item ) {  
		if (item.id.indexOf("solution") > -1 ) { //tzn. ten task to solution
			result.push(item.id);
		}
	});
	return result;
}

TasksFactory.prototype.getEffectiveSolutionsForThisRiskCount = function (riskId, isGood) {
	var result = 0;
	$.each(this.tasksList, function( i, item ) {
		if (item.id.indexOf("solution") > -1 ) { //tzn. ten task to solution
			$.each(item.solutionEvents, function( i, solutionEvent ) {
				var fullRiskId = "risk"+ solutionEvent.riskId;
				if ((fullRiskId == riskId) && ((isGood === true && solutionEvent.addOrSub === true) || ((isGood === false) && (solutionEvent.addOrSub === false)))) {
					result += 1;
					return false; //to dziala jak break
				}
			});
		}
	});
	return result;
}
/* ukrycie skrotow nazw zasobow na zadaniach */
TasksFactory.prototype.hideAssignedResourcesShorts = function () {
	$.each(this.tasksList, function( i, item ) {
		$("#tasks #" + item.id + " .assignedResource").html("");
	});
}
function ResourcesFactory(parent) {
	this.parent = parent;
	this.resourcesList = [];
	this.initialResourcesList = [];
	this.lastResourceIdIncrement = 0;
	$( "#resources" ).sortable();
}



ResourcesFactory.prototype.loadResources = function (resources) {
	var self = this;
	$.each(resources, function( i, item ) {
		var resource = new Resource(item);	
		self.resourcesList.push(resource);
		self.initialResourcesList.push(resource);
		self.lastResourceIdIncrement++;
	});
	
	self.parent.playFactory.startingProductivity = this.getAllProductivity();
}

ResourcesFactory.prototype.showResources = function(showProductivityChanges) {
	var result = "";
	
	$.each(this.resourcesList, function( i, item ) {  
		result += getResourceLi(item);		
	});
	$( "#resources" ).html(result);
}

ResourcesFactory.prototype.freeResources = function(resources) {
	if(resources) {
		$.each(resources, function( i, item ) {	
			item.currentlyUsed = false;
			item.task = null;
		});
	}
}


ResourcesFactory.prototype.getFreeResources = function() {
	var freeResources = [];
	for(var i=0;i<this.resourcesList.length;i++) {
		if (this.resourcesList[i].currentlyUsed == false) {
			freeResources.push(this.resourcesList[i]);
		}
	}
	return freeResources;
}
ResourcesFactory.prototype.getAllProductivity = function() {
	var result = 0;
	for(var i=0;i<this.resourcesList.length;i++) {
		result += this.resourcesList[i].productivity;
	}
	return result;
}
ResourcesFactory.prototype.freeAllResources = function() {
	this.resourcesList.map(function(resource){
		resource.currentlyUsed = false;
		resource.task = null;
	});
}

ResourcesFactory.prototype.addResourceToList = function (resource,newId) {
	var newResource = new Resource();
	newResource.copy(resource);
	newResource.id = newId;
	this.resourcesList.push(newResource);
	var result = getResourceLi(newResource);
	$('#resources').append(result);
}

ResourcesFactory.prototype.removeResourceFromList = function (i) {
	var removedResource = this.resourcesList.splice(i,1)[0]; //usunięcie z listy
	$('#'+removedResource.id).remove();
	var task = removedResource.task;
	if(task == null) return; // to znaczy ze ten resource nie jest uzywany przez produkty
	for(var i=0;i<task.resources.length;i++) {
		if(task.resources[i].sid == removedResource.sid) { //usunięcie z listy produktu
			task.resources.splice(i,1);
			$("#tasks #" + task.id + " .assignedResource").html("");
			break;
		}
	}
}

/* uporządkowanie listy zasobów */
ResourcesFactory.prototype.reorderResources = function () {
	
	var orderedResourcesList = $("#resources").children();
	var newResourcesList = [];
	var idsList = [];
	
	$(orderedResourcesList).each(function(index, value) {
		idsList.push(value.id);
	});
	
	while(idsList.length > 0) {
		for(var i=0;i<this.resourcesList.length;i++) {
			if(idsList[0] == this.resourcesList[i].id) {
				newResourcesList.push(this.resourcesList[i]);
				idsList.shift();
			}
		}
	}
	this.resourcesList = newResourcesList;
	
}

/* Klasa konstruująca string do logowania */

function LogMaker(parent) {
	this.parent = parent;
	this.logMessage = "";
	
	this.activeRisks = 0;
	this.plannedToDoSolutions = 0;
	this.solutionsWithResourcesAssigned = 0;
}

LogMaker.prototype.getStartData = function() {
	this.startingProductivity = this.parent.startingProductivity;
	this.initialResourcesCount = this.parent.parent.resourcesFactory.initialResourcesList.length;
	this.initialProductCount = this.parent.parent.productsFactory.productsList.length;
	this.initialProductSize = this.parent.parent.productsFactory.getAllProductsSize();
	this.averageProductSize = (this.initialProductSize / this.initialProductCount).toFixed(2);
}

LogMaker.prototype.logGameState = function() {
	this.parent.parent.tasksFactory.allocateResourcesNOW();
	this.addData(uniqueId);
	this.getTimeInfo();
	this.getRangeInfo();
	this.getQualityInfo();
	this.getSatisfactionInfo();
	this.getResourcesInfo();
	this.getProductInfo();
	this.getTaskInfo();
	this.getPointsInfo();
	this.getRiskInfo();
	this.getDecisionInfo();
	this.getExtraRiskInfo();
	log.info(this.logMessage);
	this.clearLogMessage();
}

LogMaker.prototype.clearLogMessage = function() {
	this.logMessage = "";
}

LogMaker.prototype.addData = function(dataString) {
	this.logMessage += dataString + ";";
}

LogMaker.prototype.zeroIfNan = function(variable) {
	if(variable == "NaN" || !variable ){
		variable = 0;
		}
	return (parseFloat(variable)).toFixed(2);
}

LogMaker.prototype.getTimeInfo = function() {
	var time = this.parent.getTimeProcentage();
	time = time.toFixed(2);
	
	var timeLeft = 100 - time;
	
	this.addData(time);
	this.addData(timeLeft);
}

LogMaker.prototype.getRangeInfo = function() {
	var completion = this.parent.completion;
	completion = completion.toFixed(2);
	
	var rangeToDo = 100 - completion; 
	
	this.addData(completion);
	this.addData(rangeToDo);
}

LogMaker.prototype.getQualityInfo = function() {
	var quality = this.parent.quality;
	
	quality = quality - 6; //bo chcemy miec wartosci <-5;5>
	
	this.addData(quality);
}

LogMaker.prototype.getSatisfactionInfo = function() {
	var customerSatisfaction = this.parent.customerSatisfaction;
	var teamHappiness = this.parent.teamHappiness;
	
	customerSatisfaction = (customerSatisfaction * 20).toFixed(2);
	teamHappiness = (teamHappiness * 20).toFixed(2);
	
	this.addData(customerSatisfaction);
	this.addData(teamHappiness);
}

LogMaker.prototype.getResourcesInfo = function() {
	var currentProductivity = this.parent.parent.resourcesFactory.getAllProductivity();
	var currentResourcesCount = this.parent.parent.resourcesFactory.resourcesList.length;
	var averageProductivity = (currentProductivity / currentResourcesCount).toFixed(2);
	var freeResourcesCount = this.parent.parent.resourcesFactory.getFreeResources().length;
	var busyResourcesCount = currentResourcesCount - freeResourcesCount;
	
	this.addData(this.startingProductivity);
	this.addData(currentProductivity);
	this.addData(this.initialResourcesCount);
	this.addData(currentResourcesCount);
	this.addData(averageProductivity);
	this.addData(freeResourcesCount);
	this.addData(busyResourcesCount);
}

LogMaker.prototype.getProductInfo = function() {
	var currentNotDoneProductCount = this.parent.parent.productsFactory.getNotDoneProductsCount();
	var currentDoneProductCount = this.initialProductCount - currentNotDoneProductCount;
	
	this.addData(this.initialProductCount);
	this.addData(this.initialProductSize);
	this.addData(this.averageProductSize);
	this.addData(currentDoneProductCount);
	this.addData(currentNotDoneProductCount);
}

LogMaker.prototype.getTaskInfo = function() {
	var allTasks = this.parent.parent.tasksFactory.tasksList.length;
	var tasksWithResourcesAssigned = this.parent.parent.tasksFactory.getTasksWithAssignedResourcesCount();
	var plannedToDoProducts = this.parent.parent.tasksFactory.getPlannedToDoProductCount();
	var productsWithResourcesAssigned = this.parent.parent.productsFactory.getProductsWithAssignedResourcesCount();
	this.plannedToDoSolutions = allTasks - plannedToDoProducts;
	this.solutionsWithResourcesAssigned = tasksWithResourcesAssigned - productsWithResourcesAssigned;
	var averageSizeOfPlannedSolutions = this.zeroIfNan(this.parent.parent.tasksFactory.getSolutionsOnTasksListSize() / this.plannedToDoSolutions);
	var averageTasksProgress = (this.parent.parent.tasksFactory.getAllTaskAverageProgressInPoints()).toFixed(2);
	
	this.addData(allTasks);
	this.addData(tasksWithResourcesAssigned);
	this.addData(plannedToDoProducts);
	this.addData(productsWithResourcesAssigned);	
	this.addData(this.plannedToDoSolutions);
	this.addData(this.solutionsWithResourcesAssigned);
	this.addData(averageSizeOfPlannedSolutions);
	this.addData(averageTasksProgress);
}

LogMaker.prototype.getPointsInfo = function() {
	var score = (this.parent.score).toFixed(2);
	this.addData(score)
}

LogMaker.prototype.getRiskInfo = function() {
	var riskFightingPercentage = this.parent.riskFigtingPercentage;
	this.activeRisks = this.parent.parent.risksFactory.visibleRisksList.length;
	var activeThreats = this.parent.parent.risksFactory.countVisibleThreats();
	var activeOpportunities = this.activeRisks - activeThreats;
	var allRisksEver = this.parent.parent.risksFactory.allRisksEverCount;
	var allThreatsEver = this.parent.parent.risksFactory.allThreatsEverCount;
	var allOpportunitiesEver = allRisksEver - allThreatsEver;
	var allRisksMaterialisedEver = this.parent.allRisksMaterialisedEverCount;
	var allThreatsMaterialisedEver = this.parent.allThreatsMaterialisedEverCount;
	var allOpportunitiesMaterialisedEver = allRisksMaterialisedEver - allThreatsMaterialisedEver;
	var allThreatsRemovedCount = this.parent.parent.risksFactory.allThreatsRemovedCount;
	var allThreatsRandomlyDisappearedCount = this.parent.parent.risksFactory.allThreatsRandomlyDisappearedCount;
	var allOpportunitiesMissedCount = this.parent.parent.risksFactory.allOpportunitiesMissedCount;
	var threatsProbabilitySum = (this.parent.parent.risksFactory.getVisibleThreatsProbabilitySum()).toFixed(2);
	var threatsProbabilityAverage = this.zeroIfNan((threatsProbabilitySum / activeThreats).toFixed(2));
	var threatsImpactSum = this.parent.parent.risksFactory.getVisibleThreatsImpactSum();
	var threatsImpactAverage = this.zeroIfNan((threatsImpactSum / activeThreats).toFixed(2));
	var opportunitiesProbabilitySum = (this.parent.parent.risksFactory.getVisibleOpportunitiesProbabilitySum()).toFixed(2);
	var opportunitiesProbabilityAverage = this.zeroIfNan((opportunitiesProbabilitySum / activeOpportunities).toFixed(2));
	var opportunitiesImpactSum = this.parent.parent.risksFactory.getVisibleOpportunitiesImpactSum();
	var opportunitiesImpactAverage = this.zeroIfNan((opportunitiesImpactSum / activeOpportunities).toFixed(2));
	
	this.parent.parent.tasksFactory.updateAllSolutionsEverData();
	var allSolutionsEverCount = this.parent.parent.tasksFactory.allSolutionsEverCount;
	var allEffectiveSolutionsEverCount = this.parent.parent.tasksFactory.allEffectiveSolutionsEverCount;
	var allNotEffectiveSolutionsEverCount = allSolutionsEverCount - allEffectiveSolutionsEverCount;
	var allUniqueSolutionsEverCount = this.parent.parent.tasksFactory.allUniqueSolutionsEverIds.length;
	
	
	this.addData(riskFightingPercentage);
	this.addData(this.activeRisks);
	this.addData(activeThreats);
	this.addData(activeOpportunities);
	this.addData(allRisksEver);
	this.addData(allThreatsEver);
	this.addData(allOpportunitiesEver);
	this.addData(allRisksMaterialisedEver);
	this.addData(allThreatsMaterialisedEver);
	this.addData(allOpportunitiesMaterialisedEver);
	this.addData(allThreatsRemovedCount);
	this.addData(allThreatsRandomlyDisappearedCount);
	this.addData(allOpportunitiesMissedCount);
	this.addData(threatsProbabilitySum);
	this.addData(threatsProbabilityAverage);
	this.addData(threatsImpactSum);
	this.addData(threatsImpactAverage);
	this.addData(opportunitiesProbabilitySum);
	this.addData(opportunitiesProbabilityAverage);
	this.addData(opportunitiesImpactSum);
	this.addData(opportunitiesImpactAverage);
	this.addData(allSolutionsEverCount);
	this.addData(allEffectiveSolutionsEverCount);
	this.addData(allNotEffectiveSolutionsEverCount);
	this.addData(allUniqueSolutionsEverCount);
}

LogMaker.prototype.getDecisionInfo = function() {
	// dostepne wartosci: accept, react, wait, not_applicable
	// accept - akceptuje/lekcewazy, nie reaguje
	// react - obniza zagrozenie lub maksymaliuje szanse = realizuje obecnie jakies dzialania wobec ryzyka
	// wait - obserwuje ryzyko, dzialania przeniosl na liste zadan ale nie do realizacji natychmiast
	// not_applicable - nie ma obecnie ryzyka
	var decision = "not_applicable";
	
	if (this.activeRisks > 0){
		if (this.solutionsWithResourcesAssigned > 0){
			decision = "react";
		}
		else if (this.plannedToDoSolutions > 0) {
			decision = "wait";
		}
		else {
			decision = "accept";
		}
	}
	this.addData(decision);
}

LogMaker.prototype.getExtraRiskInfo = function() {
	this.addData("extra risk info start");
	var self = this;
	var log_string = "";
	
	if (this.activeRisks > 0) {
		var timeSinceLastPause = (this.parent.lastPauseStart.getTime() - this.parent.lastPauseStop.getTime()) / 1000;
		
		log_string = "[ ";
		$.each(this.parent.parent.risksFactory.visibleRisksList, function( i, item ) { 
			item.updateInterfaceVisibleTime(self.parent.lastPauseStart, timeSinceLastPause);
			
			log_string += "{";
			log_string +=  item.id + ", ";
			log_string +=  (item.chance).toFixed(2) + ", ";
			log_string +=  item.impact + ", ";
			log_string +=  (item.interfaceVisibleTime).toFixed(2) + ", "; //czas od pojawienia sie
			log_string +=  "[" + self.parent.parent.tasksFactory.getSolutionsOnTasksListIds() + "], ";
			log_string +=  self.parent.parent.tasksFactory.getEffectiveSolutionsForThisRiskCount(item.id, item.isGood); //czy ktores z nich zadziala
			log_string += "}";
		});
		log_string += "]";
	}
	this.addData(log_string);
}

LogMaker.prototype.logGameRating = function() {
	var ratinglog = uniqueId + ";";
	ratinglog += this.parent.gameRating + ";";
	log.debug(ratinglog);
}

function Product(json, parent) {

	this.id;
	this.name;
	this.value;
	this.quality;
	this.size;
	this.visible = true;
	this.resources = [];
	this.progress = 0;
	this.isOnTaskList = false;
	this.parent = parent;
	
	if(Config.debug) this.tickets = 0;
	
	if (json == undefined) return;
	this.id = "product" + json.ID;
	this.name = json.Name;
	this.value = parseInt(json.Value);
	this.quality = parseInt(json.Quality);
	this.size = parseInt(json.Size);
	
}

Product.prototype.toString = function () {
	return "product"+this.id;
}

Product.prototype.done = function () {
	
	var freedResources = this.resources;
	
	this.visible = false;
	this.resources = [];
	this.isOnTaskList = false;
	this.progress = 100;
	
	this.parent.parent.playFactory.quality += this.quality;
	if(this.parent.parent.playFactory.quality > 11) this.parent.parent.playFactory.quality = 11;
	if(this.parent.parent.playFactory.quality < 1) this.parent.parent.playFactory.quality = 1;
	
	return freedResources;
}

Product.prototype.addProgress = function(productivity) {
	if(this.progress < 100) {
		if(Config.debug) this.tickets++;
		this.progress += productivity / Config.pointPerTickets / this.size * 100;
		var task = $('#'+this.id);
		if(!task.hasClass('badEffect') && !task.hasClass('goodEffect')) {
			task.css('background',getCssTaskProgress(this.progress));
		}
		
		$('#'+this.id+' .right .taskSize').html(Math.round(this.size-this.size*this.progress/100));
		if(this.progress >= 100) {
			if(Config.debug) console.log("Wykonano zadanie " +this.id 
					+", size: "+this.size+" w " +this.tickets +" ticketów.");
			this.parent.parent.tasksFactory.taskDone(this);		
		}
	}
}
function Resource(json) {
	this.id; //id dla htmla
	this.sid; //id z bazy
	this.name;
	this.productivity;
	this.specialisation;
	this.visible = true;
	this.currentlyUsed = false;
	this.task; //referencja na produkt lub solution w którym się znajduje
	this.lastProductivity;
	this.initialProductivity;
	
	if (json == undefined) return;
	this.id = "resource" + json.ID;
	this.sid = json.ID;
	this.name = json.Name;
	this.productivity = parseInt(json.Productivity);
	this.lastProductivity = parseInt(json.Productivity);
	this.initialProductivity = parseInt(json.Productivity);
	
}
/*zamiast kopiować produktynwość nadaje inicjalną*/
Resource.prototype.copy = function(resource) {
	this.id = resource.id;
	this.sid = resource.sid; 
	this.name = resource.name;
	this.productivity = resource.initialProductivity;
	
	this.specialisation = resource.specialisation;
	this.visible = false;
	this.currentlyUsed = false;
	this.lastProductivity = resource.lastProductivity;
}
function Risk(json,parent) {

	this.id; //id
	this.name; //nazwa
	this.chance; //szansa ryzyka, ulega zmianie
	this.baseChance; //szansa ryzyka jak wczytana z bazy, niezmienna
	this.baseDelay; //mnożnik szansy na kolejne ryzyko (mniejsza szansa z kolejnym wystąpieniem)
	this.events = []; // wydarzenia, które powoduje to ryzyko
	this.ticketsLeft;
	this.tickets;
	this.visible = false; // czy widoczne w interfejsie
	this.succeed = false; //true jeśli ryzyko się wykonało z powodzeniem
	this.parent = parent;
	this.savedPercent = 0; //reszta z dzielenia przez 10 ostatniej zmiany ryzyka przez solution
	this.impact;
	this.bonusPoints = 1;
	this.isGood;
	this.mutex = false;
	this.currentDelay = 0;
	
	//do logowania
	this.interfaceAppearTime; //data pojawienina sie na interfejsie, do pomiaru czasu
	this.interfaceVisibleTime = 0; //czas widocznosci na interfejsie, pomijajac pauzy
	
	if (json == undefined) return;
	this.id = "risk" + json.ID;
	this.name = json.Name;
	this.baseChance = parseInt(json.Possibility);
	this.chance = parseInt(json.Possibility);
	this.events = createEvents(json.Code,this);
	this.impact = json.Impact;
	this.bonusPoints = this.impact;
	this.isGood = this.events[0].addOrSub; //zakladamy ze nie ma dobro-zlych ryzyk
	this.baseDelay = json.Next > 0 ? json.Next : 1;
	
	
}
/* Wykonanie ryzyka jeśli trafiło w przedział szansy */
Risk.prototype.rollEvent = function() {
	var roll = Math.floor((Math.random() * 100) + 1); //losowanie
	if(roll <= this.chance) { //wykonanie ryzyka
		this.succeed = true; //TODO potem chyba będzie trzeba zmienić położenie tego
		if(Config.debug) console.log("SUKCES RYZYKA! " + this.toString());
		this.events.map(function(event){
			if(Config.debug) console.log("Wykonywanie eventu ryzyka:");
			event.doEvent();
		});
		
		/*za każde zmaterializowane zagrożenie +20% do ogólnego poziomu ryzyka projektu. 
		 * (tu dodajemy 10%, bo zeby pasek ryzyka nie migal, przy usuwaniu wykonanego zagrozenia
		 * nie odejmujemy 10% od ryzyka, wiec w sumie jest o 20% wiecej niz przed jego pojawieniem sie)
		 * Za każdą zmaterializowaną losowo szansę -10%, 
		 * a za każdą szansę, którą gracz podniósł działaniami do 100% 
		 * jest -20% do ogólnego poziomu ryzyka projektu*/
		if(this.isGood) {
			if(this.chance == 100) {
				this.parent.parent.playFactory.changeRiskFigtingPercentageBy(-20);
			}
			else {
				this.parent.parent.playFactory.changeRiskFigtingPercentageBy(-10);
			}
		}
		else {
			this.parent.parent.playFactory.changeRiskFigtingPercentageBy(10);
		}
		
	} else {
		if(Config.debug) console.log("Niepowodzenie, wylosowano: " + roll + " spośród: " + this.chance);
	}
}

Risk.prototype.setVisible = function() {
	this.visible = true;
	var result = getRiskLi(this);
	$( "#risks").append(result);
		$('.textRiskInfluenceImg').prop('title',language['textRiskInfluenceImgTooltip'].text);
		$('.textRiskInfluenceImg').addClass('withTooltip');
		$(".withTooltip").tooltip({show: {delay: 300}});
}
Risk.prototype.removeVisible = function(shouldVisibleRiskChange) {
	if(shouldVisibleRiskChange == true) {
		this.parent.changeVisibleRisksBy(-1, this.isGood);
	}
	this.parent.visibleRisksList.splice(
			this.parent.visibleRisksList.indexOf(this),1); //usunięcie z listy widocznych ryzyk
	$( ("#risks #"+this.id)).remove();
	this.reset();
	this.mutex = false;
}
/* TODO resetowanie ryzyka (może być ponowne użycie ryzyka?) */
Risk.prototype.reset = function () {
	this.visible = false;
	this.succeed = false;
	this.ticketsLeft = undefined;
	this.chance = this.baseChance;
	this.savedPercent = 0;
}

Risk.prototype.toString = function() {
	return " " + this.id + ", chance: " + this.chance 
	+ ", ticketsLeft: " + this.ticketsLeft + ", visible: " 
	+ this.visible + ", succeed: " + this.succeed +
	', mutex: ' + this.mutex;
}

Risk.prototype.changeChance = function(value) {
	
	if(this.chance > 0 && this.chance < 100) {
		var val = parseFloat(value);
		
		var isIncreasing = val > 0 ? true : false;
		
		var oldChance = this.chance;
		this.chance += val;
		
		//wszystko to zeby byly punkty za kazde zmniejszone o 10 przez gracza ryzyko
		var x = this.savedPercent + Math.abs(val);
		this.savedPercent = x % 10;		
		
		var multiplier = Math.floor(x);
		
		if(this.chance >= 100) {
			this.chance = 100;
			multiplier = Math.abs(100 - oldChance);
		}
		else if(this.chance <= 0){
			this.chance = 0;
			multiplier = oldChance;
			this.parent.addThreatsRemovedCounter(this.isGood);
			this.removeVisible(true);
		}
		
		if(x / 10 >= 1 && (isIncreasing == this.isGood)) {
			multiplier = parseInt(multiplier / 10) * 10
			
			var points = multiplier * this.bonusPoints; 
			this.parent.parent.playFactory.score += points;
		}
		
		changeRiskLiChanceAndColor(this);
		
		if(this.chance == 100 || this.chance == 0) {
			this.ticketsLeft = 0;
		}
	}
}

Risk.prototype.getColor = function() {
	var brightness = 200 - 200*this.chance/100;
	brightness = Math.floor(brightness);
	var rgb;
	if(this.events.length >0) { //na razie na 'sztywno'
		rgb = this.events[0].addOrSub ? "rgb(" + brightness + "," + "255" + "," + brightness +");" :
			"rgb(" + "255" + "," + brightness + "," + brightness +");" ;
	}
	
	return rgb;
}

Risk.prototype.updateInterfaceVisibleTime = function(pauseTime, timeSinceLastPause) {
	if(this.interfaceVisibleTime == 0){
		this.interfaceVisibleTime = (pauseTime.getTime() - this.interfaceAppearTime.getTime()) / 1000;
	}
	else {
		this.interfaceVisibleTime += timeSinceLastPause;
	}
}

/* event odpowiedzialny za wykonanie danego ryzyka */
function RiskEvent(parent) {
	this.doEvent; //ta zmienna to funkcji, czyli wywołanie doEvent() np. 'produktywność','zasób', 'postęp'
	this.addOrSub; // true = dodaj, false = odejmij np. '+', 'znika zasób', 'wykonać od nowa'
	this.values = []; // wartość/wartości np. [50], [1, 2, 3],[1]
	this.parent = parent;
}
/*-------zbiór funkcji realizujących dane ryzyko--------*/
RiskEvent.prototype.productivity = function() {
	
	var value = this.values[0]/100;
	var addOrSub = this.addOrSub;
	$.each(this.parent.parent.parent.resourcesFactory.resourcesList,function(i,resource){
		var newProductivity = resource.productivity * value;
		newProductivity = resource.productivity + (addOrSub ? newProductivity : -newProductivity);
		resource.lastProductivity = resource.productivity;
		resource.productivity = newProductivity;
	});
	
	this.parent.parent.eventsProductivityMutex = ++this.parent.parent.eventsProductivityMutex % Config.riskEventsMaxMutexSize;
	var self = this;
	
	$.each(this.parent.parent.parent.resourcesFactory.resourcesList, function( i, resource ){
		var productivityChangesText = "";
			var change = Math.round(resource.productivity) - Math.round(resource.lastProductivity);
			if(change != 0) {
				productivityChangesText = getResourceProducitvityEffect(change);
			}
			var currentMutex = self.parent.parent.eventsProductivityMutex;
			$('#'+resource.id + ' #resourceProductivity').html(Math.round(resource.productivity) + productivityChangesText);
			setTimeout(function() {
				if(self.parent.parent.eventsProductivityMutex == currentMutex) {
					$('#'+resource.id + ' #resourceProductivity').html(Math.round(resource.productivity));
				}
			},Config.riskShowTime);
	});
	
	this.blinkResourceTitle();
}


RiskEvent.prototype.resource = function() {
	
	var resourcesFactory= this.parent.parent.parent.resourcesFactory;
	var resourcesList = resourcesFactory.resourcesList;
	var initialResourcesList = resourcesFactory.initialResourcesList;
	var resourceId = null;
	var resourceListId = null;
	var self = this;
	if(this.addOrSub) { //dodanie zasobów
		for(var i=0;i<initialResourcesList.length;i++) {
			if(initialResourcesList[i].sid == parseInt(this.values[0])) {
				var resourceFactory = this.parent.parent.parent.resourcesFactory;
				resourcesFactory.addResourceToList(initialResourcesList[i],"resource" + (++resourceFactory.lastResourceIdIncrement));
				var justAddedresourceId = "#resource"+resourceFactory.lastResourceIdIncrement;
				$(justAddedresourceId).addClass('goodEffect');
				/*usuniecie później efektu*/
				setTimeout(function() {$(justAddedresourceId).removeClass('goodEffect')},
						Config.riskShowTime);
				break;
			}
		}
	}else {	//usunięcie zasobów
		for(var i=0;i<resourcesList.length;i++) {
			if(resourcesList[i].sid == this.values[0])  {
				var resource = resourcesList[i];
				resourceListId = i;
				resourceId = "#"+resourcesList[i].id;
				if($(resourceId).hasClass('badEffect')) continue; // juz jest w trakcie usuwania
				$(resourceId).addClass('badEffect');
				setTimeout(function() {
					var resourceListId = resourcesList.indexOf(resource);
					if(resourceListId != -1) {
						resourcesFactory.removeResourceFromList(resourceListId);
					}
				},Config.riskShowTime);
				break;
			}
		}
	}
	/*jeśli zasob do usunięcia nie znajduje się na aktualnej liście to kończy wykonywanie*/
	if(!this.addOrSub && resourceListId == null) {
		if(Config.debug) console.log("Nie ma na liście; value = " + this.values[0]);
		return;
	}
	
	this.blinkResourceTitle();
	
}
RiskEvent.prototype.blinkResourceTitle = function() {
	var self = this;
	/* czyszczenie migania napisu */
	if(this.parent.parent.eventsResourcesMutex != null) {
		clearTimeout(this.parent.parent.eventsResourcesMutex.timeout);
		clearInterval(this.parent.parent.eventsResourcesMutex.interval);
		this.parent.parent.eventsResourcesMutex = null;
		changeResourceTitleColor(false,'red','resourcesTitle');
		changeResourceTitleColor(false,'green','resourcesTitle');
	}
	
	var switchTo = true;
	var mutex = { 
		interval : setInterval(function() {
			changeResourceTitleColor(switchTo,self.addOrSub ? 'green' : 'red','resourcesTitle');
			switchTo = !switchTo;
		}, 500),
		timeout : setTimeout(function() {
			clearInterval(self.parent.parent.eventsResourcesMutex.interval);
			self.parent.parent.eventsResourcesMutex = null;
			changeResourceTitleColor(false,self.addOrSub ? 'green' : 'red','resourcesTitle');
		},Config.riskShowTime)
	};
	this.parent.parent.eventsResourcesMutex = mutex;
}
RiskEvent.prototype.progress = function() {
	
	var productsFactory = this.parent.parent.parent.productsFactory;
	var productsList = productsFactory.productsList;
	
	for(var i=0;i<productsList.length;i++) {
		if(("product"+this.values[0]) == productsList[i].id) {
			var product =  productsList[i];
			if(!product.visible) {
				product.visible = true;
				product.progress = 0;
				$('#products').append(getProductLi(product));
			} else {
				var progress = product.progress/100 * product.size - this.values[1];
				product.progress = progress > 0 ? progress : 0;
				$("#"+product.id).addClass('badEffect');
				setTimeout(function() {$("#"+product.id).removeClass('badEffect')},
						Config.riskShowTime);
			}
			$('#'+product.id).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500)
				.fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500)
				.fadeOut(500).fadeIn(500);
			var self = this;			
			if(this.parent.parent.eventsProgressMutex != null) {
				clearTimeout(this.parent.parent.eventsProgressMutex.timeout);
				clearInterval(this.parent.parent.eventsProgressMutex.interval);
				this.parent.parent.eventsProgressMutex = null;
				changeResourceTitleColor(false,'red','productsTitle');
			}
			
			var switchTo = true;
			var mutex = { 
				interval : setInterval(function() {
					changeResourceTitleColor(switchTo,'red','productsTitle');
					switchTo = !switchTo;
				}, 500),
				timeout : setTimeout(function() {
					clearInterval(self.parent.parent.eventsProgressMutex.interval);
					self.parent.parent.eventsProgressMutex = null;
					changeResourceTitleColor(false,'red','productsTitle');
				},Config.riskShowTime)
			};
			this.parent.parent.eventsProgressMutex = mutex;
			
		}
	}	
}

/* Tworzy eventy dla ryzyka */
function createEvents (code,parent) {
	var events = code.split(";"); //podział na pojedynczy event 
	var riskEvents = [];
	events.map(function(event){
		var riskEvent = new RiskEvent(parent);
		var e = event.split("."); //podział na ["funkcja","dodaj lub odejmij","wartości"]
		riskEvent.doEvent = extractFunctionForEvent(e[0],riskEvent);
		riskEvent.addOrSub = extractBooleanForEvent(e[1]);
		riskEvent.values = extractValuesForEvent(e[2]);
		isEventOk(riskEvent) ? riskEvents.push(riskEvent) : null;
	});
	return riskEvents;
}
function extractFunctionForEvent(value,riskEvent) {
	switch(value.toLowerCase()){
		case "productivity": return riskEvent.productivity;
		case "resource": return riskEvent.resource;
		case "progress": return riskEvent.progress;
		default: return null;
	}
}

function extractBooleanForEvent(value) {
	switch(value.toLowerCase()) {
	case "add":	return true;
	case "sub" : return false;
	default: return null;
	}
}

function extractValuesForEvent(values) {
	return values != null ? values.replace("[","").replace("]","").split(",") : null;
}

function isEventOk(riskEvent) {
	if(riskEvent.doEvent != null 
			&& riskEvent.addOrSub != null
			&& riskEvent.values.length > 0) 
		return true;
	return false;
}
function changeResourceTitleColor(switchTo, cssClass, id) {
	var resourcesTitle = $('#'+id);
	var color = cssClass;
	if(switchTo) resourcesTitle.addClass(color);
	else resourcesTitle.removeClass(color);
}
function Solution(json, parent) {
	this.id;
	this.name;
	this.size;
	this.solutionEvents;
	this.parent = parent;
	this.progress = 0;
	
	if (json == undefined) return;
	this.id = "solution" + json.ID;
	this.name = json.Name;
	this.size = parseInt(json.Size);
	this.solutionEvents = createSolutionEvents(json.Code,this);
}

Solution.prototype.done = function () {
	
	var freedResources = this.resources;
	
	this.resources = [];
	this.progress = 0;
	
	return freedResources;
}

Solution.prototype.addProgress = function(productivity) {
	if(this.progress < 100) {
		if(Config.debug) this.tickets++;
		
		var progress = productivity / Config.pointPerTickets / this.size * 100;
		this.progress += progress;
		var task = $('#'+this.id);
		task.css('background',getCssTaskProgress(this.progress));
		
		
		$('#'+this.id+' .right .taskSize').html(Math.round(this.size-this.size*this.progress/100));
		this.doAllSolutionEvents(progress);
		if(this.progress >= 100) {
			if(Config.debug) console.log("Wykonano solution " +this.id 
					+", size: "+this.size+" w " +this.tickets +" ticketów.");
			this.parent.parent.tasksFactory.taskDone(this);		
		}
	}
}

Solution.prototype.doAllSolutionEvents = function (progress) {
	this.solutionEvents.map(function(solutionEvent){
		if(Config.debug) console.log("Wykonywanie eventu solution:");
		solutionEvent.doSolutionEvent(progress);
	});
}



/* event odpowiedzialny za wykonanie danego wplywu */
function SolutionEvent(parent) {
	this.doSolutionEvent; //ta zmienna to funkcji, czyli wywołanie doSolutionEvent() np. 'produktywność','zasób', 'postęp'
	this.addOrSub; // true = dodaj, false = odejmij np. '+', 'znika zasób', 'wykonać od nowa'
	//this.values = []; // wartość/wartości np. [50], [1, 2, 3],[1]
	this.riskId;
	this.totalValue;
	this.parent = parent;
}
/*-------zbiór funkcji realizujących dany wplyw--------*/

SolutionEvent.prototype.possibility = function(value) {
	if(Config.debug) console.log("possibility: " + (this.addOrSub ? "add" : "sub") + ", riskId:" + this.riskId + ",value " + value);
	
	var risksList = this.parent.parent.parent.risksFactory.visibleRisksList;
	
	for(var i=0;i<risksList.length;i++)
		if(("risk"+ this.riskId) == risksList[i].id){
			var risk = risksList[i];
			
			if(this.addOrSub == false) {
				value = -value;
			}
			value *= this.totalValue / 100;
			
			risk.changeChance(value);
			break;
		}	
}

/* Tworzy eventy dla wplywu */
function createSolutionEvents (code,parent) {
	var events = code.split(/[\s;]+/); //podział na pojedynczy event 
	var solutionEvents = [];
	events.map(function(event){
		var solutionEvent = new SolutionEvent(parent);
		var e = event.split("."); //podział na ["funkcja","dodaj lub odejmij","wartości"]
		solutionEvent.doSolutionEvent = extractFunctionForSolutionEvent(e[0],solutionEvent);
		solutionEvent.addOrSub = extractBooleanForSolutionEvent(e[1]);
		var values = extractValuesForSolutionEvent(e[2]);
		
		solutionEvent.riskId = values[0];
		solutionEvent.totalValue = parseInt(values[1]);
		
		isSolutionEventOk(solutionEvent) ? solutionEvents.push(solutionEvent) : null;
	});
	return solutionEvents;
}

function extractFunctionForSolutionEvent(value,solutionEvent) {
	switch(value.toLowerCase()){
		case "possibility": return solutionEvent.possibility;
		default: return null;
	}
}

function extractBooleanForSolutionEvent(value) {
	switch(value.toLowerCase()) {
	case "add":	return true;
	case "sub" : return false;
	default: return null;
	}
}

function extractValuesForSolutionEvent(values) {
	if(values != null) {
		var desired = values.replace(/\[(.*?)\]/g,"$1");
		return desired.split(",");
	}
	return null;
}

function isSolutionEventOk(solutionEvent) {
	if(solutionEvent.doSolutionEvent != null 
			&& solutionEvent.addOrSub != null
			&& solutionEvent.riskId != null
			&& solutionEvent.totalValue != null) 
		return true;
	return false;
}

/* Klasa globalna inicjowana w MainContainer.init() */
function Config() {

	this.takt = 2; // co x procent ogólnego postępu wykonuje się aktualizacja statystyk
	this.scoreScale = 0.1;
	// Początkowe wartości
	this.initialCustomerSatisfaction = 5;
	this.initialTeamHappiness = 5;
	
	// Zmiennosc zadowolenia klienta
	this.customerSatisfactionChange = 0.05;
	// Zmiennosc zadowolenia zespolu
	this.teamHappinessChange = 0.05;
	
	this.showIntro = true;
	this.debug = false; // do wyświetlania w consoli informacji
	
	/* RYZYKO */
	this.riskTimePerTickets = 5; // co x sekund jest losowana szansa na zainstnienie ryzyka
	this.riskDisappearTimePerTickets = 10; // co x sekund jest losowana szansa na zniknięcie aktywnego ryzyka
	this.riskChance = 20; // szansa na ryzyko
	this.riskDisappearChance = 20 // szansa na zniknięcie aktywnego ryzyka
	this.riskTicketsLeftInterval = [10,30]; //losowy przedział czasowy (sekundy) ile zostało do wykonania się ryzyka
	this.riskShowTime = 5000; // ile milisekund ma się wyświetlać informacja o zaistniałym ryzyku
	
	this.maxUnhappyTeamDays = 10; //ile dni zadowolenie zespolu moze byc niskie zanim przerwie to gre
	this.maxUnhappyCustomerDays = 10; //ile dni zadowolenie klienta moze byc niskie zanim przerwie to gre
	
	this.minTeamHappiness = 3; //ile wynosi zadowolenie zespolu od ktorego zaczyna grozic koniec gry
	this.minCustomerSatisfaction = 3; //ile wynosi zadowolenie klienta od ktorego zaczyna grozic koniec gry
	
	this.pauseScorePenalty = 100; //ile punktow odejmie od wyniku reczne nacisniecie przez gracza pauzy
	
	/* CACHE */
	this.riskEventsMaxMutexSize = 8;
	
	/*FPS */
	this.milisecondsPerTicket = 100; // x milisekund zajmuje wykonanie 1 ticketu 
	
	/* CZAS */
	this.pointPerTickets = 10; // x sekund zajmuje wykonanie 1 punktu productivity
	this.ticketsPerTimeUnit = 10; // x sekund zajmuje wykonanie się 1 jednostki czasu; do wyświetlania
	this.calculateTicketsFromSeconds();
	
}

//OBLICZANIE WYNIKU
Config.prototype.returnScore = function (previousScore,previousCompletion,completion,teamHappiness,customerSatisfaction,riskFigtingPercentage) {
	var score = previousScore + this.scoreScale * ( ( customerSatisfaction * 100 ) + ( teamHappiness * 100 ) + ( ( completion - previousCompletion ) * 10 ) + riskFigtingPercentage );
	return score;
}

//AKTUALIZACJA ZADOWOLENIA KLIENTA
Config.prototype.returnCustomerSatisfaction = function (completion,procentageTime,currentQuality,MaxQuality,riskFigtingPercentage) {
	var result = (( completion / procentageTime ) * 100 + ( (currentQuality - MaxQuality/2) / (MaxQuality/2) * 100 ) + ( 100 - riskFigtingPercentage )) / 3 / 20;
	if(this.debug === true) console.log("Zadowolenie klienta: " + Math.floor(completion / procentageTime) * 100 + "% + " + Math.floor( currentQuality / MaxQuality * 100 ) + "% + " + Math.floor( 100 - riskFigtingPercentage ) + "%" );
	
	if(result > 5) result = 5;
	if(result < 0) result = 0;
	
	return result;
}

//AKTUALIZACJA ZADOWOLENIA ZESPOLU
Config.prototype.returnTeamHappiness = function ( startingProductivity, actualProductivity, completion, procentageTime, riskFigtingPercentage ) {
	var result = (( actualProductivity / startingProductivity * 100 ) + ( completion / procentageTime * 100 ) + ( 100 - riskFigtingPercentage ) ) / 3 / 20;
	if(this.debug === true) console.log("Zadowolenie zespolu: " + Math.floor( actualProductivity / startingProductivity * 100 ) + "% + " + Math.floor( completion / procentageTime * 100 ) + "% + " + Math.floor( 100 - riskFigtingPercentage ) + "%");
	
	if(result > 5) result = 5;
	if(result < 0) result = 0;
	
	return result;
}

//PRZELICZENIE JEDNOSTEK Z SEKUND NA TICKETY
Config.prototype.calculateTicketsFromSeconds = function() {
	var value = 1000/this.milisecondsPerTicket;
	
	this.pointPerTickets = this.pointPerTickets * value;
	this.ticketsPerTimeUnit = this.ticketsPerTimeUnit * value;
	this.riskTimePerTickets = this.riskTimePerTickets * value;
	this.riskDisappearTimePerTickets = this.riskDisappearTimePerTickets * value;
	this.riskTicketsLeftInterval = [this.riskTicketsLeftInterval[0]*value, this.riskTicketsLeftInterval[1]*value];
}

Config.prototype.loadConfig = function(json) {
	if(json == undefined) return;
	for(var i = 0 ;i<json.length; i++) {
		if(json[i].Value.charAt(0) == '[') {
			var result = json[i].Value.replace(/\[|\]/g,'');
			result = result.split(',');
			this[json[i].Name] = result;
		} else {
			this[json[i].Name] = json[i].Value;
		}
	}
	this.calculateTicketsFromSeconds();
}
