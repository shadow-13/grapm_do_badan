function Language() {
	this.properties = {
		"pl" : {
			textWelcomeGameName : { text : "GraPM", when : 0},
			textWelcomeTitle : { text : "Naucz się zarządzać projektami", when : 0 },
			textHowToPlay : { text : "Jak grać?", when : 0 },
			textWelcomeContents : { text : "Wciel się w rolę kierownika projektu i osiągnij sukces. Wytwórz zamówiony produkt, zarządzaj zakresem, jakością, czasem, zadaniami i ryzykiem projektu.\\nPrzenoś produkty i działania wobec ryzyka na listę zadań, naciśnij pasek postępu by Twoje zasoby przydzieliły się kolejno do zadań i rozpoczęły pracę.\\nObserwuj wskaźniki projektu, szczególnie zadowolenie klienta i zespołu. Zatrzymuj zadania, zmieniaj plany, reaguj na pojawiające się ryzyka.\\nPrzeprowadź jak najlepiej projekt w ograniczonym czasie lub jak najsprawniej wytwórz kompletny produkt. Powodzenia!", when : 0 },
			textWelcomeTypeTime : { text : "Ograniczony czas", when : 0 },
			textWelcomeTypeFull : { text : "Kompletny produkt", when : 0 },
			textWelcomeStart : { text : "Rozpocznij Grę", when : 0 },
			textWelcomeFooter : { text : "Projekt dyplomowy inżynierski 2014\\nPolitechnika Gdańska, Wydział Elektroniki, Telekomunikacji i Informatyki, Katedra Inżynierii Oprogramowania\\nAutorzy: Damian Płatek, Rafał Piechowski, Anna Wasik, Sylwia Grabowska\\nOpiekun projektu i autor koncepcji gry: dr inż. Jakub Miler\\n wersja 1.1, Anna Wasik 2016", when : 0 },
			textWelcomeTutorial : { text : "https://www.youtube.com/embed/sJ2U9OL1EfA", when : 0 },
			
			textPopupEndTitleSucceed : { text : "SUKCES!", when : 1 },
			textPopupEndContentsSucceed : { text : "Projekt ukończony", when : 1 },
			textPopupEndTitleTimeOut : { text : "KONIEC CZASU", when : 1 },
			textPopupEndContentsTimeOut : { text : "Projekt częściowo ukończony", when : 1 },
			textPopupEndTitleLost : { text : "PORAŻKA!", when : 1 },
			textPopupEndContentsLostClient : { text : "Niezadowolony klient zrezygnował z projektu", when : 1 },
			textPopupEndContentsLostTeam : { text : "Niezadowolony zespół odszedł z projektu", when : 1 },
			textPopupEndContentsLostRisk : { text : "Ryzyko porażki projektu osiągnęło 100%", when : 1 },	
			textPopupQuitContent : { text : "Przerwać projekt?", when : 1 },
			textPopupQuitYesButton : { text : "Tak", when : 1 },
			textPopupQuitNoButton : { text : "Nie", when : 1 },
			textPopupEndContentsStars : { text : "Oceń grę:", when : 1 },
			
			textInterfaceTitleProject : { text : "Projekt", when : 0 },
			textInterfaceTitleProjectTooltip : { text : "Projekt, którym zarządzasz. Przenoś produkty i działania wobec ryzyka na listę zadań, naciśnij pasek postępu by Twoje zasoby rozpoczęły pracę.", when : 1 },
			textInterfaceTitleProducts : { text : "Produkt", when : 0 },
			textInterfaceTitleProductsTooltip : { text : "Elementy i cechy produktu, który masz dostarczyć klientowi. Widzisz kolejno nazwę elementu, wartość biznesową elementu zwiększającą zakres wykonanego produktu (pudełka), wpływ elementu na jakość produktu (gwiazdki) oraz pracochłonność wytworzenia elementu (młotki).", when : 1 },
			textInterfaceTitleResources : { text : "Zasoby", when : 0 },
			textInterfaceTitleResourcesTooltip : { text : "Twój zespół wykonujący zadania. Dbaj o jego zadowolenie. Zasoby mają nazwę oraz różną wydajność pracy (młotki - ile pracy liczonej w młotkach lub kołach zębatych zasób wykonuje w ciągu jednego dnia). Sortuj zasoby tak, aby najwydajniejsze realizowały największe zadania.", when : 1 },
			textInterfaceTitleTasks : { text : "Zadania", when : 0 },
			textInterfaceTitleTasksTooltip : { text : "Bieżące zadania do wykonania w projekcie. Przenoś tutaj elementy produktu oraz działania wobec ryzyka. Ustawiaj kolejność, w jakiej mają je wykonywać Twoje zasoby. Uruchamiaj i zatrzymuj pracę paskiem postępu pracy pod listą. Zarządzaj zadaniami w trakcie pauzy. Pamiętaj, że każda pauza kosztuje 100 punktów.", when : 1 },
			textInterfaceTitleRisks : { text : "Ryzyka", when : 0 },
			textInterfaceTitleRisksTooltip : { text : "Niepewne zdarzenia mogące wpłynąć negatywnie lub pozytywnie na Twój projekt. Czerwone zagrożenia wpływają negatywnie, a zielone szanse pozytywnie. Widzisz kolejno nazwę ryzyka, możliwość jego wystąpienia w % oraz siłę jego wpływu od 1 do 4 wykrzykników. Dobierz odpowiednie działania wobec ryzyka aby obniżyć zagrożenia i zwiększyć szanse.", when : 1 },
			textInterfaceTitleActions : { text : "Działania wobec ryzyka", when : 0 },
			textInterfaceTitleActionsTooltip : { text : "Różne działania, które możesz podjąć by wpłynąć ma możliwość wystąpienia ryzyk. Działania mają nazwę oraz pracochłonność (koła zębate). Nie wiadomo, które działanie okaże się najlepsze na Twoje aktualne ryzyka. Przeglądaj ryzyka, wyszukuj pasujące działania, podejmuj je i obserwuj ich wpływ na ryzyko.", when : 1 },
			textInterfaceProjectScope : { text : "Zakres", when : 0 },
			textInterfaceProjectScopeTooltip : { text : "Wartość biznesowa produktu, który wytwarzasz. Widzisz przyrastającą wartość biznesową wraz z wytwarzaniem poszczególnych elementów produktu. Wykonaj jak najsprawniej kompletny produkt by ukończyć projekt z sukcesem.", when : 1 },
			textInterfaceProjectQuality : { text : "Jakość", when : 0 },
			textInterfaceProjectQualityTooltip : { text : "Jakość produktu, który wytwarzasz. Jakość jest określona w skali -5 (najgorsza) do +5 (najlepsza). Postaraj się utrzymywać wysoką jakość produktu wytwarzając poszczególne elementy o różnym, nawet negatywnym wpływie na jakość.", when : 1 },
			textInterfaceProjectTime : { text : "Czas", when : 0 },
			textInterfaceProjectTimeTooltip : { text : "Czas na realizację projektu. Jeżeli grasz na ograniczony czas, to po wyczerpaniu czasu projekt zostanie zakończony. W grze w kompletny produkt możesz przekroczyć czas maksymalnie o 100%. Postaraj się optymalnie wykorzystywać dostępny czas.", when : 1 },
			textInterfaceProjectClient : { text : "Klient", when : 0 },
			textInterfaceProjectClientTooltip : { text : "Zadowolenie Twojego klienta. Klient oczekuje od Ciebie wydajnej pracy, wysokiej jakości produktu i niskiego ryzyka projektu. Gdy jest zbyt długo niezadowolony, może zrezygnować z projektu, co oznacza Twoją porażkę!", when : 1 },
			textInterfaceProjectTeam : { text : "Zespół", when : 0 },
			textInterfaceProjectTeamTooltip : { text : "Zadowolenie Twojego zespołu. Zespół oczekuje dobrych postępów projektu, dużej wydajności pracy oraz niskiego ryzyka projektu. Gdy jest zbyt długo niezadowolony, może odejść z projektu, co oznacza Twoją porażkę!", when : 1 },
			textInterfaceProjectRisk : { text : "Ryzyko", when : 0 },
			textInterfaceProjectRiskTooltip : { text : "Ogólny poziom ryzyka projektu. Oznacza możliwość porażki Twojego projektu. Klient i zespół nie lubią zbyt wysokiego ryzyka. Pozbywaj się zagrożeń i wykorzystuj szanse, by zmniejszyć ogólne ryzyko projektu.", when : 1 },
			textInterfaceProjectScore : { text : "Punkty", when : 0 },
			textInterfaceProjectScoreTooltip : { text : "Twój wynik w grze. Punkty otrzymujesz za wytwarzanie produktu, obniżanie zagrożeń i wykorzystywanie szans, utrzymywanie wysokiej jakości produktu, wysokiego zadowolenia klienta i zespołu oraz niskiego poziomu ryzyka. Zdobądź najwięcej punktów i zostań najlepszym kierownikiem projektu!", when : 1 },
			
			textProductValueImgTooltip : { text : "Wartość dla klienta - wpływa na zakres", when : 1 },
			textProductQualityImgTooltip : { text : "Wpływ na jakość projektu", when : 1 },
			textProductSizeImgTooltip : { text : "Pracochłonność", when : 1 },
			textResourceProductivityImgTooltip : { text : "Wydajność zasobu", when : 1 },
			textRiskInfluenceImgTooltip : { text : "Wpływ ryzyka", when : 1 },
			textAutopause : { text : "Autopauza", when : 0 },
			
			textInterfaceTimeUnitDay : { text : "dzień", when : 0 },
			textInterfaceTimeUnitWeek : { text : "tydzień", when : 0 },
			textInterfaceTimeUnitMonth : { text : "miesiąc", when : 0 },
			textInterfaceTimeUnitYear : { text : "rok", when : 0 }
		},
		
		"en" : {
			textWelcomeGameName : { text : "GraPM", when : 0 },
			textWelcomeTitle : { text : "Learn to manage projects", when : 0 },
			textHowToPlay : { text : "How to play?", when : 0 },
			textWelcomeContents : { text : "Take on the role of project manager and achieve success. Deliver ordered product, manage the scope, quality, time, tasks, and risk of the project. \\nMove products and risk actions to the task list, press the progress bar to allocate resources to the tasks sequentially and start work. \\nMonitor the project indicators, customer and team satisfaction. Pause tasks, change your plans, react to appearing risks. \\nAccomplish best results in limited time or efficiently complete the product. Good luck!", when : 0 },
			textWelcomeTypeTime : { text : "Limited time", when : 0 },
			textWelcomeTypeFull : { text : "Complete product", when : 0 },
			textWelcomeStart : { text : "PLAY", when : 0 },
			textWelcomeFooter : { text : "Engineering Diploma Project 2014\\nGdansk University of Technology, Poland, Faculty of Electronics, Telecommunications and Informatics, Department of Software Engineering\\nAuthors: Damian Płatek, Rafał Piechowski, Anna Wasik, Sylwia Grabowska\\nProject supervisor and author of the game concept: Jakub Miler, PhD\\n version 1.1, Anna Wasik 2016", when : 0 },
			textWelcomeTutorial : { text : "https://www.youtube.com/embed/uqpD-GIQQhQ", when : 0 },
			
			textPopupEndTitleSucceed : { text : "YOU WIN!", when : 1 },
			textPopupEndContentsSucceed : { text : "Project completed", when : 1 },
			textPopupEndTitleTimeOut : { text : "TIME IS OUT", when : 1 },
			textPopupEndContentsTimeOut : { text : "Project partially completed", when : 1 },
			textPopupEndTitleLost : { text : "YOU LOSE!", when : 1 },
			textPopupEndContentsLostClient : { text : "Dissatisfied customer cancelled the project", when : 1 },
			textPopupEndContentsLostTeam : { text : "Dissatisfied team left the project", when : 1 },
			textPopupEndContentsLostRisk : { text : "The risk of project failure reached 100%", when : 1 },
			textPopupQuitContent : { text : "Abandon the project?", when : 1 },
			textPopupQuitYesButton : { text : "Yes", when : 1 },
			textPopupQuitNoButton : { text : "No", when : 1 },
			textPopupEndContentsStars : { text : "Rate game:", when : 1 },

			textInterfaceTitleProject : { text : "Project", when : 0 },
			textInterfaceTitleProjectTooltip : { text : "The project you manage. Move products and risk actions to the task list, press the progress bar to have your resources work.", when : 1 },
			textInterfaceTitleProducts : { text : "Product", when : 0 },
			textInterfaceTitleProductsTooltip : { text : "Elements and features of the product to deliver to the customer. Product elements have a name, business value increasing the product scope (boxes), impact on product quality (stars), and development cost (hammers).", when : 1 },
			textInterfaceTitleResources : { text : "Resources", when : 0 },
			textInterfaceTitleResourcesTooltip : { text : "Your team carrying out the tasks. Care for its satisfaction. Resources have a name and varying productivity (hammers - how much work counted in hammers or gears a resource does in one day). Sort the resources to have the most productive ones do the largest tasks.", when : 1 },
			textInterfaceTitleTasks : { text : "Tasks", when : 0 },
			textInterfaceTitleTasksTooltip : { text : "Current tasks to do in the project. Move the products elements and risk actions here. Sort the tasks to adjust the development sequence for your resources. Start and pause work with the progress bar under the task list. Manage tasks while paused. Mind each pause costs 100 points.", when : 1 },
			textInterfaceTitleRisks : { text : "Risks", when : 0 },
			textInterfaceTitleRisksTooltip : { text : "Uncertain events that may affect your project negatively or positively. Red threats have negative impact while green opportunities have positive impact. Risks have a name, possibility to occur in %, and the impact in a range of 1 to 4 exclamation marks. Select appropriate risk actions to reduce threats and increase opportunities.", when : 1 },
			textInterfaceTitleActions : { text : "Risk Actions", when : 0 },
			textInterfaceTitleActionsTooltip : { text : "Various actions you might take to change the possibility of risk occurrence. Risk actions have a name and execution cost (gears). You may not know which actions will affect your current risks. Examine the risks, search for matching actions, start them and observe the impact on risks.", when : 1 },
			textInterfaceProjectScope : { text : "Scope", when : 0 },
			textInterfaceProjectScopeTooltip : { text : "Business value of the product you develop. Business value increases gradually with the development of each product element. Develop complete product most effectively to finish the project successfully and WIN.", when : 1 },
			textInterfaceProjectQuality : { text : "Quality", when : 0 },
			textInterfaceProjectQualityTooltip : { text : "The quality of the product you develop. Quality scale ranges from -5 (worst) do +5 (best). Try to keep high product quality while developing the product elements with various impact on quality, even negative.", when : 1 },
			textInterfaceProjectTime : { text : "Time", when : 0 },
			textInterfaceProjectTimeTooltip : { text : "Time to deliver the product. If you play limited time, your project will end when this time runs out. When you play complete product, you can exceed the project time by 100% at most. Try to use the project time optimally.", when : 1 },
			textInterfaceProjectClient : { text : "Customer", when : 0 },
			textInterfaceProjectClientTooltip : { text : "Satisfaction of your customer. The customer expects effective work, high product quality, and low project risk. If the customer is dissatisfied too long, he can cancel the project and YOU LOSE!", when : 1 },
			textInterfaceProjectTeam : { text : "Team", when : 0 },
			textInterfaceProjectTeamTooltip : { text : "Satisfaction of your team. Your team expects effective progress, high productivity, and low project risk. If the team is dissatisfied too long, they can leave the project and YOU LOSE!", when : 1 },
			textInterfaceProjectRisk : { text : "Risk", when : 0 },
			textInterfaceProjectRiskTooltip : { text : "Overall level of project risk. It indicates the possibility of project failure. The customer and the team do not like high risk. Eliminate threats and exploit opportunities to reduce the overall project risk.", when : 1 },
			textInterfaceProjectScore : { text : "Score", when : 0 },
			textInterfaceProjectScoreTooltip : { text : "Your game score. You gain points for product development, eliminating threats, exploiting opportunities, keeping high product quality, high customer and team satisfaction, and low overall risk. Get the most points and become the best project manager!", when : 1 },
			
			textProductValueImgTooltip : { text : "Value for customer - influences project scope", when : 1 },
			textProductQualityImgTooltip : { text : "Influences project quality", when : 1 },
			textProductSizeImgTooltip : { text : "Effort", when : 1 },
			textResourceProductivityImgTooltip : { text : "Resource productivity", when : 1 },
			textRiskInfluenceImgTooltip : { text : "Risk influence", when : 1 },
			textAutopause : { text : "Autopause", when : 0 },
			
			textInterfaceTimeUnitDay : { text : "day", when : 0 },
			textInterfaceTimeUnitWeek : { text : "week", when : 0 },
			textInterfaceTimeUnitMonth : { text : "month", when : 0 },
			textInterfaceTimeUnitYear : { text : "year", when : 0 }
		}
	}
	this.choosenLanguage = "pl";
}
/* @arg language = pl, en 
 * @return pozostałe, które są ładowane w trakcie gry
 * W taki sposób, aby zaoszczędzić trochę na pamięci
 */
Language.prototype.switchLanguage = function(language) {
	if(!this.properties.hasOwnProperty(language)) language = "pl";
	this.choosenLanguage = language;
	var loadedLater = {};
	this.loadedLaterTooltips = {};
	for(var languageProperty in this.properties) {
		if(languageProperty.toString() == language.toString()) {
			var lang = this.properties[languageProperty];
			for(var fieldId in lang) {
				if(!lang.hasOwnProperty(fieldId)) continue;
				if(lang[fieldId].when == 0) {
					$("#"+fieldId).html(lang[fieldId].text.replace(/\\n/g,'<br/>'));
				}
			}
			break;
		}
	} 	
}
	
Language.prototype.getTooltipsText = function(language) {
	var loadedLaterTooltips = {};
	for(var fieldId in this.properties[this.choosenLanguage]) {
		if(fieldId.indexOf("Tooltip") > -1) {
			loadedLaterTooltips[fieldId] = this.properties[this.choosenLanguage][fieldId];
		}
	}
	return loadedLaterTooltips;
}

Language.prototype.getDialogsText = function(language) {
	var loadedLaterDialogs = {};
	for(var fieldId in this.properties[this.choosenLanguage]) {
		if(fieldId.indexOf("textPopup") > -1) {
			loadedLaterDialogs[fieldId] = this.properties[this.choosenLanguage][fieldId];
		}
	}
	return loadedLaterDialogs;
}