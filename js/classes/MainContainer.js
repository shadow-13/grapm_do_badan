function MainContainer() {
	this.productsFactory = new ProductsFactory(this);
	this.risksFactory = new RisksFactory(this);
	this.playFactory = new PlayFactory(this);
	this.tasksFactory = new TasksFactory(this);
	this.solutionsFactory = new SolutionsFactory(this);
	this.resourcesFactory = new ResourcesFactory(this);
	this.tryb = 0;
}

MainContainer.prototype.init = function(schemeId, tryb, whenReady) {
	Config = new Config();
	uniqueId = getUniqueUserId();
	
	this.tryb = tryb;
	/*Wczytywanie z bazy */
	var json = "";
	var request= new XMLHttpRequest();
	request.open("GET", "php/scheme.php?id="+schemeId, true);
	request.send(null);
	var self = this;
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			json = request.responseText;
			json = JSON.parse(json);

			showStartPopup(json.Project[0].Project.replace('\n','<br>'));
			
			Config.loadConfig(json.Config);
			self.playFactory.ticket();
			
			self.productsFactory.loadProducts(json.Features);
			self.productsFactory.showProducts();
			
			self.risksFactory.loadRisks(json.Risks);
			
			self.solutionsFactory.loadSolutions(json.Actions);
			self.solutionsFactory.showSolutions();
			
			self.resourcesFactory.loadResources(json.Resources);
			self.resourcesFactory.showResources();
			
			setImgTooltips();
			
			self.tasksFactory.initTasks();
			self.productsFactory.initProducts();
			self.solutionsFactory.initSolutions();
			self.playFactory.initParametersFromProject(json.Project[0]);
			self.playFactory.logMaker.getStartData();

			$("#controls").on("click", "#progressbar", (function() {self.playFactory.changeState();}));

			whenReady();
		}
	};
	
	
	
}

function getUniqueUserId() {
	/*funkcja z http://guid.us/GUID/JavaScript do wygenerowania mocno unikalnego id dla danego użytkownika*/
	function S4() {
	    return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
	}	
	return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
}

/* zwraca potrzebne informacje do wyświetlenia początkowego menu */
function getAllProjectsInfo() {
	
}