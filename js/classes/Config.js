/* Klasa globalna inicjowana w MainContainer.init() */
function Config() {

	this.takt = 2; // co x procent ogólnego postępu wykonuje się aktualizacja statystyk
	this.scoreScale = 0.1;
	// Początkowe wartości
	this.initialCustomerSatisfaction = 5;
	this.initialTeamHappiness = 5;
	
	// Zmiennosc zadowolenia klienta
	this.customerSatisfactionChange = 0.05;
	// Zmiennosc zadowolenia zespolu
	this.teamHappinessChange = 0.05;
	
	this.showIntro = true;
	this.debug = false; // do wyświetlania w consoli informacji
	
	/* RYZYKO */
	this.riskTimePerTickets = 5; // co x sekund jest losowana szansa na zainstnienie ryzyka
	this.riskDisappearTimePerTickets = 10; // co x sekund jest losowana szansa na zniknięcie aktywnego ryzyka
	this.riskChance = 20; // szansa na ryzyko
	this.riskDisappearChance = 20 // szansa na zniknięcie aktywnego ryzyka
	this.riskTicketsLeftInterval = [10,30]; //losowy przedział czasowy (sekundy) ile zostało do wykonania się ryzyka
	this.riskShowTime = 5000; // ile milisekund ma się wyświetlać informacja o zaistniałym ryzyku
	
	this.maxUnhappyTeamDays = 10; //ile dni zadowolenie zespolu moze byc niskie zanim przerwie to gre
	this.maxUnhappyCustomerDays = 10; //ile dni zadowolenie klienta moze byc niskie zanim przerwie to gre
	
	this.minTeamHappiness = 3; //ile wynosi zadowolenie zespolu od ktorego zaczyna grozic koniec gry
	this.minCustomerSatisfaction = 3; //ile wynosi zadowolenie klienta od ktorego zaczyna grozic koniec gry
	
	this.pauseScorePenalty = 100; //ile punktow odejmie od wyniku reczne nacisniecie przez gracza pauzy
	
	/* CACHE */
	this.riskEventsMaxMutexSize = 8;
	
	/*FPS */
	this.milisecondsPerTicket = 100; // x milisekund zajmuje wykonanie 1 ticketu 
	
	/* CZAS */
	this.pointPerTickets = 10; // x sekund zajmuje wykonanie 1 punktu productivity
	this.ticketsPerTimeUnit = 10; // x sekund zajmuje wykonanie się 1 jednostki czasu; do wyświetlania
	this.calculateTicketsFromSeconds();
	
}

//OBLICZANIE WYNIKU
Config.prototype.returnScore = function (previousScore,previousCompletion,completion,teamHappiness,customerSatisfaction,riskFigtingPercentage) {
	var score = previousScore + this.scoreScale * ( ( customerSatisfaction * 100 ) + ( teamHappiness * 100 ) + ( ( completion - previousCompletion ) * 10 ) + riskFigtingPercentage );
	return score;
}

//AKTUALIZACJA ZADOWOLENIA KLIENTA
Config.prototype.returnCustomerSatisfaction = function (completion,procentageTime,currentQuality,MaxQuality,riskFigtingPercentage) {
	var result = (( completion / procentageTime ) * 100 + ( (currentQuality - MaxQuality/2) / (MaxQuality/2) * 100 ) + ( 100 - riskFigtingPercentage )) / 3 / 20;
	if(this.debug === true) console.log("Zadowolenie klienta: " + Math.floor(completion / procentageTime) * 100 + "% + " + Math.floor( currentQuality / MaxQuality * 100 ) + "% + " + Math.floor( 100 - riskFigtingPercentage ) + "%" );
	
	if(result > 5) result = 5;
	if(result < 0) result = 0;
	
	return result;
}

//AKTUALIZACJA ZADOWOLENIA ZESPOLU
Config.prototype.returnTeamHappiness = function ( startingProductivity, actualProductivity, completion, procentageTime, riskFigtingPercentage ) {
	var result = (( actualProductivity / startingProductivity * 100 ) + ( completion / procentageTime * 100 ) + ( 100 - riskFigtingPercentage ) ) / 3 / 20;
	if(this.debug === true) console.log("Zadowolenie zespolu: " + Math.floor( actualProductivity / startingProductivity * 100 ) + "% + " + Math.floor( completion / procentageTime * 100 ) + "% + " + Math.floor( 100 - riskFigtingPercentage ) + "%");
	
	if(result > 5) result = 5;
	if(result < 0) result = 0;
	
	return result;
}

//PRZELICZENIE JEDNOSTEK Z SEKUND NA TICKETY
Config.prototype.calculateTicketsFromSeconds = function() {
	var value = 1000/this.milisecondsPerTicket;
	
	this.pointPerTickets = this.pointPerTickets * value;
	this.ticketsPerTimeUnit = this.ticketsPerTimeUnit * value;
	this.riskTimePerTickets = this.riskTimePerTickets * value;
	this.riskDisappearTimePerTickets = this.riskDisappearTimePerTickets * value;
	this.riskTicketsLeftInterval = [this.riskTicketsLeftInterval[0]*value, this.riskTicketsLeftInterval[1]*value];
}

Config.prototype.loadConfig = function(json) {
	if(json == undefined) return;
	for(var i = 0 ;i<json.length; i++) {
		if(json[i].Value.charAt(0) == '[') {
			var result = json[i].Value.replace(/\[|\]/g,'');
			result = result.split(',');
			this[json[i].Name] = result;
		} else {
			this[json[i].Name] = json[i].Value;
		}
	}
	this.calculateTicketsFromSeconds();
}
