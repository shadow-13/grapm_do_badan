function PlayFactory(parent) {
	//pobiera wartość z configa przy inicjalizacji
	this.pauseClicked = false;
	this.currentDate; //data wyrazona w ilosciach godzin, które mineły od początku gry
	this.completion = 0;
	this.score = 0;
	this.customerSatisfaction = 5;
	this.teamHappiness = 5;
	this.quality = 6; // Wartości od 1 do 11
	this.parent = parent;
	this.previousCompletion = 0;
	this.riskFigtingPercentage = 10;
	this.productFactory = parent.productsFactory;
	this.maxScenarioTime = 0; // wyrazone w jednostkach
	this.startingProductivity = 0;
	this.unhappyTeamDays = 0;
	this.unhappyCustomerDays = 0;
	this.minTeamHappiness = 3; 
	this.minCustomerSatisfaction = 3;
	this.dialogsDict = globalLanguage.getDialogsText();
	this.pauseScorePenalty = 100;
	this.logMaker = new LogMaker(this);
	this.allRisksMaterialisedEverCount = 0;
	this.allThreatsMaterialisedEverCount = 0;
	this.lastPauseStop;
	this.lastPauseStart;
	this.gameRating;
	this.isAutopause = false;
	
}

PlayFactory.prototype.getTimeProcentage = function() {
	var result = ( this.currentDate / this.maxScenarioTime ) * 100;
	if (result < 1) result = 1;
	return result;
}

PlayFactory.prototype.updateStatistics = function() {
	this.score = Config.returnScore(this.score,this.previousCompletion,this.productFactory.getAllProgressInPoints(),this.teamHappiness,this.customerSatisfaction,this.riskFigtingPercentage);
	this.previousCompletion = this.completion;
}

// Aktualizacja statystyk - głównie w lewym górnym panelu
PlayFactory.prototype.updateStatisticsNow = function(value) {
	var self = this;
	// Zakres
    self.completion = value;
    $('#progress1Text').text(Math.floor(self.completion) + "%");
    $('#progressbar').progressbar("option", "value", Math.floor(self.getCurrentProgress())); 
    $('#progress1').progressbar("option", "value", Math.floor(self.completion));
    this.actualizeColorOfProgressbar("#progressbar", "#66b3ff");
    this.actualizeColorOfProgressbar("#progress1", "#0080ff");
    
    // Wynik
    this.updateScore();
    
    // Klient
    $('#progress2').progressbar("option", "value", self.customerSatisfaction*18+10);
    
    // Jakość
    this.actualizeQuality();
    
    // Zespół
    $('#progress4').progressbar("option", "value", self.teamHappiness*18+10);
    
    // Czas
    $('#valueInterfaceTimeUnit').text(Math.floor(self.currentDate/24));
    $('#progress5').progressbar("option", "value", Math.floor(self.currentDate / self.maxScenarioTime * 100 % 100));
    if( self.currentDate / self.maxScenarioTime * 100 <= 100 ) {
    	$( "#progress5" ).css({
        	"background": "white"
        });
    	this.actualizeColorOfProgressbar("#progress5", "#0080ff");
    }
    else if( self.currentDate / self.maxScenarioTime * 100 > 100 ) {
    	$( "#progress5" ).css({
        	"background": "#0080ff"
        });
    	this.actualizeColorOfProgressbar("#progress5", "#FF0000");
    }
    
    // Ryzyko
    $('#progress6Text').text(Math.floor(self.riskFigtingPercentage) + "%");
    $('#progress6').progressbar("option", "value", Math.floor(self.riskFigtingPercentage));
    
    this.actualizeColorOfAllProgressbars(self.customerSatisfaction*20, self.teamHappiness*20, 100-10);
}

PlayFactory.prototype.actualizeQuality = function() {
	var middle = 6;
	var tmp = middle-this.quality;
	for (var int = 1; int <= 11; int++) {
		if(tmp > 0 && this.quality <= int && int < middle) {
			$( "#quality" + int ).progressbar({value: 100});
		} else if(tmp < 0 && middle < int && int <= this.quality) {
			$( "#quality" + int ).progressbar({value: 100});
		} else {
			$( "#quality" + int ).progressbar({value: 0});
		}
	}
	$( "#quality" + middle ).progressbar({value: 100});
}

PlayFactory.prototype.actualizeColorOfAllProgressbars = function(value2,value4,value6) {
    this.actualizeColorOfProgressbarOnValue('#progress2',value2);
    this.actualizeColorOfProgressbarOnValue('#progress4',value4);
    this.actualizeColorOfProgressbarOnValue('#progress6',value6);
}

PlayFactory.prototype.actualizeColorOfProgressbar = function(progressbar,color) {
    progress = $( progressbar );
    progressValue = progress.find( ".ui-progressbar-value" );
    progressValue.css({
    	"background": color
    });
}

PlayFactory.prototype.actualizeColorOfProgressbarOnValue = function(progressbar,valuePercentage) {
    if( valuePercentage <= 20 ) {
    	this.actualizeColorOfProgressbar(progressbar, "#ff8080");
    }
    else if( valuePercentage <= 40 ) {
    	this.actualizeColorOfProgressbar(progressbar, "#ff8040");
    }
    else if( valuePercentage <= 60 ) {
    	this.actualizeColorOfProgressbar(progressbar, "#ffff80");
    }
    else if( valuePercentage <= 80 ) {
    	this.actualizeColorOfProgressbar(progressbar, "#80ff80");
    }
    else {
    	this.actualizeColorOfProgressbar(progressbar, "#25df20");
    }
}

PlayFactory.prototype.updateCustomerSatisfaction = function() {
	this.customerSatisfaction = Config.returnCustomerSatisfaction(this.completion,this.getTimeProcentage(),this.quality-1,10,this.riskFigtingPercentage);
}

PlayFactory.prototype.updateTeamHappiness = function() {
	this.teamHappiness = Config.returnTeamHappiness(this.startingProductivity, this.parent.resourcesFactory.getAllProductivity(), this.completion, this.getTimeProcentage(), this.riskFigtingPercentage);
}

PlayFactory.prototype.setQualityProgressbarColors = function() {
	this.actualizeColorOfProgressbar("#quality1", "#800000");
	this.actualizeColorOfProgressbar("#quality2", "#CF0000");
	this.actualizeColorOfProgressbar("#quality3", "#FF3300");
	this.actualizeColorOfProgressbar("#quality4", "#FF9900");
	this.actualizeColorOfProgressbar("#quality5", "#FFDF00");
	this.actualizeColorOfProgressbar("#quality6", "#FFFF00");
	this.actualizeColorOfProgressbar("#quality7", "#D7FC03");
	this.actualizeColorOfProgressbar("#quality8", "#98FC03");
	this.actualizeColorOfProgressbar("#quality9", "#33FF00");
	this.actualizeColorOfProgressbar("#quality10", "#33CC00");
	this.actualizeColorOfProgressbar("#quality11","#339900");
}

PlayFactory.prototype.ticket = function () {
	var self = this;
	
	self.currentDate = 0;
	self.pauseClicked = true;
	
	// INICJOWANIE PROGRESSBARÓW
	$( "#progressbar" ).css({"visibility": "visible"});
    $('#progressbar').progressbar("option", "value", 0);

    var progressbar1 = $( "#progress1" );
    progressbar1.progressbar({value: false});
    var progressbar2 = $( "#progress2" );
    progressbar2.progressbar({value: false});
    var progressbar4 = $( "#progress4" );
    progressbar4.progressbar({value: false});
    var progressbar5 = $( "#progress5" );
    progressbar5.progressbar({value: false});
    var progressbar6 = $( "#progress6" );
    progressbar6.progressbar({value: false});
    
    // INIT QUALITY PROGRESSBAR
    for (var int = 1; int <= 11; int++) {
    	$( "#quality" + int ).progressbar({value: false});
	}
    this.setQualityProgressbarColors();
    this.createQuitDialog();
	$('#backToWelcome').click(function() { 
		self.openQuitDialog(); 
		});
    
	self.customerSatisfaction = Config.initialCustomerSatisfaction;
	self.teamHappiness = Config.initialTeamHappiness;
	
	var pointPerTickets = Config.pointPerTickets; // x ticketów = wykonanie 1 punktu
	
	var ticketCounter = 0;
	var previousTicketCounter = 0;
	
	var maxUnhappyTeamDays = Config.maxUnhappyTeamDays;
	var maxUnhappyCustomerDays = Config.maxUnhappyCustomerDays;
	
	self.minCustomerSatisfaction = Config.minCustomerSatisfaction;
	self.minTeamHappiness = Config.minTeamHappiness;
	
	self.pauseScorePenalty = Config.pauseScorePenalty;
	
	self.updateStatisticsNow(0);
	
	setInterval(function() {
		if(self.pauseClicked === false){
			// Obliczanie liczby dni, godzin i minut na podstawie realnej liczby sekund
			var days = Math.floor(self.currentDate / 24) % 30;
			var hours = Math.floor(self.currentDate) % 24;
			
		    var oldDate = Math.floor(self.currentDate/24);
			// Zwiększanie czasu 
			self.currentDate = self.currentDate + 24 / Config.ticketsPerTimeUnit;
			
			if(oldDate != Math.floor(self.currentDate/24)) {
				if(self.teamHappiness <= self.minTeamHappiness) {
					self.unhappyTeamDays += 1;
				}
				else {
					self.unhappyTeamDays = 0;
				}
				if(self.customerSatisfaction <= self.minCustomerSatisfaction) {
					self.unhappyCustomerDays += 1;
				}
				else {
					self.unhappyCustomerDays = 0;
				}
			}
			
			//Sprawdzanie warunkow konca gry
			if(self.unhappyTeamDays >= maxUnhappyTeamDays) {
		    	var title = self.dialogsDict["textPopupEndTitleLost"].text;
		    	var text = self.dialogsDict["textPopupEndContentsLostTeam"].text;
		    	self.gameOverDialog(title, text, 0);
			}
			else if(self.unhappyCustomerDays >= maxUnhappyCustomerDays) {
		    	var title = self.dialogsDict["textPopupEndTitleLost"].text;
		    	var text = self.dialogsDict["textPopupEndContentsLostClient"].text;
		    	self.gameOverDialog(title, text, 0);
			}
			else if(self.riskFigtingPercentage == 100) {
		    	var title = self.dialogsDict["textPopupEndTitleLost"].text;
		    	var text = self.dialogsDict["textPopupEndContentsLostRisk"].text;
		    	self.gameOverDialog(title, text, 0);
			}
		    
		    //Przeliczanie produktywności do zadań
		    var tasksList = self.parent.tasksFactory.tasksList;
		    tasksList.map(function(task) {
		    	task.resources.map(function(resource){
		    		task.addProgress(resource.productivity);
		    	});
		    });
		    
		    //aktualizacja postępu
		    var value = self.parent.productsFactory.getAllProgress(); // aktualny postęp prac
		    
		    //ryzyko
		    self.parent.risksFactory.checkAndIncreaceRiskChance();
		    $.each(self.parent.risksFactory.visibleRisksList,function(i,risk){
		    
		    	if(risk == undefined) return;
		    	risk.ticketsLeft --;
		    	if(risk.mutex == false && risk.ticketsLeft <= 0 && risk.visible == true && risk.succeed == false) { //czas na wykonanie ryzyka(szansa)
		    		if(risk.mutex == true) return;
		    		risk.mutex = true;
		    		if(Config.debug) console.log(i+"Czas minął, wykonanie ryzyka(szansa):" +risk.toString());
		    		risk.rollEvent(); //wykonanie ryzyka(szansa)
		    		if(risk.succeed == true) {
		    			
		    			self.allRisksMaterialisedEverCount += 1;
		    			if(!risk.isGood) {
		    				self.allThreatsMaterialisedEverCount += 1;
		    			}
		    			
		    			var jRisk = $("#risks #"+risk.id);
			    		var height = jRisk.height();
			    		var maxHeight = Math.floor(height * 1.30);
			    		if(jRisk.css('font-size') == undefined) 
			    			console.log("error");
			    		var fontSize = jRisk.css('font-size').replace('px','');
			    		var maxFontSize = Math.floor(fontSize * 1.3);
			    		
			    		jRisk.animate({ height: maxHeight, fontSize:maxFontSize }, 500 , function(){
			    			jRisk.animate({ height: height, fontSize:fontSize }, 200,function(){
				    			risk.removeVisible(risk.isGood);
				    		} );
			    		});
		    					    			
		    		} else {
		    			risk.ticketsLeft = risk.tickets/2;  //ponownie wywołanie ryzyka za 1/2 ticketów
		    			risk.mutex = false;
		    		}
		    		
		    	}
		    });		    

	    	self.updateCustomerSatisfaction();
	    	self.updateTeamHappiness();
		    
	    	self.updateStatisticsNow(value);
	        self.actualizeColorOfAllProgressbars(self.customerSatisfaction*20, self.teamHappiness*20, 100-self.riskFigtingPercentage);
		    
			// Działanie TAKTów o których mówił p. Miler
		    ticketCounter = self.parent.productsFactory.getAllProgress();
		    if( ticketCounter - previousTicketCounter >= Config.takt ){
		    	previousTicketCounter = ticketCounter;
		    	
		    	// Aktualizacja statystyk
		    	self.updateStatistics();
		    }
		    
		    if(value == 100) {
		    	var title = self.dialogsDict["textPopupEndTitleSucceed"].text;
		    	var text = self.dialogsDict["textPopupEndContentsSucceed"].text;
		    	self.gameOverDialog(title, text, 1);
		    }
		    else if(self.parent.tryb == 1 && self.currentDate >= self.maxScenarioTime ) {
		    	var title = self.dialogsDict["textPopupEndTitleTimeOut"].text;
		    	var text = self.dialogsDict["textPopupEndContentsTimeOut"].text;
		    	self.gameOverDialog(title, text, 2);
		    }		    
		    else if(self.parent.tryb == 2 && self.currentDate >= self.maxScenarioTime * 2 ) {
		    	var title = self.dialogsDict["textPopupEndTitleTimeOut"].text;
		    	var text = self.dialogsDict["textPopupEndContentsTimeOut"].text;
		    	self.gameOverDialog(title, text, 2);
		    }
		    
		  //autopauza gdy brak roboty lub nowe ryzyko w miedzyczasie
		    if(self.parent.tasksFactory.tasksList.length == 0 || self.isAutopause === true) { 
		    	self.doPause(true);
		    	return;
		    }
		}
	    
	}, Config.milisecondsPerTicket);
}

PlayFactory.prototype.changeState = function(isAutopause) {
	if(this.pauseClicked === true) this.undoPause(isAutopause);
	else this.doPause(isAutopause);
}

/*przycisk Pause */
PlayFactory.prototype.doPause = function(isAutopause) {
	this.pauseClicked = true;
	this.lastPauseStart = new Date();
	$('#progressText').attr('src','img/play.png');
	this.parent.tasksFactory.keepAllocation = false;
	$("#resources").sortable("enable");
	$("#tasks").sortable("enable");
	$("#solutions").sortable("enable");
	$("#products").sortable("enable");
	$("#tasks .close").css({"visibility": "visible"});
	this.parent.tasksFactory.hideAssignedResourcesShorts();
	
	if(isAutopause != true) {
		this.score -= this.pauseScorePenalty;
		this.updateScore();
	}
	else {
    	$('#autopausePopup').fadeIn(100).fadeOut(1600);
	}
}
/* przycisk Play */
PlayFactory.prototype.undoPause = function() {
	if(this.parent.tasksFactory.tasksList.length == 0) return;
	$('#progressText').attr('src','img/pause.png');
	this.parent.resourcesFactory.freeAllResources();
	this.parent.resourcesFactory.reorderResources();
	this.parent.tasksFactory.reorderTasks();
	this.parent.tasksFactory.allocateResources();
	this.pauseClicked = false;
	$("#resources").sortable("disable");
	$("#tasks").sortable("disable");
	$("#solutions").sortable("disable");
	$("#products").sortable("disable");
	$("#tasks .close").css({"visibility": "hidden"});
	this.logMaker.logGameState();
	this.lastPauseStop = new Date();//.getTime();
	this.isAutopause = false;
}

PlayFactory.prototype.initParametersFromProject = function(json) {
	var unit = json.Unit.toLowerCase();
	var textInterfaceTimeUnit = "textInterfaceTimeUnit";
	textInterfaceTimeUnit += unit.charAt(0).toUpperCase() + unit.slice(1);
	setUnitSpan(textInterfaceTimeUnit);
	this.maxScenarioTime = json.Time * 24;
	$('#textInterfaceSchemeTitle').html(json.Name);
}

PlayFactory.prototype.gameOverDialog = function(title, text, won) {
	 this.pauseClicked = true;
	 $("#tasks .close").click();
	 $("#tasks").sortable("enable");
	 this.parent.tasksFactory.keepAllocation = false;
	 this.parent.resourcesFactory.freeAllResources();
	 
	 var scoreText = $("#textInterfaceProjectScore").text() + ": <span id='endScore'>" + $('#valueInterfaceProjectScore').text() + "</span>";
	 
	 var titleClass;	 
	 if (won == 0) {titleClass = "class='lost'"}
	 else if(won == 1) {titleClass = "class='won'"}
	 
	 var starsText = this.dialogsDict["textPopupEndContentsStars"].text;
	 var feedbackStars = "<img id='ocena' src='img/ocena0.gif' usemap='#stars'><map name='stars'>" +
	 	"<area id='stars1' class='stars' shape='rect' coords='0,0,54,60'>" +
	 	"<area id='stars2' class='stars' shape='rect' coords='55,0,104,60'>" +
	 	"<area id='stars3' class='stars' shape='rect' coords='105,0,154,60'>" +
	 	"<area id='stars4' class='stars' shape='rect' coords='155,0,204,60'>" +
	 	"<area id='stars5' class='stars' shape='rect' coords='205,0,260,60'>" +
	 	"</map>";
	 
	 $('#dialog').append("<p class='center'><span id='popupTitle' " + titleClass + ">" + title + "</span><br\>" + text + "<br\>" + scoreText + "</p><br\>");
	 $("#mainMenu").clone().appendTo( "#dialog" );
	 $('#dialog').append("<p class='center'><span id='starsText'>" + starsText + "</span><span id='feedbackStars'>" + feedbackStars + "</span></p>");
	 
	 var self = this;
	 
	 $("#dialog").on("mouseover", ".stars", (function() {self.showStars(this.id);}));
	 $("#dialog").on("click", ".stars", (function() {self.sendStars(this.id);}));
	 $("#dialog").dialog({
		 buttons: [{
			 text: "OK",      
			 click: function() {        
				 $( this ).dialog( "close" );
				 $('#allPanelGame').fadeOut(function() {
					 window.location.href = "";
					 });
				 }    
		 }],
		 dialogClass: "rounded noTitleBar", 
		 height: $(window).height()*0.80,
		 widht: $(window).width()*0.4767,
		 minHeight: $(window).height()*0.34,
		 minWidth: $(window).width()*0.4767
		 });
	 $('#dialog').dialog("open");
	 
}

PlayFactory.prototype.createQuitDialog = function() {
	var self = this;
	var dialogButtons = {};
	var yesButton = this.dialogsDict["textPopupQuitYesButton"].text;
	var noButton = this.dialogsDict["textPopupQuitNoButton"].text;
	
	dialogButtons[yesButton] = function() { 
		$( this ).dialog( "close" ); 
		$('#allPanelGame').fadeOut(function() {
			window.location.href = "";
		 });
		};
	dialogButtons[noButton] = function() {
		$( this ).dialog( "close" ); 
		self.pauseClicked = $('#quitPopup').data("wasPaused");	 
		};
	
	$('#quitPopup').text(this.dialogsDict["textPopupQuitContent"].text);
	$('#quitPopup').dialog({		
		autoOpen: false,
		width: 400,
		close: function() { $('html').removeClass('overlay'); },
		position: { my: "center", at: "center", of: window },
		buttons: dialogButtons,
		dialogClass: "noTitleBar"
	});
}

PlayFactory.prototype.getCurrentProgress = function() {
	var allCurrentProgress = 100* this.parent.tasksFactory.tasksList.length;
	var currentProgress = 0;
	for(var i=0;i<this.parent.tasksFactory.tasksList.length;i++) {
		currentProgress += this.parent.tasksFactory.tasksList[i].progress;
	}
	return currentProgress / allCurrentProgress * 100;
}
PlayFactory.prototype.changeRiskFigtingPercentageBy = function(value) {

	this.riskFigtingPercentage += value;
	if(this.riskFigtingPercentage > 100) {
		this.riskFigtingPercentage = 100;
	}
	if(this.riskFigtingPercentage < 10) {
		this.riskFigtingPercentage = 10;
	}	
}

PlayFactory.prototype.updateScore = function() {
	$('#valueInterfaceProjectScore').text(Math.floor(this.score));
}
PlayFactory.prototype.openQuitDialog = function() {
	var wasPaused = this.pauseClicked;	
	this.pauseClicked = true;
	
	$('html').addClass('overlay');
	$('#quitPopup').data("wasPaused", wasPaused).dialog("open");
}

PlayFactory.prototype.showStars = function(starsId) {
	var starsNumber = starsId.slice(-1);
	document.getElementById('ocena').src='img/ocena' + starsNumber +'.gif';
}

PlayFactory.prototype.sendStars = function(starsId) {
	var starsNumber = starsId.slice(-1);
	$("#dialog").off("click", ".stars");
	$("#dialog").off("mouseover", ".stars");
	this.gameRating = starsNumber;
	this.logMaker.logGameRating();
}

PlayFactory.prototype.makeAutopause = function() {
	this.isAutopause = true;
}