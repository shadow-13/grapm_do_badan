function SolutionsFactory(parent) {
	this.solutionsList = [];
	this.solutionsOnTasksList = [];
	this.parent = parent;
}

SolutionsFactory.prototype.initSolutions = function() {
	var self = this;
	
	$( "#solutions" ).sortable({connectWith: "#tasks",
		forcePlaceholderSize: false,
	    helper: function(e,li) {
	        copyHelper= li.clone().insertAfter(li);
	        return li.clone();
	    },
	    stop: function() {
	        copyHelper && copyHelper.remove();
	    },
	    over: function (event, ui) {
			if(ui.item.hasClass("product")){
				ui.item.addClass("youShallNotPass");
			}
			else if(ui.item.hasClass("solution")){
				ui.item.removeClass("youShallNotPass");
			}
		},
		receive: function (event, ui) {
			if(ui.item.hasClass("solution")){
				var thisObj = $("#"+ ui.item.attr("id"));
				self.parent.tasksFactory.freeTaskResources(thisObj, false);
				self.deleteFromSolutionsOnTasksList(thisObj.attr("id"));
				thisObj.remove();
			}
		}
	});
}

SolutionsFactory.prototype.loadSolutions = function (solutions) {
	var self = this;
	$.each(solutions, function( i, item ) {     
		var solution = new Solution(item, self);	
		self.solutionsList.push(solution);
	});
}

SolutionsFactory.prototype.showSolutions = function() {
	var result = "";
	$.each(this.solutionsList, function( i, item ) {  
			result += getSolutionLi(item);
	});
	$( "#solutions" ).html(result);
}

SolutionsFactory.prototype.deleteFromSolutionsOnTasksList = function(solutionId) {
	var solutionsOnTasksList = this.solutionsOnTasksList;
	var i = solutionsOnTasksList.length;
	while(i--) {
		if(solutionId == solutionsOnTasksList[i]) {
			solutionsOnTasksList.splice(i, 1);
		};
	};
}