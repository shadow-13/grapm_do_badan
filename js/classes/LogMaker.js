/* Klasa konstruująca string do logowania */

function LogMaker(parent) {
	this.parent = parent;
	this.logMessage = "";
	
	this.activeRisks = 0;
	this.plannedToDoSolutions = 0;
	this.solutionsWithResourcesAssigned = 0;
}

LogMaker.prototype.getStartData = function() {
	this.startingProductivity = this.parent.startingProductivity;
	this.initialResourcesCount = this.parent.parent.resourcesFactory.initialResourcesList.length;
	this.initialProductCount = this.parent.parent.productsFactory.productsList.length;
	this.initialProductSize = this.parent.parent.productsFactory.getAllProductsSize();
	this.averageProductSize = (this.initialProductSize / this.initialProductCount).toFixed(2);
}

LogMaker.prototype.logGameState = function() {
	this.parent.parent.tasksFactory.allocateResourcesNOW();
	this.addData(uniqueId);
	this.getTimeInfo();
	this.getRangeInfo();
	this.getQualityInfo();
	this.getSatisfactionInfo();
	this.getResourcesInfo();
	this.getProductInfo();
	this.getTaskInfo();
	this.getPointsInfo();
	this.getRiskInfo();
	this.getDecisionInfo();
	this.getExtraRiskInfo();
	log.info(this.logMessage);
	this.clearLogMessage();
}

LogMaker.prototype.clearLogMessage = function() {
	this.logMessage = "";
}

LogMaker.prototype.addData = function(dataString) {
	this.logMessage += dataString + ";";
}

LogMaker.prototype.zeroIfNan = function(variable) {
	if(variable == "NaN" || !variable ){
		variable = 0;
		}
	return (parseFloat(variable)).toFixed(2);
}

LogMaker.prototype.getTimeInfo = function() {
	var time = this.parent.getTimeProcentage();
	time = time.toFixed(2);
	
	var timeLeft = 100 - time;
	
	this.addData(time);
	this.addData(timeLeft);
}

LogMaker.prototype.getRangeInfo = function() {
	var completion = this.parent.completion;
	completion = completion.toFixed(2);
	
	var rangeToDo = 100 - completion; 
	
	this.addData(completion);
	this.addData(rangeToDo);
}

LogMaker.prototype.getQualityInfo = function() {
	var quality = this.parent.quality;
	
	quality = quality - 6; //bo chcemy miec wartosci <-5;5>
	
	this.addData(quality);
}

LogMaker.prototype.getSatisfactionInfo = function() {
	var customerSatisfaction = this.parent.customerSatisfaction;
	var teamHappiness = this.parent.teamHappiness;
	
	customerSatisfaction = (customerSatisfaction * 20).toFixed(2);
	teamHappiness = (teamHappiness * 20).toFixed(2);
	
	this.addData(customerSatisfaction);
	this.addData(teamHappiness);
}

LogMaker.prototype.getResourcesInfo = function() {
	var currentProductivity = this.parent.parent.resourcesFactory.getAllProductivity();
	var currentResourcesCount = this.parent.parent.resourcesFactory.resourcesList.length;
	var averageProductivity = (currentProductivity / currentResourcesCount).toFixed(2);
	var freeResourcesCount = this.parent.parent.resourcesFactory.getFreeResources().length;
	var busyResourcesCount = currentResourcesCount - freeResourcesCount;
	
	this.addData(this.startingProductivity);
	this.addData(currentProductivity);
	this.addData(this.initialResourcesCount);
	this.addData(currentResourcesCount);
	this.addData(averageProductivity);
	this.addData(freeResourcesCount);
	this.addData(busyResourcesCount);
}

LogMaker.prototype.getProductInfo = function() {
	var currentNotDoneProductCount = this.parent.parent.productsFactory.getNotDoneProductsCount();
	var currentDoneProductCount = this.initialProductCount - currentNotDoneProductCount;
	
	this.addData(this.initialProductCount);
	this.addData(this.initialProductSize);
	this.addData(this.averageProductSize);
	this.addData(currentDoneProductCount);
	this.addData(currentNotDoneProductCount);
}

LogMaker.prototype.getTaskInfo = function() {
	var allTasks = this.parent.parent.tasksFactory.tasksList.length;
	var tasksWithResourcesAssigned = this.parent.parent.tasksFactory.getTasksWithAssignedResourcesCount();
	var plannedToDoProducts = this.parent.parent.tasksFactory.getPlannedToDoProductCount();
	var productsWithResourcesAssigned = this.parent.parent.productsFactory.getProductsWithAssignedResourcesCount();
	this.plannedToDoSolutions = allTasks - plannedToDoProducts;
	this.solutionsWithResourcesAssigned = tasksWithResourcesAssigned - productsWithResourcesAssigned;
	var averageSizeOfPlannedSolutions = this.zeroIfNan(this.parent.parent.tasksFactory.getSolutionsOnTasksListSize() / this.plannedToDoSolutions);
	var averageTasksProgress = (this.parent.parent.tasksFactory.getAllTaskAverageProgressInPoints()).toFixed(2);
	
	this.addData(allTasks);
	this.addData(tasksWithResourcesAssigned);
	this.addData(plannedToDoProducts);
	this.addData(productsWithResourcesAssigned);	
	this.addData(this.plannedToDoSolutions);
	this.addData(this.solutionsWithResourcesAssigned);
	this.addData(averageSizeOfPlannedSolutions);
	this.addData(averageTasksProgress);
}

LogMaker.prototype.getPointsInfo = function() {
	var score = (this.parent.score).toFixed(2);
	this.addData(score)
}

LogMaker.prototype.getRiskInfo = function() {
	var riskFightingPercentage = this.parent.riskFigtingPercentage;
	this.activeRisks = this.parent.parent.risksFactory.visibleRisksList.length;
	var activeThreats = this.parent.parent.risksFactory.countVisibleThreats();
	var activeOpportunities = this.activeRisks - activeThreats;
	var allRisksEver = this.parent.parent.risksFactory.allRisksEverCount;
	var allThreatsEver = this.parent.parent.risksFactory.allThreatsEverCount;
	var allOpportunitiesEver = allRisksEver - allThreatsEver;
	var allRisksMaterialisedEver = this.parent.allRisksMaterialisedEverCount;
	var allThreatsMaterialisedEver = this.parent.allThreatsMaterialisedEverCount;
	var allOpportunitiesMaterialisedEver = allRisksMaterialisedEver - allThreatsMaterialisedEver;
	var allThreatsRemovedCount = this.parent.parent.risksFactory.allThreatsRemovedCount;
	var allThreatsRandomlyDisappearedCount = this.parent.parent.risksFactory.allThreatsRandomlyDisappearedCount;
	var allOpportunitiesMissedCount = this.parent.parent.risksFactory.allOpportunitiesMissedCount;
	var threatsProbabilitySum = (this.parent.parent.risksFactory.getVisibleThreatsProbabilitySum()).toFixed(2);
	var threatsProbabilityAverage = this.zeroIfNan((threatsProbabilitySum / activeThreats).toFixed(2));
	var threatsImpactSum = this.parent.parent.risksFactory.getVisibleThreatsImpactSum();
	var threatsImpactAverage = this.zeroIfNan((threatsImpactSum / activeThreats).toFixed(2));
	var opportunitiesProbabilitySum = (this.parent.parent.risksFactory.getVisibleOpportunitiesProbabilitySum()).toFixed(2);
	var opportunitiesProbabilityAverage = this.zeroIfNan((opportunitiesProbabilitySum / activeOpportunities).toFixed(2));
	var opportunitiesImpactSum = this.parent.parent.risksFactory.getVisibleOpportunitiesImpactSum();
	var opportunitiesImpactAverage = this.zeroIfNan((opportunitiesImpactSum / activeOpportunities).toFixed(2));
	
	this.parent.parent.tasksFactory.updateAllSolutionsEverData();
	var allSolutionsEverCount = this.parent.parent.tasksFactory.allSolutionsEverCount;
	var allEffectiveSolutionsEverCount = this.parent.parent.tasksFactory.allEffectiveSolutionsEverCount;
	var allNotEffectiveSolutionsEverCount = allSolutionsEverCount - allEffectiveSolutionsEverCount;
	var allUniqueSolutionsEverCount = this.parent.parent.tasksFactory.allUniqueSolutionsEverIds.length;
	
	
	this.addData(riskFightingPercentage);
	this.addData(this.activeRisks);
	this.addData(activeThreats);
	this.addData(activeOpportunities);
	this.addData(allRisksEver);
	this.addData(allThreatsEver);
	this.addData(allOpportunitiesEver);
	this.addData(allRisksMaterialisedEver);
	this.addData(allThreatsMaterialisedEver);
	this.addData(allOpportunitiesMaterialisedEver);
	this.addData(allThreatsRemovedCount);
	this.addData(allThreatsRandomlyDisappearedCount);
	this.addData(allOpportunitiesMissedCount);
	this.addData(threatsProbabilitySum);
	this.addData(threatsProbabilityAverage);
	this.addData(threatsImpactSum);
	this.addData(threatsImpactAverage);
	this.addData(opportunitiesProbabilitySum);
	this.addData(opportunitiesProbabilityAverage);
	this.addData(opportunitiesImpactSum);
	this.addData(opportunitiesImpactAverage);
	this.addData(allSolutionsEverCount);
	this.addData(allEffectiveSolutionsEverCount);
	this.addData(allNotEffectiveSolutionsEverCount);
	this.addData(allUniqueSolutionsEverCount);
}

LogMaker.prototype.getDecisionInfo = function() {
	// dostepne wartosci: accept, react, wait, not_applicable
	// accept - akceptuje/lekcewazy, nie reaguje
	// react - obniza zagrozenie lub maksymaliuje szanse = realizuje obecnie jakies dzialania wobec ryzyka
	// wait - obserwuje ryzyko, dzialania przeniosl na liste zadan ale nie do realizacji natychmiast
	// not_applicable - nie ma obecnie ryzyka
	var decision = "not_applicable";
	
	if (this.activeRisks > 0){
		if (this.solutionsWithResourcesAssigned > 0){
			decision = "react";
		}
		else if (this.plannedToDoSolutions > 0) {
			decision = "wait";
		}
		else {
			decision = "accept";
		}
	}
	this.addData(decision);
}

LogMaker.prototype.getExtraRiskInfo = function() {
	this.addData("extra risk info start");
	var self = this;
	var log_string = "";
	
	if (this.activeRisks > 0) {
		var timeSinceLastPause = (this.parent.lastPauseStart.getTime() - this.parent.lastPauseStop.getTime()) / 1000;
		
		log_string = "[ ";
		$.each(this.parent.parent.risksFactory.visibleRisksList, function( i, item ) { 
			item.updateInterfaceVisibleTime(self.parent.lastPauseStart, timeSinceLastPause);
			
			log_string += "{";
			log_string +=  item.id + ", ";
			log_string +=  (item.chance).toFixed(2) + ", ";
			log_string +=  item.impact + ", ";
			log_string +=  (item.interfaceVisibleTime).toFixed(2) + ", "; //czas od pojawienia sie
			log_string +=  "[" + self.parent.parent.tasksFactory.getSolutionsOnTasksListIds() + "], ";
			log_string +=  self.parent.parent.tasksFactory.getEffectiveSolutionsForThisRiskCount(item.id, item.isGood); //czy ktores z nich zadziala
			log_string += "}";
		});
		log_string += "]";
	}
	this.addData(log_string);
}

LogMaker.prototype.logGameRating = function() {
	var ratinglog = uniqueId + ";";
	ratinglog += this.parent.gameRating + ";";
	log.debug(ratinglog);
}