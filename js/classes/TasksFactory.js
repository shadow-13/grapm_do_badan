function TasksFactory(parent) {
	this.tasksList = [];
	this.parent = parent;
	this.keepAllocation = false;
	this.counter = 0;
	this.allUniqueSolutionsEverIds = [];
	this.allEffectiveSolutionsEverCount = 0;
	this.allSolutionsEverCount = 0;
}
TasksFactory.prototype.initTasks = function () {
	var self = this;
	$( "#tasks" ).sortable({connectWith: "#products, #solutions", 
		stop: function (event, ui){
			if(ui.item.hasClass("youShallNotPass")){
				$(this).sortable('cancel');
				ui.item.removeClass("youShallNotPass");
			}},
			receive: function (event, ui) {
				copyHelper= null;
				if(ui.item.hasClass("solution")){
					ui.item.attr("id", $(ui.item).attr("id"));
				}
				ui.item.find(".close").css({"visibility": "visible"});
				var task = self.setIsOnTaskList($(ui.item),true);
				if(task != null)	{
					self.tasksList.push(task);
				}
			}
	});
	
	$("#tasks").on("click", "span.close", (function(event, isTaskDone) {
		removeTask( $(this).parent(), (isTaskDone ? isTaskDone : false) );
	}));
	
	function removeTask(thisObj, isTaskDone) {
		thisObj.fadeOut(function() {
			thisObj.detach();
			
			if (thisObj.hasClass("product") && isTaskDone==false) {
				thisObj.appendTo("#products");
				thisObj.fadeIn();
			}
			else if (thisObj.hasClass("solution")) {
				self.parent.solutionsFactory.deleteFromSolutionsOnTasksList(thisObj.attr("id"));
				thisObj.remove();
			}
			
			
		});
		self.freeTaskResources(thisObj, isTaskDone);
	}
}
/* uporządkowanie listy zadan */
TasksFactory.prototype.reorderTasks = function () {
	
	var orderedTasksList = $("#tasks").children();
	var newTasksList = [];
	var idsList = [];
	
	$(orderedTasksList).each(function(index, value) {
		idsList.push(value.id);
	});
	
	while(idsList.length > 0) {
		for(var i=0;i<this.tasksList.length;i++) {
			if(idsList[0] == this.tasksList[i].id) {
				/* przy okazji wyczyszczenie danego taska */
				$("#tasks #" + this.tasksList[i].id + " .assignedResource").html("");
				this.tasksList[i].resources = [];
				
				newTasksList.push(this.tasksList[i]);
				idsList.shift();
			}
		}
	}
	this.tasksList = newTasksList;	
}
TasksFactory.prototype.allocateResources = function () {
	//przydzielenie zasobów do zadań
	this.keepAllocation = true;
	var self = this;
	
	var intvl = setInterval(function() {
	    if (!self.keepAllocation) { 
	        clearInterval(intvl);
	    }
	    
	    var freeResourcesList = self.parent.resourcesFactory.getFreeResources();
	    if(freeResourcesList.length==0) return;
	    
	    for(var i=0;i<self.tasksList.length;i++) {
	    	if(self.tasksList[i].resources == 0) {
	    		var resource= freeResourcesList.shift();
	    		resource.task = self.tasksList[i];
	    		self.tasksList[i].resources.push(resource);
	    		resource.currentlyUsed = true;
	    		var shortcut = "(" + resource.name.substr(0,3) + ")";
	    		$("#tasks #" + self.tasksList[i].id + " .assignedResource").html(shortcut);
	    		if(freeResourcesList.length==0) break;
	    	}
	    }  
	    
	}, 100);
}
TasksFactory.prototype.freeTaskResources = function(thisObj, isTaskDone) {
	thisObj.find(".close").css({"visibility": "hidden"});
	thisObj.find(".assignedResource").html("");
	
	var i = this.tasksList.length;
	while(i--) {
		if(this.tasksList[i].id == thisObj.attr("id")) {
			task = this.tasksList.splice(i,1)[0];
			var freedResourcesList;
			if(isTaskDone) {
				freedResourcesList = task.done();
			} else {
				freedResourcesList = task.resources;
				task.resources = [];				
			}
			this.parent.resourcesFactory.freeResources(freedResourcesList);
			break;
		}
	}
}

TasksFactory.prototype.taskDone = function (task) {
	$("#"+ task.id + " .ui-icon-close").trigger("click", [true]);
}

TasksFactory.prototype.setIsOnTaskList = function (task, boolean) {	
	var container;
	if(task.hasClass("product")) {
		container = this.parent.productsFactory.productsList;
	}
	else {
		container = this.parent.solutionsFactory.solutionsList;
	}
	
	var taskObject;	
	var i = container.length;
	var id = task.attr("id");
	
	while(i--) {
		if(container[i].id == id) {
			taskObject = container[i];
			if(task.hasClass("solution")) {
				var newSolution = {};
				jQuery.extend(newSolution,taskObject);
				newSolution.id += "_" + this.counter;
				task.attr("id", id +  "_" + this.counter);
				this.counter += 1;
				this.parent.solutionsFactory.solutionsOnTasksList.push(newSolution);
				return newSolution;
			}
			else {
				taskObject.isOnTaskList = boolean;
				return taskObject;
			}
		}
	}
}

TasksFactory.prototype.allocateResourcesNOW = function () {
	//przydzielenie zasobów do zadań NATYCHMIAST, tylko na potrzeby logowania
	this.keepAllocation = true;
	var self = this;
	

	    var freeResourcesList = self.parent.resourcesFactory.getFreeResources();
	    if(freeResourcesList.length==0) return;
	    
	    for(var i=0;i<self.tasksList.length;i++) {
	    	if(self.tasksList[i].resources == 0) {
	    		var resource= freeResourcesList.shift();
	    		resource.task = self.tasksList[i];
	    		self.tasksList[i].resources.push(resource);
	    		resource.currentlyUsed = true;
	    		var shortcut = "(" + resource.name.substr(0,3) + ")";
	    		$("#tasks #" + self.tasksList[i].id + " .assignedResource").html(shortcut);
	    		if(freeResourcesList.length==0) break;
	    	}
	    }  
}

TasksFactory.prototype.getTasksWithAssignedResourcesCount = function() {
	var result = 0;
	$.each(this.tasksList, function( i, item ) {  
		if (item.resources.length > 0) {
			result += 1;
		}
	});
	return result;
}

TasksFactory.prototype.getAllTaskAverageProgressInPoints = function () {
	var result = 0;
	var len = this.tasksList.length;
	var i = this.tasksList.length;
	while(i--) {
		result += this.tasksList[i].progress;
	}
	if(len != 0) {
		result = result / len;
	}
	return result;
}

TasksFactory.prototype.getPlannedToDoProductCount = function() {
	var result = 0;
	$.each(this.tasksList, function( i, item ) {  
		if (item.id.indexOf("product") > -1 ) { //tzn. ten task to produkt
			result += 1;
		}
	});
	return result;
}

TasksFactory.prototype.getSolutionsOnTasksListSize = function () {
	var result = 0;
	$.each(this.tasksList, function( i, item ) {  
		if (item.id.indexOf("solution") > -1 ) { //tzn. ten task to solution
			result += item.size;
		}
	});
	return result;
}

TasksFactory.prototype.updateAllSolutionsEverData = function () {
	var self = this;
	$.each(this.tasksList, function( i, item ) {
		var itemId = item.id;
		if (itemId.indexOf("solution") > -1 ) { //tzn. ten task to solution
			self.allSolutionsEverCount += 1;
			if($.inArray(itemId, self.allUniqueSolutionsEverIds) === -1) { //nie mielismy dotad takiego
				self.allUniqueSolutionsEverIds.push(itemId);			
			}
			var desirablyAffectedRisksCount = 0;
			$.each(item.solutionEvents, function( i, solutionEvent ) {
				desirablyAffectedRisksCount += self.parent.risksFactory.getDesirablyAffectedRisksOfThisIdCount(solutionEvent.riskId, solutionEvent.addOrSub);
			});	
			if (desirablyAffectedRisksCount > 0) {
				self.allEffectiveSolutionsEverCount += 1;
			}
		}
	});
}

TasksFactory.prototype.getSolutionsOnTasksListIds = function () {
	var result = [];
	$.each(this.tasksList, function( i, item ) {  
		if (item.id.indexOf("solution") > -1 ) { //tzn. ten task to solution
			result.push(item.id);
		}
	});
	return result;
}

TasksFactory.prototype.getEffectiveSolutionsForThisRiskCount = function (riskId, isGood) {
	var result = 0;
	$.each(this.tasksList, function( i, item ) {
		if (item.id.indexOf("solution") > -1 ) { //tzn. ten task to solution
			$.each(item.solutionEvents, function( i, solutionEvent ) {
				var fullRiskId = "risk"+ solutionEvent.riskId;
				if ((fullRiskId == riskId) && ((isGood === true && solutionEvent.addOrSub === true) || ((isGood === false) && (solutionEvent.addOrSub === false)))) {
					result += 1;
					return false; //to dziala jak break
				}
			});
		}
	});
	return result;
}
/* ukrycie skrotow nazw zasobow na zadaniach */
TasksFactory.prototype.hideAssignedResourcesShorts = function () {
	$.each(this.tasksList, function( i, item ) {
		$("#tasks #" + item.id + " .assignedResource").html("");
	});
}