function Risk(json,parent) {

	this.id; //id
	this.name; //nazwa
	this.chance; //szansa ryzyka, ulega zmianie
	this.baseChance; //szansa ryzyka jak wczytana z bazy, niezmienna
	this.baseDelay; //mnożnik szansy na kolejne ryzyko (mniejsza szansa z kolejnym wystąpieniem)
	this.events = []; // wydarzenia, które powoduje to ryzyko
	this.ticketsLeft;
	this.tickets;
	this.visible = false; // czy widoczne w interfejsie
	this.succeed = false; //true jeśli ryzyko się wykonało z powodzeniem
	this.parent = parent;
	this.savedPercent = 0; //reszta z dzielenia przez 10 ostatniej zmiany ryzyka przez solution
	this.impact;
	this.bonusPoints = 1;
	this.isGood;
	this.mutex = false;
	this.currentDelay = 0;
	
	//do logowania
	this.interfaceAppearTime; //data pojawienina sie na interfejsie, do pomiaru czasu
	this.interfaceVisibleTime = 0; //czas widocznosci na interfejsie, pomijajac pauzy
	
	if (json == undefined) return;
	this.id = "risk" + json.ID;
	this.name = json.Name;
	this.baseChance = parseInt(json.Possibility);
	this.chance = parseInt(json.Possibility);
	this.events = createEvents(json.Code,this);
	this.impact = json.Impact;
	this.bonusPoints = this.impact;
	this.isGood = this.events[0].addOrSub; //zakladamy ze nie ma dobro-zlych ryzyk
	this.baseDelay = json.Next > 0 ? json.Next : 1;
	
	
}
/* Wykonanie ryzyka jeśli trafiło w przedział szansy */
Risk.prototype.rollEvent = function() {
	var roll = Math.floor((Math.random() * 100) + 1); //losowanie
	if(roll <= this.chance) { //wykonanie ryzyka
		this.succeed = true; //TODO potem chyba będzie trzeba zmienić położenie tego
		if(Config.debug) console.log("SUKCES RYZYKA! " + this.toString());
		this.events.map(function(event){
			if(Config.debug) console.log("Wykonywanie eventu ryzyka:");
			event.doEvent();
		});
		
		/*za każde zmaterializowane zagrożenie +20% do ogólnego poziomu ryzyka projektu. 
		 * (tu dodajemy 10%, bo zeby pasek ryzyka nie migal, przy usuwaniu wykonanego zagrozenia
		 * nie odejmujemy 10% od ryzyka, wiec w sumie jest o 20% wiecej niz przed jego pojawieniem sie)
		 * Za każdą zmaterializowaną losowo szansę -10%, 
		 * a za każdą szansę, którą gracz podniósł działaniami do 100% 
		 * jest -20% do ogólnego poziomu ryzyka projektu*/
		if(this.isGood) {
			if(this.chance == 100) {
				this.parent.parent.playFactory.changeRiskFigtingPercentageBy(-20);
			}
			else {
				this.parent.parent.playFactory.changeRiskFigtingPercentageBy(-10);
			}
		}
		else {
			this.parent.parent.playFactory.changeRiskFigtingPercentageBy(10);
		}
		
	} else {
		if(Config.debug) console.log("Niepowodzenie, wylosowano: " + roll + " spośród: " + this.chance);
	}
}

Risk.prototype.setVisible = function() {
	this.visible = true;
	var result = getRiskLi(this);
	$( "#risks").append(result);
		$('.textRiskInfluenceImg').prop('title',language['textRiskInfluenceImgTooltip'].text);
		$('.textRiskInfluenceImg').addClass('withTooltip');
		$(".withTooltip").tooltip({show: {delay: 300}});
}
Risk.prototype.removeVisible = function(shouldVisibleRiskChange) {
	if(shouldVisibleRiskChange == true) {
		this.parent.changeVisibleRisksBy(-1, this.isGood);
	}
	this.parent.visibleRisksList.splice(
			this.parent.visibleRisksList.indexOf(this),1); //usunięcie z listy widocznych ryzyk
	$( ("#risks #"+this.id)).remove();
	this.reset();
	this.mutex = false;
}
/* TODO resetowanie ryzyka (może być ponowne użycie ryzyka?) */
Risk.prototype.reset = function () {
	this.visible = false;
	this.succeed = false;
	this.ticketsLeft = undefined;
	this.chance = this.baseChance;
	this.savedPercent = 0;
}

Risk.prototype.toString = function() {
	return " " + this.id + ", chance: " + this.chance 
	+ ", ticketsLeft: " + this.ticketsLeft + ", visible: " 
	+ this.visible + ", succeed: " + this.succeed +
	', mutex: ' + this.mutex;
}

Risk.prototype.changeChance = function(value) {
	
	if(this.chance > 0 && this.chance < 100) {
		var val = parseFloat(value);
		
		var isIncreasing = val > 0 ? true : false;
		
		var oldChance = this.chance;
		this.chance += val;
		
		//wszystko to zeby byly punkty za kazde zmniejszone o 10 przez gracza ryzyko
		var x = this.savedPercent + Math.abs(val);
		this.savedPercent = x % 10;		
		
		var multiplier = Math.floor(x);
		
		if(this.chance >= 100) {
			this.chance = 100;
			multiplier = Math.abs(100 - oldChance);
		}
		else if(this.chance <= 0){
			this.chance = 0;
			multiplier = oldChance;
			this.parent.addThreatsRemovedCounter(this.isGood);
			this.removeVisible(true);
		}
		
		if(x / 10 >= 1 && (isIncreasing == this.isGood)) {
			multiplier = parseInt(multiplier / 10) * 10
			
			var points = multiplier * this.bonusPoints; 
			this.parent.parent.playFactory.score += points;
		}
		
		changeRiskLiChanceAndColor(this);
		
		if(this.chance == 100 || this.chance == 0) {
			this.ticketsLeft = 0;
		}
	}
}

Risk.prototype.getColor = function() {
	var brightness = 200 - 200*this.chance/100;
	brightness = Math.floor(brightness);
	var rgb;
	if(this.events.length >0) { //na razie na 'sztywno'
		rgb = this.events[0].addOrSub ? "rgb(" + brightness + "," + "255" + "," + brightness +");" :
			"rgb(" + "255" + "," + brightness + "," + brightness +");" ;
	}
	
	return rgb;
}

Risk.prototype.updateInterfaceVisibleTime = function(pauseTime, timeSinceLastPause) {
	if(this.interfaceVisibleTime == 0){
		this.interfaceVisibleTime = (pauseTime.getTime() - this.interfaceAppearTime.getTime()) / 1000;
	}
	else {
		this.interfaceVisibleTime += timeSinceLastPause;
	}
}

/* event odpowiedzialny za wykonanie danego ryzyka */
function RiskEvent(parent) {
	this.doEvent; //ta zmienna to funkcji, czyli wywołanie doEvent() np. 'produktywność','zasób', 'postęp'
	this.addOrSub; // true = dodaj, false = odejmij np. '+', 'znika zasób', 'wykonać od nowa'
	this.values = []; // wartość/wartości np. [50], [1, 2, 3],[1]
	this.parent = parent;
}
/*-------zbiór funkcji realizujących dane ryzyko--------*/
RiskEvent.prototype.productivity = function() {
	
	var value = this.values[0]/100;
	var addOrSub = this.addOrSub;
	$.each(this.parent.parent.parent.resourcesFactory.resourcesList,function(i,resource){
		var newProductivity = resource.productivity * value;
		newProductivity = resource.productivity + (addOrSub ? newProductivity : -newProductivity);
		resource.lastProductivity = resource.productivity;
		resource.productivity = newProductivity;
	});
	
	this.parent.parent.eventsProductivityMutex = ++this.parent.parent.eventsProductivityMutex % Config.riskEventsMaxMutexSize;
	var self = this;
	
	$.each(this.parent.parent.parent.resourcesFactory.resourcesList, function( i, resource ){
		var productivityChangesText = "";
			var change = Math.round(resource.productivity) - Math.round(resource.lastProductivity);
			if(change != 0) {
				productivityChangesText = getResourceProducitvityEffect(change);
			}
			var currentMutex = self.parent.parent.eventsProductivityMutex;
			$('#'+resource.id + ' #resourceProductivity').html(Math.round(resource.productivity) + productivityChangesText);
			setTimeout(function() {
				if(self.parent.parent.eventsProductivityMutex == currentMutex) {
					$('#'+resource.id + ' #resourceProductivity').html(Math.round(resource.productivity));
				}
			},Config.riskShowTime);
	});
	
	this.blinkResourceTitle();
}


RiskEvent.prototype.resource = function() {
	
	var resourcesFactory= this.parent.parent.parent.resourcesFactory;
	var resourcesList = resourcesFactory.resourcesList;
	var initialResourcesList = resourcesFactory.initialResourcesList;
	var resourceId = null;
	var resourceListId = null;
	var self = this;
	if(this.addOrSub) { //dodanie zasobów
		for(var i=0;i<initialResourcesList.length;i++) {
			if(initialResourcesList[i].sid == parseInt(this.values[0])) {
				var resourceFactory = this.parent.parent.parent.resourcesFactory;
				resourcesFactory.addResourceToList(initialResourcesList[i],"resource" + (++resourceFactory.lastResourceIdIncrement));
				var justAddedresourceId = "#resource"+resourceFactory.lastResourceIdIncrement;
				$(justAddedresourceId).addClass('goodEffect');
				/*usuniecie później efektu*/
				setTimeout(function() {$(justAddedresourceId).removeClass('goodEffect')},
						Config.riskShowTime);
				break;
			}
		}
	}else {	//usunięcie zasobów
		for(var i=0;i<resourcesList.length;i++) {
			if(resourcesList[i].sid == this.values[0])  {
				var resource = resourcesList[i];
				resourceListId = i;
				resourceId = "#"+resourcesList[i].id;
				if($(resourceId).hasClass('badEffect')) continue; // juz jest w trakcie usuwania
				$(resourceId).addClass('badEffect');
				setTimeout(function() {
					var resourceListId = resourcesList.indexOf(resource);
					if(resourceListId != -1) {
						resourcesFactory.removeResourceFromList(resourceListId);
					}
				},Config.riskShowTime);
				break;
			}
		}
	}
	/*jeśli zasob do usunięcia nie znajduje się na aktualnej liście to kończy wykonywanie*/
	if(!this.addOrSub && resourceListId == null) {
		if(Config.debug) console.log("Nie ma na liście; value = " + this.values[0]);
		return;
	}
	
	this.blinkResourceTitle();
	
}
RiskEvent.prototype.blinkResourceTitle = function() {
	var self = this;
	/* czyszczenie migania napisu */
	if(this.parent.parent.eventsResourcesMutex != null) {
		clearTimeout(this.parent.parent.eventsResourcesMutex.timeout);
		clearInterval(this.parent.parent.eventsResourcesMutex.interval);
		this.parent.parent.eventsResourcesMutex = null;
		changeResourceTitleColor(false,'red','resourcesTitle');
		changeResourceTitleColor(false,'green','resourcesTitle');
	}
	
	var switchTo = true;
	var mutex = { 
		interval : setInterval(function() {
			changeResourceTitleColor(switchTo,self.addOrSub ? 'green' : 'red','resourcesTitle');
			switchTo = !switchTo;
		}, 500),
		timeout : setTimeout(function() {
			clearInterval(self.parent.parent.eventsResourcesMutex.interval);
			self.parent.parent.eventsResourcesMutex = null;
			changeResourceTitleColor(false,self.addOrSub ? 'green' : 'red','resourcesTitle');
		},Config.riskShowTime)
	};
	this.parent.parent.eventsResourcesMutex = mutex;
}
RiskEvent.prototype.progress = function() {
	
	var productsFactory = this.parent.parent.parent.productsFactory;
	var productsList = productsFactory.productsList;
	
	for(var i=0;i<productsList.length;i++) {
		if(("product"+this.values[0]) == productsList[i].id) {
			var product =  productsList[i];
			if(!product.visible) {
				product.visible = true;
				product.progress = 0;
				$('#products').append(getProductLi(product));
			} else {
				var progress = product.progress/100 * product.size - this.values[1];
				product.progress = progress > 0 ? progress : 0;
				$("#"+product.id).addClass('badEffect');
				setTimeout(function() {$("#"+product.id).removeClass('badEffect')},
						Config.riskShowTime);
			}
			$('#'+product.id).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500)
				.fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500)
				.fadeOut(500).fadeIn(500);
			var self = this;			
			if(this.parent.parent.eventsProgressMutex != null) {
				clearTimeout(this.parent.parent.eventsProgressMutex.timeout);
				clearInterval(this.parent.parent.eventsProgressMutex.interval);
				this.parent.parent.eventsProgressMutex = null;
				changeResourceTitleColor(false,'red','productsTitle');
			}
			
			var switchTo = true;
			var mutex = { 
				interval : setInterval(function() {
					changeResourceTitleColor(switchTo,'red','productsTitle');
					switchTo = !switchTo;
				}, 500),
				timeout : setTimeout(function() {
					clearInterval(self.parent.parent.eventsProgressMutex.interval);
					self.parent.parent.eventsProgressMutex = null;
					changeResourceTitleColor(false,'red','productsTitle');
				},Config.riskShowTime)
			};
			this.parent.parent.eventsProgressMutex = mutex;
			
		}
	}	
}

/* Tworzy eventy dla ryzyka */
function createEvents (code,parent) {
	var events = code.split(";"); //podział na pojedynczy event 
	var riskEvents = [];
	events.map(function(event){
		var riskEvent = new RiskEvent(parent);
		var e = event.split("."); //podział na ["funkcja","dodaj lub odejmij","wartości"]
		riskEvent.doEvent = extractFunctionForEvent(e[0],riskEvent);
		riskEvent.addOrSub = extractBooleanForEvent(e[1]);
		riskEvent.values = extractValuesForEvent(e[2]);
		isEventOk(riskEvent) ? riskEvents.push(riskEvent) : null;
	});
	return riskEvents;
}
function extractFunctionForEvent(value,riskEvent) {
	switch(value.toLowerCase()){
		case "productivity": return riskEvent.productivity;
		case "resource": return riskEvent.resource;
		case "progress": return riskEvent.progress;
		default: return null;
	}
}

function extractBooleanForEvent(value) {
	switch(value.toLowerCase()) {
	case "add":	return true;
	case "sub" : return false;
	default: return null;
	}
}

function extractValuesForEvent(values) {
	return values != null ? values.replace("[","").replace("]","").split(",") : null;
}

function isEventOk(riskEvent) {
	if(riskEvent.doEvent != null 
			&& riskEvent.addOrSub != null
			&& riskEvent.values.length > 0) 
		return true;
	return false;
}
function changeResourceTitleColor(switchTo, cssClass, id) {
	var resourcesTitle = $('#'+id);
	var color = cssClass;
	if(switchTo) resourcesTitle.addClass(color);
	else resourcesTitle.removeClass(color);
}
