function Product(json, parent) {

	this.id;
	this.name;
	this.value;
	this.quality;
	this.size;
	this.visible = true;
	this.resources = [];
	this.progress = 0;
	this.isOnTaskList = false;
	this.parent = parent;
	
	if(Config.debug) this.tickets = 0;
	
	if (json == undefined) return;
	this.id = "product" + json.ID;
	this.name = json.Name;
	this.value = parseInt(json.Value);
	this.quality = parseInt(json.Quality);
	this.size = parseInt(json.Size);
	
}

Product.prototype.toString = function () {
	return "product"+this.id;
}

Product.prototype.done = function () {
	
	var freedResources = this.resources;
	
	this.visible = false;
	this.resources = [];
	this.isOnTaskList = false;
	this.progress = 100;
	
	this.parent.parent.playFactory.quality += this.quality;
	if(this.parent.parent.playFactory.quality > 11) this.parent.parent.playFactory.quality = 11;
	if(this.parent.parent.playFactory.quality < 1) this.parent.parent.playFactory.quality = 1;
	
	return freedResources;
}

Product.prototype.addProgress = function(productivity) {
	if(this.progress < 100) {
		if(Config.debug) this.tickets++;
		this.progress += productivity / Config.pointPerTickets / this.size * 100;
		var task = $('#'+this.id);
		if(!task.hasClass('badEffect') && !task.hasClass('goodEffect')) {
			task.css('background',getCssTaskProgress(this.progress));
		}
		
		$('#'+this.id+' .right .taskSize').html(Math.round(this.size-this.size*this.progress/100));
		if(this.progress >= 100) {
			if(Config.debug) console.log("Wykonano zadanie " +this.id 
					+", size: "+this.size+" w " +this.tickets +" ticketów.");
			this.parent.parent.tasksFactory.taskDone(this);		
		}
	}
}