function Resource(json) {
	this.id; //id dla htmla
	this.sid; //id z bazy
	this.name;
	this.productivity;
	this.specialisation;
	this.visible = true;
	this.currentlyUsed = false;
	this.task; //referencja na produkt lub solution w którym się znajduje
	this.lastProductivity;
	this.initialProductivity;
	
	if (json == undefined) return;
	this.id = "resource" + json.ID;
	this.sid = json.ID;
	this.name = json.Name;
	this.productivity = parseInt(json.Productivity);
	this.lastProductivity = parseInt(json.Productivity);
	this.initialProductivity = parseInt(json.Productivity);
	
}
/*zamiast kopiować produktynwość nadaje inicjalną*/
Resource.prototype.copy = function(resource) {
	this.id = resource.id;
	this.sid = resource.sid; 
	this.name = resource.name;
	this.productivity = resource.initialProductivity;
	
	this.specialisation = resource.specialisation;
	this.visible = false;
	this.currentlyUsed = false;
	this.lastProductivity = resource.lastProductivity;
}