function Solution(json, parent) {
	this.id;
	this.name;
	this.size;
	this.solutionEvents;
	this.parent = parent;
	this.progress = 0;
	
	if (json == undefined) return;
	this.id = "solution" + json.ID;
	this.name = json.Name;
	this.size = parseInt(json.Size);
	this.solutionEvents = createSolutionEvents(json.Code,this);
}

Solution.prototype.done = function () {
	
	var freedResources = this.resources;
	
	this.resources = [];
	this.progress = 0;
	
	return freedResources;
}

Solution.prototype.addProgress = function(productivity) {
	if(this.progress < 100) {
		if(Config.debug) this.tickets++;
		
		var progress = productivity / Config.pointPerTickets / this.size * 100;
		this.progress += progress;
		var task = $('#'+this.id);
		task.css('background',getCssTaskProgress(this.progress));
		
		
		$('#'+this.id+' .right .taskSize').html(Math.round(this.size-this.size*this.progress/100));
		this.doAllSolutionEvents(progress);
		if(this.progress >= 100) {
			if(Config.debug) console.log("Wykonano solution " +this.id 
					+", size: "+this.size+" w " +this.tickets +" ticketów.");
			this.parent.parent.tasksFactory.taskDone(this);		
		}
	}
}

Solution.prototype.doAllSolutionEvents = function (progress) {
	this.solutionEvents.map(function(solutionEvent){
		if(Config.debug) console.log("Wykonywanie eventu solution:");
		solutionEvent.doSolutionEvent(progress);
	});
}



/* event odpowiedzialny za wykonanie danego wplywu */
function SolutionEvent(parent) {
	this.doSolutionEvent; //ta zmienna to funkcji, czyli wywołanie doSolutionEvent() np. 'produktywność','zasób', 'postęp'
	this.addOrSub; // true = dodaj, false = odejmij np. '+', 'znika zasób', 'wykonać od nowa'
	//this.values = []; // wartość/wartości np. [50], [1, 2, 3],[1]
	this.riskId;
	this.totalValue;
	this.parent = parent;
}
/*-------zbiór funkcji realizujących dany wplyw--------*/

SolutionEvent.prototype.possibility = function(value) {
	if(Config.debug) console.log("possibility: " + (this.addOrSub ? "add" : "sub") + ", riskId:" + this.riskId + ",value " + value);
	
	var risksList = this.parent.parent.parent.risksFactory.visibleRisksList;
	
	for(var i=0;i<risksList.length;i++)
		if(("risk"+ this.riskId) == risksList[i].id){
			var risk = risksList[i];
			
			if(this.addOrSub == false) {
				value = -value;
			}
			value *= this.totalValue / 100;
			
			risk.changeChance(value);
			break;
		}	
}

/* Tworzy eventy dla wplywu */
function createSolutionEvents (code,parent) {
	var events = code.split(/[\s;]+/); //podział na pojedynczy event 
	var solutionEvents = [];
	events.map(function(event){
		var solutionEvent = new SolutionEvent(parent);
		var e = event.split("."); //podział na ["funkcja","dodaj lub odejmij","wartości"]
		solutionEvent.doSolutionEvent = extractFunctionForSolutionEvent(e[0],solutionEvent);
		solutionEvent.addOrSub = extractBooleanForSolutionEvent(e[1]);
		var values = extractValuesForSolutionEvent(e[2]);
		
		solutionEvent.riskId = values[0];
		solutionEvent.totalValue = parseInt(values[1]);
		
		isSolutionEventOk(solutionEvent) ? solutionEvents.push(solutionEvent) : null;
	});
	return solutionEvents;
}

function extractFunctionForSolutionEvent(value,solutionEvent) {
	switch(value.toLowerCase()){
		case "possibility": return solutionEvent.possibility;
		default: return null;
	}
}

function extractBooleanForSolutionEvent(value) {
	switch(value.toLowerCase()) {
	case "add":	return true;
	case "sub" : return false;
	default: return null;
	}
}

function extractValuesForSolutionEvent(values) {
	if(values != null) {
		var desired = values.replace(/\[(.*?)\]/g,"$1");
		return desired.split(",");
	}
	return null;
}

function isSolutionEventOk(solutionEvent) {
	if(solutionEvent.doSolutionEvent != null 
			&& solutionEvent.addOrSub != null
			&& solutionEvent.riskId != null
			&& solutionEvent.totalValue != null) 
		return true;
	return false;
}

