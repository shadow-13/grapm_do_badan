function ResourcesFactory(parent) {
	this.parent = parent;
	this.resourcesList = [];
	this.initialResourcesList = [];
	this.lastResourceIdIncrement = 0;
	$( "#resources" ).sortable();
}



ResourcesFactory.prototype.loadResources = function (resources) {
	var self = this;
	$.each(resources, function( i, item ) {
		var resource = new Resource(item);	
		self.resourcesList.push(resource);
		self.initialResourcesList.push(resource);
		self.lastResourceIdIncrement++;
	});
	
	self.parent.playFactory.startingProductivity = this.getAllProductivity();
}

ResourcesFactory.prototype.showResources = function(showProductivityChanges) {
	var result = "";
	
	$.each(this.resourcesList, function( i, item ) {  
		result += getResourceLi(item);		
	});
	$( "#resources" ).html(result);
}

ResourcesFactory.prototype.freeResources = function(resources) {
	if(resources) {
		$.each(resources, function( i, item ) {	
			item.currentlyUsed = false;
			item.task = null;
		});
	}
}


ResourcesFactory.prototype.getFreeResources = function() {
	var freeResources = [];
	for(var i=0;i<this.resourcesList.length;i++) {
		if (this.resourcesList[i].currentlyUsed == false) {
			freeResources.push(this.resourcesList[i]);
		}
	}
	return freeResources;
}
ResourcesFactory.prototype.getAllProductivity = function() {
	var result = 0;
	for(var i=0;i<this.resourcesList.length;i++) {
		result += this.resourcesList[i].productivity;
	}
	return result;
}
ResourcesFactory.prototype.freeAllResources = function() {
	this.resourcesList.map(function(resource){
		resource.currentlyUsed = false;
		resource.task = null;
	});
}

ResourcesFactory.prototype.addResourceToList = function (resource,newId) {
	var newResource = new Resource();
	newResource.copy(resource);
	newResource.id = newId;
	this.resourcesList.push(newResource);
	var result = getResourceLi(newResource);
	$('#resources').append(result);
}

ResourcesFactory.prototype.removeResourceFromList = function (i) {
	var removedResource = this.resourcesList.splice(i,1)[0]; //usunięcie z listy
	$('#'+removedResource.id).remove();
	var task = removedResource.task;
	if(task == null) return; // to znaczy ze ten resource nie jest uzywany przez produkty
	for(var i=0;i<task.resources.length;i++) {
		if(task.resources[i].sid == removedResource.sid) { //usunięcie z listy produktu
			task.resources.splice(i,1);
			$("#tasks #" + task.id + " .assignedResource").html("");
			break;
		}
	}
}

/* uporządkowanie listy zasobów */
ResourcesFactory.prototype.reorderResources = function () {
	
	var orderedResourcesList = $("#resources").children();
	var newResourcesList = [];
	var idsList = [];
	
	$(orderedResourcesList).each(function(index, value) {
		idsList.push(value.id);
	});
	
	while(idsList.length > 0) {
		for(var i=0;i<this.resourcesList.length;i++) {
			if(idsList[0] == this.resourcesList[i].id) {
				newResourcesList.push(this.resourcesList[i]);
				idsList.shift();
			}
		}
	}
	this.resourcesList = newResourcesList;
	
}