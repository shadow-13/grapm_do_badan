function ProductsFactory(parent) {
	this.productsList = [];
	this.parent = parent;
	this.allSize = 0;
}

ProductsFactory.prototype.initProducts = function() {
	var self = this;
	
	$( "#products" ).sortable({connectWith: "#tasks", 
		over: function (event, ui) {
			if(ui.item.hasClass("solution")){
				ui.item.addClass("youShallNotPass");
			}
			else if(ui.item.hasClass("product")){
				ui.item.removeClass("youShallNotPass");
			}
		},
		receive: function (event, ui) {
			if(ui.item.hasClass("product")){
				self.parent.tasksFactory.freeTaskResources(ui.item, false);
			}
		}
	});
}

ProductsFactory.prototype.loadProducts = function (products) {
	var self = this;
	
	$.each(products, function( i, item ) {     
		var product = new Product(item, self);	
		self.productsList.push(product);
		self.allSize += product.size;
	});
	
}

ProductsFactory.prototype.showProducts = function() {
	var result = "";
	$.each(this.productsList, function( i, item ) {  
		if (item.visible && (item.isOnTaskList == false)) {
			result += getProductLi(item);
		}
	});
	$( "#products" ).html(result);
}

ProductsFactory.prototype.getAllProgress = function () {
	var result = 0;
	var i = this.productsList.length;
	while(i--) {
		result += this.productsList[i].progress;
	}
	if(this.productsList.length>0) {
		result /= this.productsList.length*100;
		result *= 100;
	}
	return result;
}

ProductsFactory.prototype.getAllProgressInPoints = function () {
	var result = 0;
	var i = this.productsList.length;
	while(i--) {
		result += this.productsList[i].progress;
	}
	return result;
}

ProductsFactory.prototype.getAllProductsSize = function () {
	var result = 0;
	var i = this.productsList.length;
	while(i--) {
		result += this.productsList[i].size;
	}
	return result;
}

ProductsFactory.prototype.getNotDoneProductsCount = function() {
	var result = 0;
	$.each(this.productsList, function( i, item ) {  
		if (item.progress < 100 ) {
			result += 1;
		}
	});
	return result;
}

ProductsFactory.prototype.getProductsWithAssignedResourcesCount = function() {
	var result = 0;
	$.each(this.productsList, function( i, item ) {  
		if (item.resources.length > 0 && (item.isOnTaskList == true)) {
			result += 1;
		}
	});
	return result;
}