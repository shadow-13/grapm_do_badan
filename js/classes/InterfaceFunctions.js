/* zmiennej globalne */
globalLanguage = new Language();
mainContainer = null;
defaultLanguage = "pl";
log = log4javascript.getLogger();
ajaxAppender = new log4javascript.AjaxAppender("php/writetolog.php");
log.addAppender(ajaxAppender);

function getCssTaskProgress(progress) {
	var css = 'linear-gradient(to left, #636363 :progress%, blue 0%)';
	return css.replace(':progress',progress);
}

function getProductLi(product) {
	var result = '<li id='+product.id+' class="draggable interactable product">' + product.name + 
	'<span class="assignedResource"></span><span class="close ui-icon ui-icon-close"></span>'
	+ '<span class="right">'+ product.value + ' <img src="img/cubeValue.png" class="smallImg textProductValueImg" />   ' 
	+ product.quality + ' <img src="img/starQuality.png" class="smallImg textProductQualityImg" /><span class="taskSize"> ' 
	+ Math.round(product.size-product.size*product.progress/100) 
	+ ' </span><img src="img/hammer-redSize.png" class="smallImg textProductSizeImg" /></span></li>'; 			
	return result;
}

function getSolutionLi(solution) {
	var result = '<li id='+solution.id+' class="draggable interactable solution">' + solution.name + 
	'<span class="assignedResource"></span><span class="close ui-icon ui-icon-close"></span>' 
	+ '<span class="right textProductSizeImg"><span class="taskSize">'
	+ Math.round(solution.size-solution.size*solution.progress/100) 
	+' </span><img src="img/1.png" class="smallImg" /></li>';
	return result;
}
function getRiskLi(risk) {
	var impacts = '';
	for(var i =0; i<risk.impact;i++) {
		impacts += '!';
	}
	var result = '<li id='+risk.id+' class="ui-state-default risk"' +
	'style="background:'+risk.getColor()+' !important;"><span>' +
	risk.name + '</span><span class="right textRiskInfluenceImg"><span class="riskChance">'+ 
	Math.floor(risk.chance) +'%</span> <span class="riskImpact textBorder">'+
	impacts+'</span></span>' +'</li>';
	return result;
}

function getResourceLi(recource) {
	var result = '<li id='+recource.id+' class="resource">' +  recource.name + '<span class="right textResourceProductivityImg"><span id="resourceProductivity">' 
	+ Math.round(recource.productivity) + '</span> <img src="img/hammer-blueProductivity.png" class="smallImg" /></span></li>'; 			
	return result;
}

function getResourceProducitvityEffect(change) {
	var result = '<span class="' + (change > 0 ? 'green' : 'red') + '">';
	result += ' (' + (change > 0 ? '↑' : '↓') + Math.abs(change) + ')';
	result += '</span>';
	return result;
}

function changeRiskLiChanceAndColor(risk) {
	$("#risks #"+risk.id).css('background-color',risk.getColor());
	$("#risks #"+risk.id+' .right .riskChance').text(Math.floor(risk.chance)+"%");
}

function setTooltips(language) {
	for(var id in language) {
		if(!language.hasOwnProperty(id) && !id.contains('Tooltip')) continue;
		var idx = id.replace('Tooltip','');
		$('#'+idx).prop('title',language[id].text);
		$('#'+idx).addClass('withTooltip');
	}
	$(".withTooltip").tooltip({show: {delay: 300}});
}

function setUnitSpan(unitTextId) {
	var span = '<span id="'+unitTextId+'"></span>: <span id="valueInterfaceTimeUnit">0</span>';
	$('#progress5Text').append(span);
	$('#'+unitTextId).text(globalLanguage.properties[globalLanguage.choosenLanguage][unitTextId].text);
}
function changeLanguage(language) {
	$($('.selectedLanguage')[0]).removeClass('selectedLanguage');
	globalLanguage.switchLanguage(language);
	$("#"+language).addClass('selectedLanguage');
	var combo = document.getElementById("welcomeLowerComboBox");
	var length = combo.options.length;
	for (i = 0; i < length; i++) {
		combo.options[0] = null;
	}
	var videoSource = globalLanguage.properties[language].textWelcomeTutorial.text;
    $('#tutorialVideoPlayer').attr("src",videoSource);
	
	var schemes = globalLanguage.properties[language].schemes;
	if(schemes!=undefined) {
		for(var i=0;i<schemes.length;i++) {
			var option = document.createElement("option");
			option.text = schemes[i].expanded;
			option.value = schemes[i].id;
			try {
				combo.add((option), null); //Standard 
			}catch(error) {
				combo.add(option); // IE only
			}
		}
	}
	setTooltips(globalLanguage.getTooltipsText());
}
function showStartPopup(text) {
	$('html').addClass('overlay');
	$('#textStartPopupUpper').html(text);
	$('#startPopup').fadeIn();
}
function showTutorialPopup(tutorialHtml) {
	$('html').addClass('overlay');
	$('#tutorialPopup').fadeIn();
}
function setImgTooltips() {
	language = globalLanguage.getTooltipsText();
	for(var id in this.language) {
		if(!this.language.hasOwnProperty(id) && !id.contains('Tooltip')) continue;
		if(id.indexOf('Img') != -1) {
		var classx = id.replace('Tooltip','');
		$('.'+classx).prop('title',language[id].text);
		$('.'+classx).addClass('withTooltip');
		}
	}
	$(".withTooltip").tooltip({show: {delay: 300}});
}

/* Ładowane na początku */
$(function() {
	$( "ul, li" ).disableSelection();
	$( "#progressbar" ).progressbar().css({"visibility": "hidden"});
	$( "#icons li" ).hover(
			function() {
				$( this ).addClass( "ui-state-hover" );
			},
			function() {
				$( this ).removeClass( "ui-state-hover" );
			}
	);
	$( ".product, .task, .solution" ).hover(
			function() {
				$( this ).addClass("highlighted");
			},
			function() {
				$( this ).removeClass("highlighted");
			}
	);
	$( "#dialog" ).dialog({
		autoOpen: false,
		width: 400,
		close: function() { $('html').removeClass('overlay'); },
		buttons: [
			{
				text: "Ok",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	
	/* Inicjalizacja gry (START) */
	var request = new XMLHttpRequest();
	request.open("GET", "php/scheme.php?id=info", true);
	request.send(null);
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			json = request.responseText;
			json = JSON.parse(json);
			for(var lang in json) {
				for(var field in json[lang]) {
					globalLanguage.properties[lang][field] = json[lang][field];
				}
			}
			changeLanguage(defaultLanguage);
			$('#preloader').fadeOut();
			$('#allPanelWelcome').fadeIn();
		}
	}
	/* Inicjalizacja gry (END) */
	
	$('#startPopupLowerButton').click(function() {
		$('#startPopup').fadeOut();
		$('html').removeClass('overlay');
	});
	
	$('#panelWelcomeLowerStartGameButton').click(function() {
		
		var schemeId = document.getElementById("welcomeLowerComboBox").value;
		var tryb = $("input[name=panelWelcomeLowerRadioButtons]:checked").val();
		if(schemeId == undefined || schemeId == "") return;
		mainContainer = new MainContainer();
		$('#allPanelWelcome').fadeOut("slow",function() {
			$('#preloader').fadeIn();
			mainContainer.init(schemeId,tryb,function() {
				$('#preloader').fadeOut();
				$('#allPanelGame').fadeIn("slow");
			});
			
		});
	});
	$('#tutorialPopupLowerButton').click(function() {
		$('#tutorialPopup').fadeOut();
		var videoSource=$('#tutorialVideoPlayer').attr("src");
	    $('#tutorialVideoPlayer').attr("src",videoSource); //zatrzyma wideo, rozwiazanie niezalezne od zmian API
		$('html').removeClass('overlay');
	});
	$('#howToPlayButton').click(function() {
		$('html').addClass('overlay');
		$('#tutorialPopup').fadeIn("slow");
	});
	var languages = $('#languagesAvailable').children();
	for(var i=0; i<languages.length;i++){
		$('#'+languages[i].id).click(function() {
			changeLanguage(this.id);
		});
	}
});

