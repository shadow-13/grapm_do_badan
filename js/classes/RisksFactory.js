function RisksFactory(parent) {
	this.risksList = [];
	this.visibleRisksList = []; //lista ryzyk, które są wyświetlone w interfejsie
	this.riskChance = 0;
	this.riskTime = 1;
	this.riskDisappearTime = 1;
	this.parent = parent;
	this.eventsProductivityMutex = 0;
	this.eventsResourcesMutex;
	this.eventsProgressMutex;
	this.allRisksEverCount = 0;
	this.allThreatsEverCount = 0;
	this.allThreatsRemovedCount = 0;
	this.allOpportunitiesMissedCount = 0;
	this.allThreatsRandomlyDisappearedCount = 0;	
}

RisksFactory.prototype.loadRisks = function (risks) {
	var self = this;
	$.each(risks, function( i, item ) {     
		var risk = new Risk(item,self);	
		self.risksList.push(risk);
	});
}

RisksFactory.prototype.checkAndIncreaceRiskChance = function() {
	
	if(this.riskDisappearTime%(Config.riskDisappearTimePerTickets+1) == 0){
		this.riskDisappearTime = 1; //reset czasu
		var roll = Math.floor((Math.random() * 100) + 1); //losowanie
		if(roll <= Config.riskDisappearChance) {
			roll = Math.floor((Math.random() 
					* (this.visibleRisksList.length-1))); //losowanie ryzyka, które ma być usunięte
			
			var risk = this.visibleRisksList[roll];			
			var idx = this.visibleRisksList.indexOf(risk);
			if(idx < 0) return;
			this.addRiskRandomlyDisappearedCounter(risk.isGood);
			risk.removeVisible(true);
			this.refreshAllVisible();
			if(Config.debug) console.log("Usunięto ryzyko z interfejsu:" + risk.toString());
		}
	} else {
		this.riskDisappearTime++;
	}
	if(this.riskTime%(Config.riskTimePerTickets+1) == 0) {
		this.riskTime = 1; //reset czasu
		this.riskChance = Config.riskChance;
		var roll = Math.floor((Math.random() * 100) + 1); //losowanie
		if(roll <= this.riskChance) { //wyświetlenie szansy na zaistnienie ryzka
			roll = Math.floor((Math.random() 
					* (this.risksList.length-1))); //losowanie ryzyka, które ma być dodane
			
			var risk = this.risksList[roll];			
			if(this.visibleRisksList.indexOf(risk) != -1) {
				if(Config.debug) console.log("To ryzyko jest już na liście: " + risk.toString());
				return; //to ryzyko jest już na liście
			}
			if(risk.currentDelay > 0) {
				risk.currentDelay--;
				return;
			}
			risk.currentDelay = risk.baseDelay-1;
			risk.baseDelay *= risk.baseDelay;
			
			risk.ticketsLeft = Math.floor((Math.random() * 
					Config.riskTicketsLeftInterval[1]) + 
					Config.riskTicketsLeftInterval[0]); //losowanie czasu (ticketów) do zainstnienia ryzyka
			risk.tickets = risk.ticketsLeft;
			
			if(Config.debug) console.log("Dodano ryzyko  do interfejsu:" + risk.toString());
			this.visibleRisksList.push(risk);
			risk.interfaceAppearTime = new Date();
			this.changeVisibleRisksBy(1, risk.isGood);
			this.allRisksEverCount += 1;
			if(!risk.isGood) {
				this.allThreatsEverCount += 1;
				}
			this.refreshAllVisible();
			this.parent.playFactory.makeAutopause();
		}
		
	} else {
		this.riskTime++;
	}
}
RisksFactory.prototype.refreshAllVisible = function () {
	$("#risks").html('');
	this.visibleRisksList.sort(function(a,b) { return parseInt(b.chance) - parseInt(a.chance) } );
	$.each(this.visibleRisksList,function(i,item){
		item.setVisible();
	});
}

RisksFactory.prototype.changeVisibleRisksBy = function(quantity, isRiskGood) {
	if(isRiskGood == false) {		
		this.parent.playFactory.changeRiskFigtingPercentageBy(quantity * 10);
	}
}

RisksFactory.prototype.countVisibleThreats = function() {
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {     
		if(risk.isGood == false) {		
			result += 1;
		}
	});
	return result;
}

RisksFactory.prototype.addThreatsRemovedCounter = function(isRiskGood) {
	if(!isRiskGood){
		this.allThreatsRemovedCount += 1;
	}
}

RisksFactory.prototype.addRiskRandomlyDisappearedCounter = function(isRiskGood) {
	if(!isRiskGood){
		this.allThreatsRandomlyDisappearedCount += 1;
	}
	else {
		this.allOpportunitiesMissedCount += 1;
	}
}

RisksFactory.prototype.getVisibleThreatsProbabilitySum = function() {
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.isGood == false) {
			result += risk.chance;
		}
	});
	return result;
}

RisksFactory.prototype.getVisibleOpportunitiesProbabilitySum = function() {
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.isGood == true) {
			result += risk.chance;
		}
	});
	return result;
}

RisksFactory.prototype.getVisibleThreatsImpactSum = function() { //wykrzykniki
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.isGood == false) {
			result += Number(risk.impact);
		}
	});
	return result;
}

RisksFactory.prototype.getVisibleOpportunitiesImpactSum = function() { //wykrzykniki
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.isGood == true) {
			result += Number(risk.impact);
		}
	});
	return result;
}

RisksFactory.prototype.getDesirablyAffectedRisksOfThisIdCount = function(riskId, addOrSub) {
	var fullRiskId = "risk"+ riskId;
	var result = 0;	
	$.each(this.visibleRisksList, function( i, risk ) {  
		if(risk.id == fullRiskId) {
			if (risk.isGood && addOrSub === true) {
				result += 1;
			}
			else if ((risk.isGood === false) && (addOrSub === false)) {
				result += 1;
			}		
		}
	});
	return result;
}