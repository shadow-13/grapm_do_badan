<?php
header('Content-type: text/plain; charset=utf-8');
/*$dbhost = "sql202.byethost16.com";
 $dbuser = "b16_15507762";
 $dbpass = "Miler2014";
 $dbname = "b16_15507762_gramp"; */
$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "grapm";

$timestamp = date('Y-m-d H:i:s');

$message_recieved = htmlspecialchars($_POST["message"]);
$message_parts = explode(";", $message_recieved, 2);
$unique_gameplay_id = $message_parts[0];
$logged_values = $message_parts[1];

$link = mysqli_connect($dbhost, $dbuser, $dbpass);
mysqli_select_db($link, $dbname) or die(mysqli_error($link));
mysqli_query($link, "SET NAMES utf8") or die(mysqli_error($link));

if (strcmp(htmlspecialchars($_POST["level"]), "DEBUG") == 0) {
	$log_version = 1;
	$query = "INSERT INTO game_rating (log_version, timestamp, gameplay_id, rating)
		VALUES ('". $log_version. "','". $timestamp. "','".  $unique_gameplay_id. "','".  $logged_values ."')";
	echo $query;
	$qry_result = mysqli_query($link, $query) or die(mysqli_error($link));
}
else {
		
	$log_parts = explode("extra risk info start;", $logged_values, 2);
	$normal_log = $log_parts[0];
	$extra_risk_info_log = $log_parts[1];
	
	//za kazdym razem gdy chcemy zmienic logowane dane, nalezy:
	// nazwac odpowiednio nowo logowane pole w logged_fieldnames 
	//i podniesc o jeden wartosc logged_fieldnames_version 
	//(w celu ulatwienia pozniejszego wybierania odpowiednich danych)
	$log_version = 1;
	
	$logged_fieldnames = "gametime_percent; gametime_percent_left; completion_percent; completion_percent_left; quality; ".
			"customer_satisfaction_percent; team_happiness_percent; start_productivity; current_productivity; ". 
			"start_resources_count; current_resources_count; average_productivity; free_resources_count; ". 
			"busy_resources_count; start_product_count; start_product_size; average_product_size; done_product_count; " .
			"not_done_product_count; all_tasks_count; tasks_with_resources_count; planned_todo_products; " .
			"resources_assigned_products; planned_todo_solutions; resources_assigned_solutions; average_todo_solutions_size; " .
			"average_tasks_progress_percent; points; risk_fighting_percent; active_risks_count; active_threats_count; " .
			"active_opportunities_count; all_risks_ever; all_threats_ever; all_opportunities_ever; all_risks_materialised; " .
			"all_threats_materialised; all_opportunities_materialised; all_threats_removed; all_threats_randomly_disappeared; all_opportunities_missed; " .
			"threats_probability_sum; threats_probability_average; threats_impact_sum; threats_impact_average; " .
			"opportunities_probability_sum; opportunities_probability_average; opportunities_impact_sum; " .
			"opportunities_impact_average; all_solutions_ever_count; all_effective_solutions_ever_count; all_not_effective_solutions_ever_count; " .
			"all_unique_solutions_ever_count; decision;"; 
	
	$extra_risk_log_fieldnames = "[{risk_id, risk_chance, risk_impact, interface_time, active_solution_ids, effective_solutions_count}]";
	
	$query = "INSERT INTO game_logs (log_version, timestamp, gameplay_id, logged_fieldnames, logged_values , extra_risk_log_fieldnames, extra_risk_log_values) 
			VALUES ('". $log_version. "','". $timestamp. "','".  $unique_gameplay_id. "','".  $logged_fieldnames. "','". $normal_log. "','".  $extra_risk_log_fieldnames. "','". $extra_risk_info_log."')";
	echo $query;
	$qry_result = mysqli_query($link, $query) or die(mysqli_error($link));
}
?>