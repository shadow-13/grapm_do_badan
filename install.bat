@echo off
set destination=C:\xampp\htdocs\grapm\
set options=/E /XF /NP /NDL /R:2 /W:5 .buildpath .project .gitignore install.bat /XD .settings .git
set command=%destination% %options%

echo ----START----
IF "%1"=="/D" (
	echo ...usuwanie
	rd /S /Q C:\xampp\htdocs\grapm
)

echo ...kopiowanie do %destination%
robocopy . %command%

echo ----KONIEC----